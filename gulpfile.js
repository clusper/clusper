var elixir = require('laravel-elixir');
require('laravel-elixir-vueify');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    //mix.copy('./node_modules/materialize-css/bin/materialize.css', 'resources/assets/css');
    //mix.copy('./node_modules/materialize-tags/src/materialize-tags.css', 'resources/assets/css');
    //mix.copy('./node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css', 'resources/assets/css');
    //mix.copy('./node_modules/font-awesome/css/font-awesome.css', 'resources/assets/css');
    //mix.copy('./node_modules/nouislider/src/nouislider.css', 'resources/assets/css');

    mix.scripts([
        './node_modules/jquery/dist/jquery.min.js',
        './node_modules/materialize-css/bin/materialize.js',
        './node_modules/wnumb/wNumb.js',
        './node_modules/aws-sdk/dist/aws-sdk.min.js',
        './node_modules/jquery-ui/ui/widget.js',
        './node_modules/blueimp-file-upload/js/jquery.fileupload.js',
        './resources/assets/js/scripts/snd.min.js',
        './resources/assets/js/scripts/video.min.js',
         './resources/assets/js/scripts/Chart.js',
        './resources/assets/js/scripts/slick.min.js',
        './resources/assets/js/scripts/croppic.min.js',
        './resources/assets/js/scripts/typeahead.bundle.min.js',
        './resources/assets/js/scripts/jquery.validate.min.js',
        './resources/assets/js/scripts/notifications/classie.js',
        './resources/assets/js/scripts/notifications/modernizr.custom.js',
        './resources/assets/js/scripts/notifications/snap.svg-min.js',
        './resources/assets/js/scripts/notifications/notificationFx.js',
        './node_modules/nouislider/distribute/nouislider.min.js',
        './resources/assets/js/scripts/jquery.nicescroll.min.js',
        './node_modules/materialize-tags/src/materialize-tags.js',
        './node_modules/jquery-serializejson/jquery.serializejson.js',
        './resources/assets/js/scripts/dropzone.js',
        './resources/assets/js/scripts/script.js',
        './node_modules/socket.io-client/socket.io.js'
    ], 'public/js/scripts/all.js');

    //mix.copy('./node_modules/font-awesome/fonts', 'public/fonts');

    mix.sass('style.scss', 'resources/assets/css/style.css');

    mix.styles([
         './node_modules/font-awesome/css/font-awesome.css',
        './resources/assets/css/materialize.css',
        './resources/assets/css/nouislider.css',
        './resources/assets/css/video-js.min.css',
        './resources/assets/css/preloader.css',
        './resources/assets/css/notifications/ns-default.css',
        './resources/assets/css/notifications/ns-style-growl.css',
        // './resources/assets/css/dropzone.css',
        './resources/assets/css/style.css'
    ]);

    mix.browserify('/app/app.js', 'public/js/app/app.js');
});
