<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1" /> 
		<meta id="token" name="token" value="{{ csrf_token() }}">
	    <link href='https://fonts.googleapis.com/css?family=Lato:400,500,300,700,900' rel='stylesheet' type='text/css'>
	    <link href='https://fonts.googleapis.com/css?family=Raleway:400,700,500' rel='stylesheet' type='text/css'>
			
	    <link rel="stylesheet" type="text/css" href="/css/all.css"/>
	    <link rel="stylesheet" type="text/css" href="/css/style.css"/>
	
	    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	    <script src="/js/scripts/all.js"></script>
	</head> 
<body>
	<div class="home_wrap">
		<header class= "home_header">
			<div class="container">
				<div class="row valign-wrapper">
					<div class="col s4">
						<div class="logo">
							<img src="../img/logo.png" alt="">
						</div>
					</div>
					<div class="col s4">
						<div class="search-wrapper card">
							<input id="search"><i class="material-icons">search</i>
							<div class="search-results"></div>
						</div>
					</div> 
					<div class="col s4 blue-text text-darken-3 right-align what_we_are">
					<i class="material-icons">play_circle_outline</i>
					<span>Take a tour</span>
					</div>
				</div>
			</div>
		</header>
		<div class="container">
			<div class="row">
				<div class="col s6 storage_advantage center-align">
		            <h4 class="blue-text text-darken-3">Your Storage advantages</h4>
		            <div class="advantage1 col s6">
		                <span></span>
		                <h6>Any digital products</h6>
		                <p>Reduce the time and cost of completing tasks and projects.</p>
		            </div>
		            <div class="advantage2 col s6">
		                <span></span>
		                <h6>New type repository</h6>
		                <p>Reduce the time and cost of completing tasks and projects.</p>
		            </div>
		            <div class="advantage3 col s6">
		                <span></span>
		                <h6>Sell your file</h6>
		                <p>Keep all project files and feedback in one easy-to-access place.
		                </p>
		            </div>
		            <div class="advantage4 col s6">
		                <span></span>
		                <h6>Big digital choose</h6>
		                <p>Spend less time managing feedback and more on creating great work.</p>
		            </div>
		            <div class="clr"></div>
		        </div>
				<authorization></authorization>
				<div class="cloud_storage col s12">
            		<h5 class=" blue-text text-darken-3 center-align">Get Cloud Storage Now!</h5>
					<ul>
						@foreach ($categories as $key => $category)
							<li>
								<a class="{{ $category->alias }}" href="/{{ $category->alias }}">
									<span class="waves-effect"></span><p>{{ $category->title }}</p>
								</a>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
		<footer>
			<div class="container">
				<div class="row">
					<div class="col s3 copyright">2015 © Cloudpire.com  All rights reserved.</div>
					<nav class="col s6 center-align"><ul>
						<li><a class="active" href="#">Home </a></li>
						<li><a href="#">About</a></li>
						<li><a href="#">News</a></li>
						<li><a href="#">Contact</a></li>
					</ul></nav>
					<div class="follow_us col s3 right-align">
						Follow us:
						<a class="active" href="#"><i class="fa fa-facebook"></i></a> 
						<a href="#"><i class="fa fa-twitter"></i></a> 
						<a href="#"><i class="fa fa-rss"></i></a> 
					</div> 
				</div>
			</div>
		</footer>
	</div>
	<script src="{{ asset('js/app/home.js') }}"></script>
</body>

</html>