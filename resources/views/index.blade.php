<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="icon" href="/img/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon" />
    <link href='https://fonts.googleapis.com/css?family=Lato:400,500,300,700,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,700,500' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="/css/all.css"/>
    <script src="/js/scripts/all.js"></script>
</head>
<body>
<div id="app">
    <script>
        var socket = io('{{ env('HOST_NAME', 'localhost') }}:8082');
    </script>
    <header-component></header-component>

    <section id="content">

        <spinner></spinner>

        <router-view></router-view>

    </section>

    <footer-component></footer-component>
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script src="/js/app/app.js"></script>
</div>
</body>
</html>