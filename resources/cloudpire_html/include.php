<?php
$name = '../../public'.$_SERVER['REQUEST_URI'];

$fp = fopen($name, 'rb');

switch(pathinfo($name, PATHINFO_EXTENSION)){
    case 'css':
        $contentType = 'text/css';
        break;
    case 'js':
        $contentType = 'text/javascript; charset=UTF-8';
        break;
    case 'ttf':
        $contentType = 'application/font-ttf';
        break;
    case 'woff':
    case 'woff2':
        $contentType = 'application/font-woff';
        break;
    case 'otf':
        $contentType = 'application/font-otf';
        break;
    case 'eot':
        $contentType = 'application/vnd.ms-fontobject';
        break;
    case 'svg':
        $contentType = 'image/svg+xml';
        break;
    default:
        $contentType = mime_content_type($name);
}
header("Content-Type: " . $contentType);
header("Content-Length: " . filesize($name));
file_get_contents($name);
fpassthru($fp);
exit;