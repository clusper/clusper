 <div class="container">
 	<div class="row">
 		<div class="col s6 storage_advantage center-align">
 			<h2 class="blue-text text-darken-3">Your Storage advantages</h2>
 			<div class="advantage1 col s6">
 				<span></span>
 				<h6>Any digital products</h6>
 				<p>Reduce the time and cost of completing tasks and projects.</p>
 			</div>
 			<div class="advantage2 col s6">
 				<span></span>
 				<h6>New type repository</h6>
 				<p>Reduce the time and cost of completing tasks and projects.</p>
 			</div>
 			<div class="advantage3 col s6">
 				<span></span>
 				<h6>Sell your file</h6>
 				<p>Keep all project files and feedback 
 					in one easy-to-access place.
 				</p>
 			</div>
 			<div class="advantage4 col s6">
 				<span></span>
 				<h6>Big digital choose</h6>
 				<p>Spend less time managing feedback and more on creating great work.</p>
 			</div>
 			<div class="clr"></div>
 		</div> 
 		<div class="col s6 autorization">
 			<div class="col s12 card">
 				<h3 class="center-align">Get Started</h3>
 				<i class="material-icons close">close</i>
 				<div class="login">
 					<div class="step1 active row">
 					<form> 
 						<div class="col s6"> 
 							<div class="input-field col s12">
 								<input id="email" type="email" class="validate">
 								<label for="email">Email</label>
 							</div>
 							<div class="input-field col s12">
 								<input id="email" type="email" class="validate">
 								<label for="email">Email</label>
 							</div>
 							<p class="col s12 check">
 								<input type="checkbox" id="test6" checked="checked" />
 								<label class="blue-text text-darken-3" for="test6">I agree to the Terms of Use and to  receive emails.</label>
 							</p>
 						</div>
 						<div class="col s6"> 
 							<div class="input-field col s12">
 								<input id="email" type="email" class="validate">
 								<label for="email">Email</label>
 							</div>
 							<div class="input-field col s12">
 								<input id="email" type="email" class="validate">
 								<label for="email">Email</label>
 							</div>
 							<div class="col s12">
 								<span class="waves-effect submit blue darken-3 waves-light btn col s12">NExt</span>
 							</div>

 						</div>
 					</form>
 					</div>
 					<div class="step2 row">
 						<form class="valign-wrapper">
 							<div class="col s6"> 
 								<div class="col offset-s3 s6">
 									<img src="../img/default_photo.png" alt="" class="circle responsive-img">
 								</div>  
 							</div>
 							<div class="col s6">
 								<div class="input-field col s12">
 									<input id="email" type="email" class="validate">
 									<label for="email">Email</label>
 								</div>
 								<div class="col s12">
 									<span class="waves-effect  blue darken-3 waves-light btn col s12 submit">NExt</span>
 								</div>

 							</div>
 						</form>
 					</div>
 					<div class="step3 row">
 						<form class="valign-wrapper">
 							<div class="col s6"> 
 								<div class="col offset-s3 s6">
 									<img src="../img/default_photo.png" alt="" class="circle responsive-img">
 								</div> 
 								<p class="col s12 center-align">
 									<input type="checkbox" id="test6" checked="checked" />
 									<label class="blue-text text-darken-3" for="test6">Remember me</label>
 								</p> 
 							</div>
 							<div class="col s6">
 								<p class="center-align" >Welcome, <b>Gordon Gekko</b></p>
 								<div class="input-field col s12">
 									<input id="email" type="email" class="validate">
 									<label for="email">Email</label>
 								</div>
 								<div class="col s12">
 									<span class="waves-effect  blue darken-3 waves-light btn col s12 submit">NExt</span>
 								</div>

 							</div>
 						</form>
 					</div> 
 				</div> 
 				<div class="row footer_login">
 					<div class="col s12"> 
 						<div class="col s12 join_with"><span>or login with</span></div>
 						<div class="col s6">
 							<a class="waves-effect waves-light deep-purple darken-4 btn col s12"><i class="fa fa-facebook"></i>Facebook</a>
 						</div>
 						<div class="col s6">
 							<a class="waves-effect waves-light red darken-3 btn col s12"><i class="fa fa-google"></i>Google</a> 
 						</div>

 						<div class="clr"></div>
 					</div>
 				</div>
 			</div>
 		</div>  
 		<div class="cloud_storage col s12">
 			<h3 class=" blue-text text-darken-3 center-align">Get Cloud Storage Now!</h3>  
 			<ul>
 				<li><a class="music"href="#"><span class="waves-effect"></span><p>Music</p></a></li>
 				<li><a class="graphics"href="#"><span class="waves-effect"></span><p>Graphics</p></a></li>
 				<li><a class="audiobooks"href="#"><span class="waves-effect"></span><p>Audiobooks</p></a></li>
 				<li><a class="libraries"href="#"><span class="waves-effect"></span><p>Libraries</p></a></li>
 				<li><a class="ebook"href="#"><span class="waves-effect"></span><p>E-Books</p></a></li>
 				<li><a class="templates"href="#"><span class="waves-effect"></span><p>Templates</p></a></li>
 				<li><a class="photos"href="#"><span class="waves-effect"></span><p>Photos</p></a></li>
 				<li><a class="freebies"href="#"><span class="waves-effect"></span><p>Freebies</p></a></li>
 			</ul>
 		</div>
 	</div> 
 </div>