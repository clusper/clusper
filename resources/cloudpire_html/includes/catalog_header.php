<div class="catalogs scroll_header off">
	<header>
		<div class="container">
			<div class="row">
				<div class="top_header">
					<div class="logo col s4">
						<a href="#">
							<img src="../img/logo.png" height="37" width="214" alt="logo" > 
						</a>
					</div>
					<div class=" search col s4">
						<div class="search-wrapper card">
							<input id="search" class="modal-trigger" href="#search-modal"><i class="material-icons modal-trigger" href="#search-modal">search</i>
							<div class="search-results"></div>
						</div>
					</div>
					<div class="col s4 profile">
						<a class="col s3 green accent-4  btn right cart"><i class="material-icons left">shopping_cart</i>Cart
						<span class="count_cloud"><i class="material-icons blue-text">cloud</i><b>3</b></span></a>
						<span class="col right dropdown-button profile_dropdown" href="#!" data-activates="dropdown1">Hi, Nazar Mykhalkevych<i class="material-icons right">arrow_drop_down</i></span>
						<ul id="dropdown1" class="dropdown-content profile_setting">
							<li><a class="add" href="#"><i class="fa fa-plus"></i>Add Storage</a></li>
							<li><a href="#"><i class="fa fa-list-alt"></i>Shopping list</a></li>
							<li><a href="#"><i class="fa fa-users"></i>Following</a></li>
							<li><a href="#"><i class="fa fa-list-alt"></i>Wish list</a></li>
							<li><a href="#"><i class="fa fa-building"></i>Storage</a></li>
							<li><a href="#"><i class="fa fa-cog"></i>Settings</a></li> 
						</ul>
						<div class="col user_foto right s2">
							<img src="../img/user_photo.jpg" alt="profile" class="circle responsive-img">
						</div>
					</div> 
			</div>
		</div>
		<nav class="menu">
			<ul>
				<li class="color_music"><a class="nav_link" href="#"><i class=" icon icon-music"></i>Music</a></li>
			<li class="color_audiobooks"><a class="nav_link" href="#"><i class=" icon icon-audiobooks"></i>Audiobooks</a></li>
			<li class="color_ebooks"><a class="nav_link" href="#"><i class="icon icon-e-books"></i>E-books</a></li>
			<li class="color_photo"><a class=" active_nav nav_link" href="#"><i class="icon icon-photo"></i>Photo</a></li>
			<li class="color_graphics"><a class="nav_link" href="#"><i class="icon icon-graphics"></i>Graphics</a></li>
			<li class="color_libraries"><a class="nav_link" href="#"><i class="icon icon-libraries"></i>Libraries</a></li>
			<li class="color_templates"><a class="nav_link" href="#"><i class="icon icon-templates"></i>Templates</a></li>
			<li class="color_effects"><a class="nav_link" href="#"><i class="icon icon-effects"></i>Effects</a></li>
		</ul>
	</nav>
</div>
</div>
</header> 