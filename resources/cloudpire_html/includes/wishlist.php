<div class="container middle_content">
		<h2 class="center-align">Wishlist</h2>
		<div class="filter_lots row">
			<div class="col">
				<span class="title">Category</span>
				<div class="filter_group  right color_templates"> 
					<select>
						<option>Music</option>
						<option>Audiobooks</option>
						<option>Music</option> 
						<option>Music</option> 
					</select>
				</div>
			</div>
			<div class="col">
				<span class="title">Price filter</span>
				<div class="filter_group right">
					<div class="input-field">
						<div id="range" class="range-slider"></div>
					</div>
					<div class="row from_to">
						<div class="input-field col s6">
							<input placeholder="From" id="from" type="text"> 
						</div>
						<div class="input-field col s6">
							<input placeholder="To" type="text"> 
						</div>
					</div>
				</div>
			</div>
			<div class="col right"> 
				<span class="title">Sort by</span>
				<div class="filter_group color_music sort_by">
					<select>
						<option>Top</option>
						<option>Last</option>
						<option>Autors</option>
						<option>Downloads</option> 
						<option>Price</option> 
					</select> 
				</div>
			</div> 
		</div>
		<div class="products_list row">
			<div class="col s2">
				<div class="product_card">
					<span class="price free">Free</span>
					<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
					<div class="clr"></div>
					<a href="">
						<p class="product_name">Puppet Theatre Feat. Peter, Björn & John (Original Mix)</p>
						<div class="product_img"><img src="../img/product_img2.jpg" alt=""><span class="product_rate rate_two"><b>2</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
							width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
							preserveAspectRatio="xMidYMid meet">
						</metadata>
						<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
						fill="#000000" stroke="none">
						<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
						-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
						13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
					</g>
				</svg></span></div>
			</a>
			<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
			<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
			<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
			<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
			<span class="download"><i class="fa fa-download"></i>Download</span>
		</div>
	</div>
	<div class="col s2">
		<div class="product_card">
			<span class="price"><i>$</i>3</span>
			<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
			<div class="clr"></div>
			<a href="">
				<p class="product_name">Bring Me To Life</p>
				<div class="product_img color_music"><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
					width="32.000000pt" height="31.000000pt" viewBox="0 0 32.000000 31.000000"
					preserveAspectRatio="xMidYMid meet"> 
					<g transform="translate(0.000000,31.000000) scale(0.100000,-0.100000)"
					fill="#000000" stroke="none">
					<path d="M210 294 c-143 -23 -130 -13 -130 -102 0 -77 0 -77 -27 -79 -56 -3
					-71 -75 -20 -103 24 -13 30 -13 55 0 26 15 27 20 30 102 3 97 -3 91 105 104
					l57 7 0 -37 c0 -31 -3 -36 -24 -36 -50 0 -75 -60 -38 -93 24 -22 65 -21 85 1
					14 15 17 41 17 135 0 88 -3 117 -12 116 -7 -1 -51 -7 -98 -15z"/>
				</g>
			</svg><span class="product_rate rate_three"><b>3</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
			width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
			preserveAspectRatio="xMidYMid meet">
		</metadata>
		<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
		fill="#000000" stroke="none">
		<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
		-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
		13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
	</g>
</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img2.jpg" alt=""><span class="product_rate rate_four"><b>4</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img1.jpg" alt=""><span class="product_rate rate_fife"><b>5</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img1.jpg" alt=""><span class="product_rate rate_fife"><b>4</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img1.jpg" alt=""><span class="product_rate rate_one"><b>4</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img2.jpg" alt=""><span class="product_rate rate_one"><b>4</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img2.jpg" alt=""><span class="product_rate rate_one"><b>4</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div> 
</div>
</div> 