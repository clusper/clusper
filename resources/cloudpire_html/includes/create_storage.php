<div class="container middle_content">
	<div class="row"> 
		<div class="create_storage">  
			<div class="product_prop col s12">
				<h2>Create Storage</h2>
				<form>
					<div class="row prop"> 
						<div class="col s6"> 
							<div class="col s6">
								<span class="label">Label 1</span>
							</div>
							<div class="input-field col s6">
								<input id="password" type="password" class="validate">
								<label for="password">Password</label>
							</div> 
						</div>
						<div class="col s6">
							<div class="col s6">
								<span class="label">Label 2</span>
								<p>Lorem ipsum dolor sit amet, consectetur adip cing elit. Quibusdam, quo?</p>
							</div>
							<div class="input-field col s6">
								<input id="email" type="email" class="validate">
								<label for="email" data-error="wrong" data-success="right">Email</label>
							</div> 
						</div> 
						<div class="col s6"> 
							<div class="col s6">
								<span class="label">Label 3</span>
							</div>
							<div class="input-field col s6">
								<select>
									<option value="" disabled selected>Choose your option</option>
									<option value="1">Option 1</option>
									<option value="2">Option 2</option>
									<option value="3">Option 3</option>
								</select>
								<label>Materialize Select</label>
							</div>
						</div>
						<div class="col s6">
							<div class="col s6">
								<span class="label">Label 4</span> 
							</div>
							<div class="input-field col s6">
								<input id="email" type="email" class="validate">
								<label for="email" data-error="wrong" data-success="right">Email</label>
							</div> 
						</div> 
						<div class="col s6"> 
							<div class="col s6">
								<span class="label">Label 5</span>
							</div>
							<div class="input-field col s6">
								<textarea id="textarea1" class="materialize-textarea"></textarea>
								<label for="textarea1">Textarea</label>
							</div> 
						</div>
						<div class="col s6">
							<div class="col s6">
								<span class="label">Label 6</span> 
							</div>
							<p class="col s6">
								<span class="col">
									<input type="checkbox" id="check1" checked="checked"  />
									<label for="check1">Green</label>
								</span>
								<span class="col">
									<input type="checkbox" id="check2" />
									<label for="check2">blue</label>
								</span> 
							</p>
						</div>
						<div class="col s6"> 
							<div class="col s6">
								<span class="label">Label 7</span>
							</div>
							<p class="col s6"> 
								<span class="col">
									<input class="with-gap" name="group1" type="radio" id="blue"  />
									<label for="blue">blue</label>
								</span>
								<span class="col">
									<input class="with-gap" name="group1" type="radio" id="red"  />
									<label for="red">red</label>
								</span>
							</p>
						</div>
						<div class="col s6">
							<div class="col s6">
								<span class="label">Label 8</span> 
							</div>
							<div class="input-field col s6">
								<input id="text" type="text">
								<label for="text">Name</label>
							</div> 
						</div>
					</div> 
					<div class="btn waves-effect right">Create Storage</div>
				</form>
			</div>
		</div>
	</div>
</div>
