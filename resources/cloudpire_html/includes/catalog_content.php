<div class="container">
	<div class="row">
		<div class="filters valign-wfrapper">
		<h2 class="grey-text text-darken-4 center-align">Repositories filter</h2> 
			<div class="col s2 valign filter-group">
				<span>Materialize Select</span>
				<div class="input-field s12">
					<select> 
						<option value="1">Option 1</option>
						<option value="2">Option 2</option>
						<option value="3">Option 3</option>
					</select>
				</div> 
			</div>
			<div class="col s2 valign filter-group">
				<span>Materialize Select</span>
				<div class="input-field">
				<input name="group1" type="radio" id="test1" />
				<label for="test1">Yellow</label> 
				<input class="with-gap" name="group1" type="radio" id="test2" />
				<label for="test2">Yellow</label> 
				</div>
			</div>
			<div class="col s2  valign filter-group">
				<span>Materialize Select</span>
				<div class="input-field">
				<div id="range" class="range-slider"></div>
				</div>
				<div class="row from_to">
					<div class="input-field col s6">
					<input placeholder="From" id="from" type="text"> 
					</div>
					<div class="input-field col s6">
						<input placeholder="To" type="text"> 
					</div>
				</div>
			</div>
			<div class="col s2 valign filter-group">
				<span>Materialize Select</span>
				<div class="input-field  s12">
					<select> 
						<option value="1">Option 1</option>
						<option value="2">Option 2</option>
						<option value="3">Option 3</option>
					</select> 
				</div> 
			</div>
			<div class="col s4 valign filter-group">
				<span>Materialize Select</span>
				<div class="input-field">
				<input type="checkbox" id="test6" checked="checked" />
				<label for="test6">Yellow</label> 
				<input type="checkbox" id="test3" />
				<label for="test3">Yellow</label>
				<input type="checkbox" id="test4" />
				<label for="test4">Yellow</label>
				<input type="checkbox" id="test5" />
				<label for="test5">Yellow</label>
				<input type="checkbox" id="test6" />
				<label for="test6">Yellow</label>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="blockProducts">
	<div class="container">
		<div class="row">
			<div class="top_repo">
				<h4 class="left-align">Top-16 Music repositories:</h4>
				<a href="#" class="repo">
					<div class="author_photo"><img src="../img/author1.jpg" alt=""></div>
					<p class="author_name">Author Name</p>
				</a>
				<a href="#" class="repo"> 
					<div class="author_photo"><img src="../img/author2.jpg" alt=""></div>
					<p class="author_name">Author Name</p>
				</a>
				<a href="#" class="repo">
					<div class="author_photo"><img src="../img/author3.jpg" alt=""></div>
					<p class="author_name">Author Name</p>
				</a>
				<a href="#" class="repo">
					<div class="author_photo"><img src="../img/author2.jpg" alt=""></div>
					<p class="author_name">Author Name</p>
				</a>
				<a href="#" class="repo"> 
					<div class="author_photo"><img src="../img/author1.jpg" alt=""></div>
					<p class="author_name">Author Name</p>
				</a>
				<a href="#" class="repo">
					<div class="author_photo"><img src="../img/author3.jpg" alt=""></div>
					<p class="author_name">Author Name</p>
				</a>
				<a href="#" class="repo"> 
					<div class="author_photo"><img src="../img/author2.jpg" alt=""></div>
					<p class="author_name">Author Name</p>
				</a>
				<a href="#" class="repo">
					<div class="author_photo"><img src="../img/author3.jpg" alt=""></div>
					<p class="author_name">Author Name</p>
				</a>
				<a href="#" class="repo">
					<div class="author_photo"><img src="../img/author2.jpg" alt=""></div>
					<p class="author_name">Author Name</p>
				</a>
				<a href="#" class="repo">
					<div class="author_photo"><img src="../img/author1.jpg" alt=""></div>
					<p class="author_name">Author Name</p>
				</a>
			</div> 
		</div>  
		<div class="products_list row">
			<div class="col s2">
				<div class="product_card">
					<span class="price"><i>$</i>3</span>
					<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
					<div class="modal_info">
						<h6 class="card-title grey-text text-darken-4">Properties<i class="material-icons close right">close</i></h6>
						<span><label>last update</label>12/27/15</span>
						<span><label>bitrate</label>320 kbps</span>
						<span><label>songs</label>6</span>
						<span><label>downloads</label>89</span>
						<span><label>format</label>mp3</span> 
					</div>
					<div class="clr"></div>
					<a href="">
						<p class="product_name">Poolside Australia 2016</p>
						<div class="product_img"><img src="../img/product_img1.jpg" alt=""><span class="product_rate rate_one"><b>1</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
							width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
							preserveAspectRatio="xMidYMid meet">
						</metadata>
						<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
						fill="#000000" stroke="none">
						<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
						-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
						13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
					</g>
				</svg></span></div>
			</a>
			<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
			<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
			<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
			<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
			<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
		</div>
	</div>
	<div class="col s2">
		<div class="product_card">
			<span class="price free">Free</span>
			<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
			<div class="modal_info">
				<h6 class="card-title grey-text text-darken-4">Properties<i class="material-icons close right">close</i></h6>
				<span><label>last update</label><span>12/27/15</span></span>
				<span><label>bitrate</label><span>320 kbps</span></span>
				<span><label>songs</label><span>6</span></span>
				<span><label>downloads</label><span>89</span></span>
				<span><label>format</label><span>mp3</span></span> 
				</div>
			<div class="clr"></div>
			<a href="">
				<p class="product_name">Puppet Theatre Feat. Peter, Björn & John (Original Mix)</p>
				<div class="product_img"><img src="../img/product_img2.jpg" alt=""><span class="product_rate rate_two"><b>2</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
					width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
					preserveAspectRatio="xMidYMid meet">
				</metadata>
				<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
				fill="#000000" stroke="none">
				<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
				-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
				13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
			</g>
		</svg></span></div>
	</a>
	<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
	<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
	<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
	<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
	<span class="download"><i class="fa fa-download"></i>Download</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img color_music"><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="31.000000pt" viewBox="0 0 32.000000 31.000000"
				preserveAspectRatio="xMidYMid meet"> 
				<g transform="translate(0.000000,31.000000) scale(0.100000,-0.100000)"
				fill="#000000" stroke="none">
				<path d="M210 294 c-143 -23 -130 -13 -130 -102 0 -77 0 -77 -27 -79 -56 -3
				-71 -75 -20 -103 24 -13 30 -13 55 0 26 15 27 20 30 102 3 97 -3 91 105 104
				l57 7 0 -37 c0 -31 -3 -36 -24 -36 -50 0 -75 -60 -38 -93 24 -22 65 -21 85 1
				14 15 17 41 17 135 0 88 -3 117 -12 116 -7 -1 -51 -7 -98 -15z"/>
			</g>
		</svg><span class="product_rate rate_three"><b>3</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
		width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
		preserveAspectRatio="xMidYMid meet">
	</metadata>
	<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
	fill="#000000" stroke="none">
	<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
	-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
	13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
</g>
</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img2.jpg" alt=""><span class="product_rate rate_four"><b>4</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img1.jpg" alt=""><span class="product_rate rate_fife"><b>5</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img1.jpg" alt=""><span class="product_rate rate_fife"><b>4</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img1.jpg" alt=""><span class="product_rate rate_one"><b>4</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img2.jpg" alt=""><span class="product_rate rate_one"><b>4</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img2.jpg" alt=""><span class="product_rate rate_one"><b>4</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div> 
</div>
</div>
<ul class="pagination center-align">
    <li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
    <li class="active"><a href="#!">1</a></li>
    <li class="waves-effect"><a href="#!">2</a></li>
    <li class="waves-effect"><a href="#!">3</a></li>
    <li class="waves-effect"><a href="#!">4</a></li>
    <li class="waves-effect"><a href="#!">5</a></li>
    <li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
  </ul>
</div>