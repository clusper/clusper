<div class="container middle_content">
	<div class="row">
		<div class="col s12 sales">
			<h2>Sales</h2>
			<div class="row">
				<div class="col s8 l3 left">
					<ul class="tabs">
						<li class="tab col s6"><a class="active" href="#sales">Sales</a></li>
						<li class="tab col s6"><a href="#statistic">Statistic</a></li>
					</ul>
				</div>
				<div class=" col right balance">
					<span>Balance: <b class="right">1223$</b></span>
					<div class="btn">Get money</div>
				</div>
				<div class="col right procent">
					<span>Current procent <b class="right">40%</b></span>
					<div class="btn grey lighten-4 blue-text text-darken-3 waves-effect waves-light btn modal-trigger" href="#low_procent">Low procent</div>
				</div>
			</div>
			<div class="row">
				<div id="sales" class=" col s12 sale_box"> 
				<div class="sort_by left valign-wrapper">
						<span class="label col">Sort by:</span>
						<div class="filter_group color_music ">
							<select>
								<option>Top</option>
								<option>Last</option>
								<option>Autors</option>
								<option>Downloads</option> 
								<option>Price</option> 
							</select> 
						</div>
					</div> 
					<table class="bordered centered highlight">
						<thead>
							<tr>
								<th data-field="id">Date</th>
								<th data-field="name">Product</th>
								<th data-field="price">Buyer</th>
								<th data-field="price">License</th>
								<th data-field="price">Profit</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>12/11/15</br>13:12</td>
								<td>Product1</td>
								<td>Eclair</td>
								<td>license</td>
								<td>Profit</td>
							</tr>
							<tr>
								<td>12/11/15</br>13:12</td>
								<td>Product1</td>
								<td>Eclair</td>
								<td>license</td>
								<td>Profit</td>
							</tr>
							<tr>
								<td>12/11/15</br>13:12</td>
								<td>Product1</td>
								<td>Eclair</td>
								<td>license</td>
								<td>Profit</td>
							</tr>
							<tr>
								<td>12/11/15</br>13:12</td>
								<td>Product1 Product1</td>
								<td>Eclair</td>
								<td>license</td>
								<td>Profit</td>
							</tr>
						</tbody>
					</table>
				</div> 
			</div>
			<div id="statistic" class="charts col s12"> 
			<div class="col s6"><canvas id="chart1"></canvas></div>
			<div class="col s6"><canvas id="chart2"></canvas></div>
			<div class="col s6"><canvas id="chart3"></canvas></div>
			<div class="col s6"><canvas id="chart4"></canvas></div>
			<div class="col s12"><canvas id="chart5"></canvas></div>
			</div>
		</div> 
	</div>
</div>