<div class="container">
	<div class="row">
		<div class="left_sidebar col">
			<div class="storage_profile col">
				<div class="col s6">
					<div class="photo circle"><img src="../img/default_photo.png"></div>
				</div>
				<div class="col s6">
					<div class="follows">Follows <span>117</span></div>
					<div class="followers">Followers <span>1237</span></div>
				</div>
				<div class="clearfix"></div>
				<p class="owner_info">Boston - USA, 37 years old</p>
				<p class="slogan_text">Hey guys! Welcome in my cloudpire
					repository. Here you can buy some
					JS scripts, other templates and more.
				</p>
				<div class="prefers_lots">
					<span>Prefers lots:</span>
					<span class="color_audiobooks"><i class="fa fa-headphones"></i></span>
					<span class="color_ebooks"><i class="fa fa-folder"></i></span> 
					<span class="color_graphics"><i class="fa fa-picture-o"></i></span>
					<span class="color_templates"><i class="fa fa-folder"></i></span>
					<span class="color_effects"><i class="fa fa-picture-o"></i></span>
					<span class="color_music"><i class="fa fa-folder"></i></span>
				</div> 
				<span class=" profile_link waves-effect btn-flat"><i class="material-icons left">mode_edit</i> Edit My Profile</span>
				<span class=" profile_link waves-effect btn-flat"><i class="material-icons left">add</i> Add News</span>
				<span class=" profile_link waves-effect btn-flat"><i class="material-icons left">add</i> Add Product</span> 
				<span class=" profile_link waves-effect btn-flat"><i class="material-icons left">mode_edit</i> Edit Profile</span> 
				<a class="waves-effect waves-light btn"><i class="material-icons left">add</i>Follow</a>
			</div> 
		</div>
		<div class="add_products col">
			<div class="product_type col s12">
				<h1>Choose type of product</h1> 
				<ul class="category_list valign-wrapper">
					<li><a class="btn-floating btn-large waves-effect color_music"><i class="icon-music"></i></a></li>
					<li><a class="btn-floating btn-large waves-effect color_audiobooks"><i class="icon-audiobooks"></i></a></li>
					<li><a class="btn-floating btn-large waves-effect color_ebooks"><i class="icon-e-books"></i></a></li>
					<li><a class="active btn-floating btn-large waves-effect color_graphics"><i class="icon-graphics"></i></a></li>
					<li><a class="btn-floating btn-large waves-effect color_photo"><i class="icon-photo"></i></a></li>
					<li><a class="btn-floating btn-large waves-effect color_libraries"><i class="icon-libraries"></i></a></li>
					<li><a class="btn-floating btn-large waves-effect color_templates"><i class="icon-templates"></i></a></li>
					<li><a class="btn-floating btn-large waves-effect color_effects"><i class="icon-effects"></i></a></li>
				</ul> 

			</div>
			<div class="input-field col s12 l3">
				<select>
					<option value="" disabled selected>Choose type</option>
					<option value="1">Music</option>
					<option value="2">Audiobooks</option>
					<option value="3">Ebooks</option>
					<option value="4">Graphics</option>
					<option value="5">Photo</option>
					<option value="6">Libraries</option>
					<option value="7">Templates</option>
					<option value="8">Effects</option>
				</select> 
			</div>
			<div class="col s12">
				<p class="flow-text">Please chose your type</p>
			</div>
			<div class="product_prop col s12">
				<form> 
					<div class="row prop">
						<div class="col s4">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quos quo, veniam velit ducimus.
						</div>
						<div class="col s8">
							<div class="row">
								<div class="input-field col s6">
									<input id="password" type="password" class="validate">
									<label for="password">Password</label>
								</div> 
								<div class="input-field col s6">
									<input id="email" type="email" class="validate">
									<label for="email" data-error="wrong" data-success="right">Email</label>
								</div>
							</div>
						</div>
					</div>
					<div class="divider"></div>
					<div class="row prop">
						<div class="col s4">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus, error. Itaque vero quo voluptatibus.
						</div>
						<div class="col s8">
							<div class="row">
								<div class="input-field col s6">
									<select>
										<option value="" disabled selected>Choose your option</option>
										<option value="1">Option 1</option>
										<option value="2">Option 2</option>
										<option value="3">Option 3</option>
									</select>
									<label>Materialize Select</label>
								</div>
								<div class="input-field col s6">
									<select multiple>
										<optgroup label="team 1">
											<option value="1">Option 1</option>
											<option value="2">Option 2</option>
										</optgroup>
										<optgroup label="team 2">
											<option value="3">Option 3</option>
											<option value="4">Option 4</option>
										</optgroup>
									</select>
									<label>Optgroups</label>
								</div>
							</div>
						</div>
					</div>
					<div class="divider"></div>
					<div class="row prop">
						<div class="col s4">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nam quae enim odit consectetur eos.
						</div>
						<div class="col s8">
							<p class="row">
								<span class="col">
									<input type="checkbox" id="check1" checked="checked"  />
									<label for="check1">Green</label>
								</span>
								<span class="col">
									<input type="checkbox" id="check2" />
									<label for="check2">blue</label>
								</span>
								<span class="col">
									<input type="checkbox" id="check3"/>
									<label for="check3">red</label>
								</span>
							</p> 
						</div>
					</div>
					<div class="divider"></div>
					<div class="row prop">
						<div class="col s4">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Temporibus tempore, facere nihil voluptates explicabo?
						</div>
						<div class="col s8">
							<div class="row">
								<div class="input-field col s12">
									<textarea id="textarea1" class="materialize-textarea"></textarea>
									<label for="textarea1">Textarea</label>
								</div>
							</div>
						</div>
					</div>
					<div class="divider"></div>
					<div class="row prop">
						<span class="right btn">Next</span>
					</div>
					<div class="divider"></div>
					<div class="row prop">
						<span class="col">
									<input type="checkbox" id="agree"  />
									<label for="agree">I agree with ... .. . .</label>
								</span>
					</div> 
					<div class="row prop">
						<span class="center btn">Upload</span>
					</div>
				</form>
			</div>
		</div>

	</div>
</div>
