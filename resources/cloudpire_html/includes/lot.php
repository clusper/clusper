<div class="container"> 
	<section class="storage_info row">
		<div class="col s12">
			<div class="storage_header">
				<p><span>Gordon Gekko</span>'s Storage</p>
				<a class="waves-effect white grey-text btn change_bg"><i class="material-icons">add</i><i class="material-icons ">picture_in_picture</i></a>
			</div>
		</div>
		<div class="left_sidebar col">
			<div class="storage_profile col">
				<div class="col s6">
					<div class="photo circle"><img src="../img/default_photo.png"></div>
				</div>
				<div class="col s6">
					<div class="follows">Follows <span>117</span></div>
					<div class="followers">Followers <span>1237</span></div>
				</div>
				<div class="clearfix"></div>
				<p class="owner_info">Boston - USA, 37 years old</p>
				<p class="slogan_text">Hey guys! Welcome in my cloudpire
					repository. Here you can buy some
					JS scripts, other templates and more.
				</p>
				<div class="prefers_lots">
					<span>Prefers lots:</span>
					<span class="color_audiobooks"><i class="fa fa-headphones"></i></span>
					<span class="color_ebooks"><i class="fa fa-folder"></i></span> 
					<span class="color_graphics"><i class="fa fa-picture-o"></i></span>
					<span class="color_templates"><i class="fa fa-folder"></i></span>
					<span class="color_effects"><i class="fa fa-picture-o"></i></span>
					<span class="color_music"><i class="fa fa-folder"></i></span>
				</div> 
				<span class=" profile_link waves-effect btn-flat"><i class="material-icons left">mode_edit</i> Edit My Profile</span>
				<span class=" profile_link waves-effect btn-flat"><i class="material-icons left">add</i> Add News</span>
				<span class=" profile_link waves-effect btn-flat"><i class="material-icons left">add</i> Add Product</span> 
				<span class=" profile_link waves-effect btn-flat"><i class="material-icons left">mode_edit</i> Edit Profile</span> 
				<a class="waves-effect waves-light btn"><i class="material-icons left">add</i>Follow</a>
			</div>
			<div class="col s12">
				<div class="top_albums">
					<h4>Top Albums</h4>
					<div class="album_box">
						<div class="picture">
							<img src="../img/album_img.jpg">
						</div>
						<p class="album_title">Royalty (Deluxe Vers.)</p>
						<p class="album_author">Chris Brown</p>
					</div>
					<div class="album_box">
						<div class="picture">
							<img src="../img/album_img.jpg">
						</div>
						<p class="album_title">A Head Full Of Dreams</p>
						<p class="album_author">Coldplay</p>
					</div>
					<div class="album_box">
						<div class="picture">
							<img src="../img/album_img.jpg">
						</div>
						<p class="album_title">Royalty (Deluxe Vers.)</p>
						<p class="album_author">Chris Brown</p>
					</div>
					<div class="album_box">
						<div class="picture">
							<img src="../img/album_img.jpg">
						</div>
						<p class="album_title">A Head Full Of Dreams</p>
						<p class="album_author">Coldplay</p>
					</div>
					<div class="album_box">
						<div class="picture">
							<img src="../img/album_img.jpg">
						</div>
						<p class="album_title">Royalty (Deluxe Vers.)</p>
						<p class="album_author">Chris Brown</p>
					</div>
				</div>
			</div>
		</div>
		<div class="lot">
			<div class="main_info">
				<div class="preview">
					<img src="../img/preview.jpg">
				</div>
				<h2 class="name">The old song</h2>
				<p class="subtitle"><span>Album</span>The 25th degree</p>
				
				<div class="date"><i class="fa fa-clock-o"></i> 12/27/15</div>
				<div class="rate"><i class="fa fa-cloud"></i><b>4.1</b></div>
				
				<div class="tags">
					<span class="tag">Top-40</span>
					<span class="tag">Rock</span>
					<span class="tag">Pop</span>
					<span class="tag">Some tag</span>
					<span class="tag">Some another tag</span>
					<span class="tag">Some tag</span>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="ad_info">
				<span><label>last update</label><span>12/27/15</span></span>
				<span class="text_blue"><label>bitrate</label><span>320 kbps</span></span>
				<span><label>songs</label><span>6</span></span>
				<span><label>downloads</label><span>89</span></span>
				<span class="text_blue"><label>format</label><span>mp3</span></span>
				<div class="clearfix"></div>
			</div>
			<div class="audioplayer">
				<div class="player">
					<div class="toggle">
						<span class="toggle-play"><i class="fa fa-play-circle" aria-hidden="true"></i></span>
						<span class="toggle-pause"><i class="fa fa-pause-circle" aria-hidden="true"></i></span>
					</div>
					<span class="prev"><i class="fa fa-backward" aria-hidden="true"></i></span>
					<span class="next"><i class="fa fa-forward" aria-hidden="true"></i></span>
					<span class="btn_mute"><input id="mute" class="mute" type="checkbox"/><label for="mute"><i class="fa fa-volume-up" aria-hidden="true"></i></label></span>
					<span class="name"></span>
					<span class="duration">00:00</span>
					<span class="seperate"> / </span>
					<span class="currenttime">00:00</span>
					<div class="rangetime"><span class="currentpos"></span><input class="time"   type="range" value="0" /></div>
				</div>
				<ol class="playlist">
					<li class="playlist_item waves-effect">Track 1</li>
					<li class="playlist_item waves-effect">Track 2</li>
					<li class="playlist_item waves-effect">Track 3</li>
				</ol>
			</div> 
			<video   class="video-js vjs-default-skin" controls preload="none" data-setup="{}" poster="http://vjs.zencdn.net/v/oceans.png">
				<source src="http://vjs.zencdn.net/v/oceans.mp4" type="video/mp4">  
				</video> 
			<p class="text_info">
				There are two options for sending model data to the agency, one is static where models profile like Name, Age, Country etc. are send to the admin as a email and the other option is dynamic where the models data is stored in database.
			</p>
			<p class="text_info">
				We are keep on updating theme as soon as find the bugs or new features requested by users, so please download updated files from your download section and update your theme by replacing entire theme folder. 
			</p>
			<div class="share">
				<span>Share</span>
				<span class="waves-effect facebook"><i class="fa fa-facebook"></i> 54</span>
				<span class="waves-effect twitter"><i class="fa fa-twitter"></i> 89</span>
				<span class="waves-effect google"><i class="fa fa-google-plus"></i> 36</span>
			</div>
			<div class="comment_block">
				<h4>Comments</h4>
				<ul class="collection">
					<li class="collection-item avatar author_comment">
						<img src="../img/author3.jpg" alt="" class="circle">
						<span class="title">Author1</span><span class="a_label">author</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia deserunt excepturi, commod ores?</p>
						<span href="#!" class="secondary-content"><i class="fa fa-clock-o"></i> today at 14:40</span>
					</li>
					<li class="collection-item avatar">
						<img src="../img/author2.jpg" alt="" class="circle">
						<span class="title">Author2</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia deserunt excepturi</p>
						<span class="secondary-content"><i class="fa fa-clock-o"></i> today at 14:40</span>
					</li>
					<li class="collection-item avatar">
						<img src="../img/author1.jpg" alt="" class="circle">
						<span class="title">Author3</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia deserunt excepturi</p>
						<span href="#!" class="secondary-content"><i class="fa fa-clock-o"></i> today at 14:40</span>
					</li>
					<li class="collection-item avatar author_comment">
						<img src="../img/author3.jpg" alt="" class="circle">
						<span class="title">Author1</span><span class="a_label">author</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia deserunt excepturi, commod ores?</p>
						<span href="#!" class="secondary-content"><i class="fa fa-clock-o"></i> today at 14:40</span>
					</li>
				</ul>
				<ul class="collection">  
					<li class="collection-item avatar">
						<img src="../img/author1.jpg" alt="" class="circle">
						<span class="title">Author3</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia deserunt excepturi</p>
						<span href="#!" class="secondary-content"><i class="fa fa-clock-o"></i> today at 14:40</span>
					</li>
					<li class="collection-item avatar author_comment">
						<img src="../img/author3.jpg" alt="" class="circle">
						<span class="title">Author1</span><span class="a_label">author</span>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia deserunt excepturi, commod ores?</p>
						<span href="#!" class="secondary-content"><i class="fa fa-clock-o"></i> today at 14:40</span>
					</li>
				</ul>
				<form class="col s12">
					<div class="row">
						<div class="input-field  s12">
							<textarea id="comment" class="materialize-textarea"></textarea>
							<label for="comment">Textarea</label>
						</div>
					</div>
					<button class="send_comment btn-flat waves-effect" type="submit"><i class="material-icons">send</i>
					</button>
				</form>
			</div>
		</div>
		<div class="valign-wrapper rigth_col">
			<div class="price">
				<label>Price</label>
				<span>$ <b>4</b></span>
			</div>
			<div class="btn green accent-4 waves-effect"><i class="material-icons left">credit_card</i>Buy now</div>
			<div class="btn grey lighten-4 blue-text text-darken-3 waves-effect"><i class="material-icons left">add</i>Add to Cart</div>
			<div class=" btn grey lighten-4 blue-text text-darken-3 waves-effect add_to_wishlist"><i class="fa left fa-star"></i> Add to Wishlist</div>
			<div class="btn waves-effect white grey-text text-darken-2"><i class="fa left fa-exclamation-circle"></i> Report</div>
		</div>
	</section> 
</div> 
</div> 
</div>