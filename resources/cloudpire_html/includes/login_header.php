<div class="home_wrap">
	<header class= "home_header">
		<div class="container">
			<div class="row valign-wrapper">
				<div class="col s4">
					<div class="logo">
						<img src="../img/logo.png" alt="">
					</div>
				</div>
				<div class="col s4">
					<div class="search-wrapper card">
						<input id="search" class="modal-trigger" href="#search-modal"><i class="material-icons">search</i>
						<div class="search-results"></div>
					</div>
				</div> 
				<div class="col s4 blue-text text-darken-3 right-align what_we_are">
				<i class="material-icons">play_circle_outline</i>
				<span>What we are</span>
				</div>
			</div>
		</div>
	</header>