<div class="container settings_page middle_content">
	<div class="row">
		<div class="col s12">
			<h2>Settings</h2> 
			<div class="dashboard_sidebar col">
				<div class="user_block">
					<div class="user_photo">
						<img src="../img/default_photo.png" alt="Photo">
					</div>
					<div class="user_name">Nazar Mykhalkevych</div> 
					<a class="waves-effect btn change_bg"><i class="material-icons">add</i><i class="material-icons ">picture_in_picture</i></a>
				</div>
			</div>
			<div class="settings_content">
				<div class="row">
					<div class="col s12">
						<ul class="tabs">
							<li class="tab col s3"><a class="active" href="#profile">Profile</a></li>
							<li class="tab col s3"><a href="#billing">Billing</a></li>
							<li class="tab col s3"><a href="#permission">Permission</a></li>
							<li class="tab col s3"><a href="#notification">Notification</a></li>
							<li class="tab col s3"><a href="#invoce">Invoce</a></li>
						</ul>
					</div>
					<div id="profile" class="col s12">
						<form>
							<div class="row">
								<div class="input-field col s6">
									<input id="first_name" type="text" class="validate">
									<label for="first_name">First Name</label>
								</div>
								<div class="input-field col s6">
									<input id="last_name" type="text">
									<label for="last_name">Last Name</label>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<input type="checkbox" id="test5" />
									<label for="test5">Red</label>  
								</div>
								<div class="col">
									<input type="checkbox" id="test6" checked="checked"/>
									<label for="test6">Yellow</label>
								</div>
								<div class="col">
									<input type="checkbox" id="test7"/>
									<label for="test7">Green</label>
								</div> 
							</div> 
							<div class="row">
								<div class="input-field col s6">
									<input id="password" type="password" class="validate">
									<label for="password">Password</label>
								</div>
								<div class="input-field col s6">
									<input id="email" type="email" class="validate">
									<label for="email" data-error="wrong" data-success="right">Email</label>
								</div>
							</div>
							<div class="row">
								<div class="input-field col s6">
									<select> 
										<option value="1">Option 1</option>
										<option value="2">Option 2</option>
										<option value="3">Option 3</option>
									</select>
									<label>Materialize Select</label>
								</div> 
								<div class="input-field col s6">
									<select multiple>
										<option value="" disabled selected>Choose your option</option>
										<option value="1">Option 1</option>
										<option value="2">Option 2</option>
										<option value="3">Option 3</option>
									</select>
									<label>Materialize Multiple Select</label>
								</div>
							</div>
						</form>
					</div>
					<div id="billing" class="col s12">
						<form>
							<div class="row">
								<div class="input-field col s6">
									<input id="first_name" type="text" class="validate">
									<label for="first_name">First Name</label>
								</div>
								<div class="input-field col s6">
									<input id="last_name" type="text">
									<label for="last_name">Last Name</label>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<input type="checkbox" id="test5" />
									<label for="test5">Red</label>  
								</div>
								<div class="col">
									<input type="checkbox" id="test6" checked="checked"/>
									<label for="test6">Yellow</label>
								</div>
								<div class="col">
									<input type="checkbox" id="test7"/>
									<label for="test7">Green</label>
								</div> 
							</div> 
							<div class="row">
								<div class="input-field col s12">
									<textarea id="textarea1" class="materialize-textarea"></textarea>
									<label for="textarea1">Textarea</label>
								</div>
							</div>
							<div class="row">
								<div class="col">
									<input class="with-gap" name="group1" type="radio" id="test1"  />
									<label for="test1">Red</label> 
								</div>
								<div class="col">
									<input class="with-gap" name="group1" type="radio" id="test2"  />
									<label for="test2">Green</label>
								</div> 
							</div>
						</form>
					</div>
					<div id="permission" class="col s12">Permission</div>
					<div id="notification" class="col s12">Notification</div>
					<div id="invoce" class="col s12">Invoce</div>
				</div>
			</div>

		</div>
	</div>
</div>