<!-- Modal Structure -->
<div id="search-modal" class=" modal search-modal">
    <div class="container">
        <div class="row">
            <div class="input-field col s12">
                <input id="search_input" type="text">
                <label for="search_input">Search</label>
            </div> 
            <div class="modal-action modal-close material-icons right">add</div> 
        </div>
        <div class="row">
            <h4>Categories - 24 founds</h4>
            <div class="col s3">
                <div class="title color_music"><i class=" icon icon-music"></i>Music<span>8 result</span></div>
                <ul>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">Free</span></a></li>
                    <li><a href="#">02 Mason  - Willing down copy<span class="right">$4</span></a></li>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                    <li><a href="#">02 Mason  - Willing down copy<span class="right">$4</span></a></li>
                </ul>
                <a href="" class="show_all right">Show All <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
            </div>
            <div class="col s3">
                <div class="title color_audiobooks"><i class=" icon icon-audiobooks"></i>Audiobooks<span>8 result</span></div>
                <ul>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                </ul>
                <a href="" class="show_all right">Show All <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
            </div>
            <div class="col s3">
                <div class="title color_ebooks"><i class="icon icon-e-books"></i>E-books<span>8 result</span></div>
                <ul>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                </ul>
                <a href="" class="show_all right">Show All <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
            </div>
            <div class="col s3">
                <div class="title color_photo "><i class="icon icon-photo"></i>Photo<span>8 result</span></div>
                <ul>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">Free</span></a></li>
                    <li><a href="#">02 Jason - I'm Not The Only One<span class="right">$4</span></a></li>
                    <li><a href="#">02 Mason  - Willing down copy<span class="right">$4</span></a></li>
                </ul>
                <a href="" class="show_all right">Show All <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
            </div>
            <div class="col s3">
                <div class="title color_graphics"><i class="icon icon-graphics"></i>Graphics<span>8 result</span></div>
                <ul>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">Free</span></a></li>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                    <li><a href="#">02 Jason - I'm Not The Only One<span class="right">$4</span></a></li> 
                </ul>
                <a href="" class="show_all right">Show All <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
            </div>
            <div class="col s3">
                <div class="title color_libraries"><i class="icon icon-libraries"></i>Libraries<span>8 result</span></div>
                <ul>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                </ul>
                <a href="" class="show_all right">Show All <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
            </div>
            <div class="col s3">
                <div class="title color_templates"><i class="icon icon-templates"></i>Templates<span>no matches</span></div>
                <ul>
                </ul>
                <a href="" class="show_all right">Show All <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
            </div>
            <div class="col s3">
                <div class="title color_effects"><i class="icon icon-effects"></i>Effects<span>8 result</span></div>
                <ul>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>

                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                    <li><a href="#">Gordon Gekko - 02 Stay With Me<span class="right">$4</span></a></li>
                </ul>
                <a href="" class="show_all right">Show All <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
            </div>
        </div>
        <div class="row">
            <h4>Srorage - 18 found <a class="show_all" href="#">ALL</a></h4>
            <div class="founded_storage valign-wrapper">
                <a href="#" class="col">
                    <div class="center-align"><img src="../img/author3.jpg" alt="" class="circle"></div>
                    <p class="name center-align">Fred Gream</p>
                </a>
                <a href="#" class="col">
                    <div class="center-align"><img src="../img/author1.jpg" alt="" class="circle"></div>
                    <p class="name center-align">Fred Gream</p>
                </a>
                <a href="#" class="col">
                    <div class="center-align"><img src="../img/author2.jpg" alt="" class="circle"></div>
                    <p class="name center-align">Fred Gream</p>
                </a>
                <a href="#" class="col">
                    <div class="center-align"><img src="../img/author3.jpg" alt="" class="circle"></div>
                    <p class="name center-align">Fred Gream</p>
                </a>
                <a href="#" class="col">
                    <div class="center-align"><img src="../img/author1.jpg" alt="" class="circle"></div>
                    <p class="name center-align">Fred Gream</p>
                </a>
                <a href="#" class="col">
                    <div class="center-align"><img src="../img/author3.jpg" alt="" class="circle"></div>
                    <p class="name center-align">Fred Gream</p>
                </a>
                <a href="#" class="col">
                    <div class="center-align"><img src="../img/author2.jpg" alt="" class="circle"></div>
                    <p class="name center-align">Fred Gream</p>
                </a>
                <a href="#" class="col">
                    <div class="center-align"><img src="../img/author1.jpg" alt="" class="circle"></div>
                    <p class="name center-align">Fred Gream</p>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- Modal Structure -->
  <div id="low_procent" class="modal">
    <div class="modal-content">
      <h4>Modal Header</h4>
      <p>I want to low procent</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">Ok</a>
    </div>
  </div>
<!-- Modal Structure -->
<div id="manage" class="modal">
<a href="#!" class="modal-action modal-close btn-close"><i class="fa fa-times" aria-hidden="true"></i></a>
    <div class="modal-content">
    <h3>Manage Header</h3>
        <div class="row">
            <div class="col s6">
                <div class="storage_item">
                    <div class="storage_logo">
                        <img class="responsive-img" src="../img/preview.jpg" alt="logo">
                    </div>
                    <div class="storage_name">
                        The best Storage
                    </div>
                    <div class="btn">Subscribe</div>
                </div>
                <div class="storage_item">
                    <div class="storage_logo">
                        <img class="responsive-img" src="../img/preview.jpg" alt="logo">
                    </div>
                    <div class="storage_name">
                        The best Storage in the world
                    </div>
                    <div class="btn">Subscribe</div>
                </div>
                <div class="storage_item">
                    <div class="storage_logo">
                        <img class="responsive-img" src="../img/preview.jpg" alt="logo">
                    </div>
                    <div class="storage_name">
                        The best Storage in the world
                    </div>
                    <div class="btn">Subscribe</div>
                </div>
                <div class="storage_item">
                    <div class="storage_logo">
                        <img class="responsive-img" src="../img/preview.jpg" alt="logo">
                    </div>
                    <div class="storage_name">
                        The best Storage in the world
                    </div>
                    <div class="btn">Subscribe</div>
                </div>
            </div>
            <div class="col s6">
              <div class="storage_item">
                <div class="storage_logo">
                    <img class="responsive-img" src="../img/preview.jpg" alt="logo">
                </div>
                <div class="storage_name">
                    The best Storage in the world
                </div>
                <div class="btn">Subscribe</div>
            </div>
            <div class="storage_item">
                <div class="storage_logo">
                    <img class="responsive-img" src="../img/preview.jpg" alt="logo">
                </div>
                <div class="storage_name">
                    The best Storage
                </div>
                <div class="btn">Subscribe</div>
            </div>
            <div class="storage_item">
                <div class="storage_logo">
                    <img class="responsive-img" src="../img/preview.jpg" alt="logo">
                </div>
                <div class="storage_name">
                    The best Storage
                </div>
                <div class="btn">Subscribe</div>
            </div>
            <div class="storage_item">
                <div class="storage_logo">
                    <img class="responsive-img" src="../img/preview.jpg" alt="logo">
                </div>
                <div class="storage_name">
                    The best Storage
                </div>
                <div class="btn">Subscribe</div>
            </div>
        </div>
    </div> 
</div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col s3 copyright">2015 © Cloudpire.com  All rights reserved.</div>
            <nav class="col s6 center-align"><ul>
                <li><a class="active" href="#">Home </a></li>
                <li><a href="#">About</a></li>
                <li><a href="#">News</a></li>
                <li><a href="#">Contact</a></li>
            </ul></nav>
            <div class="follow_us col s3 right-align">
                Follow us:
                <a class="active" href="#"><i class="fa fa-facebook"></i></a> 
                <a href="#"><i class="fa fa-twitter"></i></a> 
                <a href="#"><i class="fa fa-rss"></i></a> 
            </div> 
        </div>
    </div>
</footer>
</div> 
<script src="../js/scripts/all.js"></script>  
<!--<script src="../js/nouislider.js"></script>
<script src="../js/materialize.min.js"></script>
<script src="../js/slick.min.js"></script>
<script src="../js/script.js"></script>-->
</body>
</html>