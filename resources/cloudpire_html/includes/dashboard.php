<div class="container middle_content">
	<div class="row">
		<div class="col">
			<h2>Dashboard</h2> 
			<div class="dashboard_sidebar col">
				<div class="user_block">
					<div class="user_photo">
						<img src="../img/default_photo.png" alt="Photo">
					</div>
					<div class="user_name">Nazar Mykhalkevych</div>
					<div class="follow btn modal-trigger grey lighten-4 blue-text text-darken-3" href="#manage"><i class="fa fa-cog  manage modal-trigger" href="#manage""></i>Followers</div>
					<span class="count_follow right">34</span>
				</div>
				<div class="watched_list">
					<h4>Last viewed</h4>
					<a href="#" class="product waves-effect">
						<div class="img">
							<img src="../img/album_img.jpg" alt="Photo">
						</div>
						<div class="name">Cool product</div>
						<div class="price right"><span>$</span>32</div>
					</a>
					<a href="#" class="product waves-effect">
						<div class="img">
							<img src="../img/album_img.jpg" alt="Photo">
						</div>
						<div class="name">Cool product</div>
						<div class="price right"><span>$</span>32</div>
					</a>
					<a href="#" class="product waves-effect">
						<div class="img">
							<img src="../img/album_img.jpg" alt="Photo">
						</div>
						<div class="name">Cool product</div>
						<div class="price right"><span>$</span>32</div>
					</a>
					<a href="#" class="product waves-effect">
						<div class="img">
							<img src="../img/album_img.jpg" alt="Photo">
						</div>
						<div class="name">Cool product</div>
						<div class="price right"><span>$</span>32</div>
					</a>
					<a href="#" class="product waves-effect">
						<div class="img">
							<img src="../img/album_img.jpg" alt="Photo">
						</div>
						<div class="name">Cool product</div>
						<div class="price right"><span>$</span>32</div>
					</a>
				</div>
			</div>
			<div class="dashboard_content">
				<div class="timeline_content">
					<div class="time_label">
						<span class="chip">3 Jan. 2016</span> 
					</div>
					<div class="timeline_item">
						<div class="timeline_icon blue"><i class="fa fa-envelope"></i></div>
						<div class="timeline_header">
							<span class="time right"><i class="fa fa-clock-o"></i> 12:05</span>
							<h5><a href="#">Support Team</a> sent you an email</h5> 
						</div>
						<div class="timeline_body">
							Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles, weebly ning heekya handango imeem plugg dopplr jibjab, movity jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle quora plaxo ideeli hulu weebly balihoo...
						</div>
					</div>
					<div class="timeline_item">
						<div class="timeline_icon green"><i class="fa fa-user"></i></div>
						<div class="timeline_header">
							<span class="time right"><i class="fa fa-clock-o"></i> 12:05</span>
							<h5><a href="#">Sarah Young</a> accepted your friend request</h5>
						</div> 
					</div> 
					<div class="timeline_item">
						<div class="timeline_icon purple"><i class="fa fa-comments"></i></div>
						<div class="timeline_header">
							<span class="time right"><i class="fa fa-clock-o"></i> 27 mins ago</span>
							<h5><a href="#">Jay White</a> commented on your post</h5> 
						</div>
						<div class="timeline_body">
							Take me to your leader! Switzerland is small and neutral! We are more like Germany, ambitious and misunderstood...
						</div>
					</div>
					<div class="time_label">
						<span class="chip">10 Feb. 2016</span> 
					</div>
					<div class="timeline_item">
						<div class="timeline_icon orange"><i class="fa fa-camera"></i></div>
						<div class="timeline_header">
							<span class="time right"><i class="fa fa-clock-o"></i> 12:05</span>
							<h5><a href="#">Mina Lee</a>  uploaded new photos</h5> 
						</div>
						<div class="timeline_body">
							<img src="../img/storage_img1.jpg" alt="photo1">
							<img src="../img/product_img2.jpg">
							<img src="../img/storage_img2.jpg" alt="photo2">
							<img src="../img/product_img2.jpg">
							<img src="../img/storage_img3.jpg" alt="photo3">
						</div>
					</div>
					<div class="timeline_icon"><i class="fa fa-clock-o"></i></div>
				</div>
			</div>
		</div>
	</div>
</div>