<div class="container"> 
	<section class="storage_info row">
		<div class="col s12">
			<div class="storage_header">
				<p><span>Gordon Gekko</span>'s Storage</p>
				<a class="waves-effect white grey-text btn change_bg"><i class="material-icons">add</i><i class="material-icons ">picture_in_picture</i></a>
			</div>
		</div>
		<div class="left_sidebar col">
			<div class="storage_profile col">
				<div class="col s6">
					<div class="photo circle"><img src="../img/default_photo.png"></div>
				</div>
				<div class="col s6">
					<div class="follows">Follows <span>117</span></div>
					<div class="followers">Followers <span>1237</span></div>
				</div>
				<div class="clearfix"></div>
				<p class="owner_info">Boston - USA, 37 years old</p>
				<p class="slogan_text">Hey guys! Welcome in my cloudpire
					repository. Here you can buy some
					JS scripts, other templates and more.
				</p>
				<div class="prefers_lots">
					<span>Prefers lots:</span>
					<span class="color_audiobooks"><i class="fa fa-headphones"></i></span>
					<span class="color_ebooks"><i class="fa fa-folder"></i></span> 
					<span class="color_graphics"><i class="fa fa-picture-o"></i></span>
					<span class="color_templates"><i class="fa fa-folder"></i></span>
					<span class="color_effects"><i class="fa fa-picture-o"></i></span>
					<span class="color_music"><i class="fa fa-folder"></i></span>
				</div> 
				<span class=" profile_link waves-effect btn-flat"><i class="material-icons left">mode_edit</i> Edit My Profile</span>
				<span class=" profile_link waves-effect btn-flat"><i class="material-icons left">add</i> Add News</span>
				<span class=" profile_link waves-effect btn-flat"><i class="material-icons left">add</i> Add Product</span> 
				<span class=" profile_link waves-effect btn-flat"><i class="material-icons left">mode_edit</i> Edit Profile</span> 
				<a class="waves-effect waves-light btn"><i class="material-icons left">add</i>Follow</a> 
			</div>
		</div>
		<div class="storage_content col"> 
			<h2>News</h2>
			<div class="block_news">
				<div class="">
					<div class="photo_carusel">
						<img src="../img/storage_img1.jpg">
						<img src="../img/product_img1.jpg">
						<img src="../img/storage_img2.jpg">
						<img src="../img/product_img2.jpg">
						<img src="../img/storage_img1.jpg">
						<img src="../img/storage_img2.jpg"> 
					</div>
					<div class="news_list nice_scroll">
						<div class="new waves-effect">
							<p>You’ve just purchased <a href="#">Boost UI Kit.psd</a></p>
							<div class="new_info">
								<span class="coment"><i class="fa fa-commenting"></i> 3</span>
								<span class="time_new"><i class="fa fa-clock-o"></i> today at 14:40</span>
								<span class="del_new">+</span>
							</div>
						</div>
						<div class="new waves-effect">
							<p>You’ve added file <span>2 audio files</span></p>
							<div class="atouch_file"><i class="fa fa-folder-open-o"></i>The Old Song.mp3<i class="fa fa-cloud-download"></i></div>
							<div class="atouch_file"><i class="fa fa-folder-open-o"></i>Long time ago in Lviv.mp3<i class="fa fa-cloud-download"></i></div>
							<div class="new_info">
								<span class="coment"><i class="fa fa-commenting"></i> 3</span>
								<span class="time_new"><i class="fa fa-clock-o"></i> today at 14:40</span>
								<span class="del_new">+</span>
							</div>
						</div>
						<div class="new waves-effect">
							<p>You’ve just appreciated your <a href="#">Alan Petrov’s</a> project</p>
						</div>
						<div class="new waves-effect">
							<p>You’ve just purchased <a href="#">Boost UI Kit.psd</a></p>
							<div class="new_info">
								<span class="coment"><i class="fa fa-commenting"></i> 3</span>
								<span class="time_new"><i class="fa fa-clock-o"></i> today at 14:40</span>
								<span class="del_new">+</span>
							</div>
						</div>
						<div class="new waves-effect">
							<p>You’ve added file <span>2 audio files</span></p>
							<div class="atouch_file"><i class="fa fa-folder-open-o"></i>The Old Song.mp3<i class="fa fa-cloud-download"></i></div>
							<div class="atouch_file"><i class="fa fa-folder-open-o"></i>Long time ago in Lviv.mp3<i class="fa fa-cloud-download"></i></div>
							<div class="new_info">
								<span class="coment"><i class="fa fa-commenting"></i> 3</span>
								<span class="time_new"><i class="fa fa-clock-o"></i> today at 14:40</span>
								<span class="del_new">+</span>
							</div>
						</div>
					</div>
				</div>
				<span class="waves-effect btn grey lighten-5 grey-text text-darken-2 show_more"><i class="material-icons left">play_for_work</i>  Show More</span> 
			</div>
			<div class="news_bl">
				<div class="news_box">
					<span class="waves-effect back btn-flat"><i class="fa fa-angle-left left"></i>Back to news</span>  
					<p class="title_new">Alan's <a href="#">Boost UI Kit.psd</a> purchase</p>
					<a href="#" class="author_new">
						<span><img src="../img/author1.jpg" alt="author"></span>
						Alan Ayrton
					</a>
					<span class="time_new"><i class="fa fa-clock-o"></i> today at 14:40</span>
					<div class="clearfix"></div>
					<p class="text_new">Lorem is. Voluptatum laudantium ut, minus nam. Libero facilis aspernatur, cumque quisqum laudantium ut, minus nam. Libero facilis aspernatur, cpsum dolor sit amet, consectetur adipisicing elit. Ut dignissimos ipsam obcaecati corrupti modi, deserunt facere asperioreumque quisqum laudantium ut, minus nam. Libero facilis aspernatur, cumque quisquam quod, sint odit dolorum voluptates, enim sit qui hic laboriosam odio doloribus suscipit.</p>
					<span class="file"><i class="fa fa-file-audio-o"></i><span>12mb</span></span>
					<span class="btn  download_file waves-effect"><i class="fa fa-download left"></i> Download</span>
				</div>
				<div class="comments_box">
					<p class="comment_count">3 Comments </p>
					<div class="btn btn grey lighten-5 grey-text text-darken-2 waves-effect write_comment"><i class="fa fa-pencil left"></i>Comment</div>
					<div class="nice_scroll">
						<div class="row leave_comment">
							<div class="input-field col s12">
								<textarea id="comment_text" class="materialize-textarea"></textarea>
								<label for="comment_text">Your comment text...</label>
								<button class="btn-flat  send_comment waves-effect" type="submit"><i class="material-icons">send</i>
								</button>
							</div>
						</div>
						<div class="comment">
							<a href="#" class="author_comment">
								<span><img src="../img/author2.jpg"></span>
								Carl Stanley
							</a>
							<p class="comment_text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum aspernatur est at voluptatem minus temporibus qui, reprehenderit culpa voluptates quo.</p>
							<span class="comment_time"><i class="fa fa-clock-o"></i> today at 14:40</span>
						</div>
						<div class="comment">
							<a href="#" class="author_comment">
								<span><img src="../img/author3.jpg"></span>
								Carl Stanley
							</a>
							<p class="comment_text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum aspernatur est at voluptatem minus temporibus qui, reprehenderit culpa voluptates quo.</p>
							<span class="comment_time"><i class="fa fa-clock-o"></i> yesterday at 14:40</span>
						</div>
						<div class="comment">
							<a href="#" class="author_comment">
								<span><img src="../img/author1.jpg"></span>
								Carl Stanley
							</a>
							<p class="comment_text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum aspernatur est at voluptatem minus temporibus qui, reprehenderit culpa voluptates quo.</p>
							<span class="comment_time"><i class="fa fa-clock-o"></i> today at 14:40</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> 
</div>
<div class="blockProducts">
	<div class="container">  
		<h2 class="center-align">Gekko's Lots</h2>
		<div class="filter_lots row">
			<div class="col">
				<span class="title">Category</span>
				<div class="filter_group  right color_templates"> 
					<select>
						<option>Music</option>
						<option>Audiobooks</option>
						<option>Music</option> 
						<option>Music</option> 
					</select>
				</div>
			</div> 
			<div class="col right"> 
				<span class="title">Sort by</span>
				<div class="filter_group color_music sort_by">
					<select>
						<option>Top</option>
						<option>Last</option>
						<option>Autors</option>
						<option>Downloads</option> 
						<option>Price</option> 
					</select> 
				</div>
			</div> 
		</div>
		<div class="products_list row">
			<div class="col s2">
				<div class="product_card"> 
					<span class="price"><i>$</i>3</span>
					<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
					<div class="clr"></div>
					<a href="">
						<p class="product_name">Poolside Australia 2016</p>
						<div class="product_img"><img src="../img/product_img1.jpg" alt=""><span class="product_rate rate_one"><b>1</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
							width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
							preserveAspectRatio="xMidYMid meet">
						</metadata>
						<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
						fill="#000000" stroke="none">
						<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
						-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
						13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
					</g>
				</svg></span></div>
			</a>
			<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
			<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
			<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
			<span class="delete_product waves-effect"><i class="fa fa-trash"></i>Delete</span>
			<span class="edit waves-effect"><i class="fa fa-pencil"></i>Edit</span>
		</div>
	</div>
	<div class="col s2">
		<div class="product_card">
			<span class="price free">Free</span>
			<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
			<div class="clr"></div>
			<a href="">
				<p class="product_name">Puppet Theatre Feat. Peter, Björn & John (Original Mix)</p>
				<div class="product_img"><img src="../img/product_img2.jpg" alt=""><span class="product_rate rate_two"><b>2</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
					width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
					preserveAspectRatio="xMidYMid meet">
				</metadata>
				<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
				fill="#000000" stroke="none">
				<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
				-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
				13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
			</g>
		</svg></span></div>
	</a>
	<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
	<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
	<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
	<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
	<span class="download"><i class="fa fa-download"></i>Download</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img color_music"><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="31.000000pt" viewBox="0 0 32.000000 31.000000"
				preserveAspectRatio="xMidYMid meet"> 
				<g transform="translate(0.000000,31.000000) scale(0.100000,-0.100000)"
				fill="#000000" stroke="none">
				<path d="M210 294 c-143 -23 -130 -13 -130 -102 0 -77 0 -77 -27 -79 -56 -3
				-71 -75 -20 -103 24 -13 30 -13 55 0 26 15 27 20 30 102 3 97 -3 91 105 104
				l57 7 0 -37 c0 -31 -3 -36 -24 -36 -50 0 -75 -60 -38 -93 24 -22 65 -21 85 1
				14 15 17 41 17 135 0 88 -3 117 -12 116 -7 -1 -51 -7 -98 -15z"/>
			</g>
		</svg><span class="product_rate rate_three"><b>3</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
		width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
		preserveAspectRatio="xMidYMid meet">
	</metadata>
	<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
	fill="#000000" stroke="none">
	<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
	-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
	13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
</g>
</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img2.jpg" alt=""><span class="product_rate rate_four"><b>4</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img1.jpg" alt=""><span class="product_rate rate_fife"><b>5</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img1.jpg" alt=""><span class="product_rate rate_fife"><b>4</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img1.jpg" alt=""><span class="product_rate rate_one"><b>4</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img2.jpg" alt=""><span class="product_rate rate_one"><b>4</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div>
<div class="col s2">
	<div class="product_card">
		<span class="price"><i>$</i>3</span>
		<span class="btn_sample waves-effect"><i class="fa fa-eye"></i>Sample</span>
		<div class="clr"></div>
		<a href="">
			<p class="product_name">Bring Me To Life</p>
			<div class="product_img"><img src="../img/product_img2.jpg" alt=""><span class="product_rate rate_one"><b>4</b><svg version="1.0" xmlns="http://www.w3.org/2000/svg"
				width="32.000000pt" height="21.000000pt" viewBox="0 0 32.000000 21.000000"
				preserveAspectRatio="xMidYMid meet">
			</metadata>
			<g transform="translate(0.000000,21.000000) scale(0.100000,-0.100000)"
			fill="#000000" stroke="none">
			<path d="M106 189 c-14 -11 -26 -29 -26 -40 0 -12 -7 -19 -20 -19 -28 0 -60
			-35 -60 -65 0 -14 9 -34 20 -45 18 -18 33 -20 135 -20 110 0 117 1 140 25 14
			13 25 35 25 48 0 31 -36 77 -60 77 -10 0 -32 14 -48 30 -36 35 -69 38 -106 9z"/>
		</g>
	</svg></span></div>
</a>
<a href="#" class="product_author"><i class="fa fa-user-secret"></i>Arsen Soroka</a>
<div class="product_tags"><i class="fa fa-tag"></i>Top-40, Pop</div>
<div class="product_look"><i class="fa fa-cloud-download"></i>145</div>
<span class="add_to_wishroom waves-effect">Add to <small>Wishroom</small></span>
<span class="buy waves-effect"><i class="fa fa-credit-card"></i>Buy</span>
</div>
</div> 
</div>
</div>
<ul class="pagination center-align">
	<li class="disabled"><a href="#!"><i class="material-icons">chevron_left</i></a></li>
	<li class="active"><a href="#!">1</a></li>
	<li class="waves-effect"><a href="#!">2</a></li>
	<li class="waves-effect"><a href="#!">3</a></li>
	<li class="waves-effect"><a href="#!">4</a></li>
	<li class="waves-effect"><a href="#!">5</a></li>
	<li class="waves-effect"><a href="#!"><i class="material-icons">chevron_right</i></a></li>
</ul>
</div>