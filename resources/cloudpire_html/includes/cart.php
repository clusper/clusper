<div class="container middle_content">
	<div class="row">
		<h2 class="center-align">Shopping Cart</h2>
		<div class="col s9 products">
			<div class="col s12"> 
				<div class="row">
					<div class="waves-effect btn grey lighten-5 grey-text text-darken-2 continue_shopping">
						<i class="fa fa-chevron-left" aria-hidden="true"></i>
						Continue Shopping
					</div>
					<div class="waves-effect btn grey lighten-3 black-text empty_cart"> Empty Cart</div>
				</div>
				<div class="row">
					<div class="col s12">
						<div class="product_photo left"><img src="../img/storage_img1.jpg" alt="photo"></div>
						<div class="products_name  grey-text text-darken-2">The Old-Song.mp3</div>
						<div class="product_by  grey-text text-darken-1">Product by <a href="#">Gordon Gekko</a></div>
						<div class="delete btn-flat grey-text right"><i class="fa fa-times" aria-hidden="true"></i> Delete</div>
						<div class="product_price green-text text-accent-4"><span>$</span>3</div>
					</div>
					<div class="divider col s12"></div>
					<div class="col s12">
						<div class="product_photo left"><img src="../img/storage_img1.jpg" alt="photo"></div>
						<div class="products_name  grey-text text-darken-2">The Old-Song.mp3</div>
						<div class="product_by  grey-text text-darken-1">Product by <a href="#">Gordon Gekko</a></div>
						<div class="delete btn-flat grey-text right"><i class="fa fa-times" aria-hidden="true"></i> Delete</div>
						<div class="product_price green-text text-accent-4"><span>$</span>3</div>
					</div>
					<div class="divider col s12"></div>
					<div class="col s12">
						<div class="product_photo left"><img src="../img/storage_img1.jpg" alt="photo"></div>
						<div class="products_name  grey-text text-darken-2">The Old-Song.mp3</div>
						<div class="product_by  grey-text text-darken-1">Product by <a href="#">Gordon Gekko</a></div>
						<div class="delete btn-flat grey-text right"><i class="fa fa-times" aria-hidden="true"></i> Delete</div>
						<div class="product_price green-text text-accent-4"><span>$</span>3</div>
					</div>
					<div class="btn right green accent-4 waves-effect">Next</div>
				</div>
			</div>
		</div>
		<div class="col s3 total_sum">
			<div class="center-align">You have <b>3</b> items in your cart</div>
			<div class="total_price">
			<p class="left">Total</br>Summary</p>
			<span class="green-text text-accent-4 right"><i>$</i>13</span></div>
		</div>
	</div>
</div>