'use strict';

$(function(){
  //initStyleElements();
  /********* INPUT EDIT *******/
  $(document).on('keyup','.form-control',function(){
    if($(this).val().length)
      $(this).addClass('edited');
});
  $(document).on('click','.delete',function(){
   $(this).siblings('.form-control')
   .removeClass("edited")
   .val("");
   $(this).addClass('edited');
});

  $(document).on('click','.settings_toggle',function(e){
    e.preventDefault();
    e.stopPropagation();
    $('.dropdown_settings').slideToggle(150);
});
  if($(".nice_scroll").length > 0)
    $(".nice_scroll").niceScroll({
        zindex: 10000,
        autohidemode: "leave"
    });
$(window).scroll(function(){
    if($(this).scrollTop()>100 && $(".scroll_header").hasClass("off")){
     $(".scroll_header").addClass("on").removeClass("off");
     $(".scroll_header header").addClass("fix_header");
 }
 if($(this).scrollTop()<=100 && $(".scroll_header").hasClass("on")){
    $(".scroll_header header").removeClass("fix_header");
    $(".scroll_header").removeClass("on").addClass("off");
}
});

$(document).on('click','.sort_list .top',function(){
    $(this).toggleClass('down');
    $(this).toggleClass('up');
});

$(document).on('click','.sort_list li',function(){
    $('.sort_list li').removeClass('active');
    $(this).addClass('active');
});

$(document).on('click','.write_comment ,.leave_comment button',function(){
    $('.leave_comment').slideToggle();
});

$('.modal-trigger').leanModal({
    top: 0,
    opacity: 0.5,
    ready: function() {
      $("#search_input").focus();
  }
});

var range = document.getElementById('range');
if(range)
    noUiSlider.create(range, {
     start: [0, 100],
     connect: true,
     step: 1,
     range: {
       'min': 0,
       'max': 100
   },
   format: wNumb({
       decimals: 0
   })
});
$(".comments_box").css("height", parseInt($('.news_box .text_new').height() + 220) + "px");
$('.photo_carusel').slick({
    infinite: true,
    speed: 300,
    autoplay: true,
    autoplaySpeed: 3500,
    slidesToShow: 5,
    centerMode: true,
    prevArrow: '<span type="button" class="slick-prev"><i class="fa fa-chevron-left"></i></span>',
    nextArrow: '<span type="button" class="slick-next"><i class="fa fa-chevron-right"></i></span>',
    variableWidth: true
});
$('.audioplayer').snd([{src : '../img/akcent_feat_reea__aza_-_phou_phou_(zv.fm).mp3', name: "Akcent"},
  {src : '../img/Jamala.mp3', name: "Jamala"},{src : 'http://kolber.github.io/audiojs/demos/mp3/01-dead-wrong-intro.mp3', name: "The xx Intro"}]);

$(document).on('click', '.btn_sample', function(event){
    event.preventDefault();
   $(this).next('.modal_info').slideToggle("fast");
});
$(document).on('click','.modal_info .close', function(){
    $(this).parents('.modal_info').slideToggle("fast");
});

var d = {
    labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
    datasets: [
    {
        label: "My First dataset",
        backgroundColor: "rgba(179,181,198,0.2)",
        borderColor: "rgba(179,181,198,1)",
        pointBackgroundColor: "rgba(179,181,198,1)",
        pointBorderColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgba(179,181,198,1)",
        data: [65, 59, 90, 81, 56, 55, 40]
    },
    {
        label: "My Second dataset",
        backgroundColor: "rgba(21,101,192,0.2)",
        borderColor: "rgba(21,101,192,1)",
        pointBackgroundColor: "rgba(21,101,192,1)",
        pointBorderColor: "#fff",
        pointHoverBackgroundColor: "#fff",
        pointHoverBorderColor: "rgba(21,101,192,1)",
        data: [28, 48, 40, 19, 96, 27, 100]
    }
    ]
};

var d2 = {
    labels : ["Январь","Февраль","Март","Апрель","Май","Июнь"],
    datasets : [
    {
        label: "My Second dataset",
        backgroundColor: "rgba(179,181,198,0.2)",
        fillColor : "rgba(172,194,132,0.4)",
        strokeColor : "#ACC26D",
        pointColor : "#fff",
        pointStrokeColor : "#9DB86D",
        data : [200,155,100,250,305,250]
    },
    {
        label: "My Second dataset",
        backgroundColor: "rgba(21,101,192,0.2)",
        fillColor : "rgba(172,14,132,0.4)",
        strokeColor : "#A2234D",
        pointColor : "#fff",
        pointStrokeColor : "#9D436D",
        data : [123,345,200,189,243,332]
    }
    ]
}
var d3 = {
    labels : ["Январь","Февраль","Март","Апрель","Май","Июнь"],
    datasets : [
    {
        label: "My Second dataset",
        backgroundColor: "rgba(179,181,198,0.2)",
        fillColor : "rgba(172,194,132,0.4)",
        strokeColor : "#ACC26D",
        pointColor : "#fff",
        pointStrokeColor : "#9DB86D",
        data : [200,155,100,250,305,250]
    }
    ]
}
/*var ctx1 = document.getElementById('chart1').getContext('2d');
var scatterChart = new Chart(ctx1, {
    type: 'line',
    data: d2
});
var ctx2 = document.getElementById('chart2').getContext('2d');
var scatterChart = new Chart(ctx2, {
    type: 'pie',
    data: d3
});
var ctx3 = document.getElementById('chart3').getContext('2d');
var scatterChart = new Chart(ctx3, {
    type: 'line',
    data: d,
    options: {
        scales: {
            xAxes: [{
                type: 'linear',
                position: 'bottom'
            }]
        }
    }
});
var ctx4 = document.getElementById('chart4').getContext('2d');
var scatterChart = new Chart(ctx4, {
    type: "bar",
    data: d
});
var ctx5 = document.getElementById('chart5').getContext('2d');
var scatterChart = new Chart(ctx5, {
    type: 'radar',
    data: d,
    options: {
        scale: {
            reverse: true,
            ticks: {
                beginAtZero: true
            }
        }
    }
});*/
});
function initStyleElements() {
    $('select').material_select();
}
/**
 *  Show notification message
 */
function notify(response) {
    var message, status;

    if(response.status == 500 && !response.data.message){
        message = response.data.message;
        status = 'error';
    } else if(response.request.method == 'GET' || !response.data.message) {
        return false;
    } else {
        message = response.data.message;
        status = response.data.status || 'success';
    }

    $('.ns-box').fadeOut(100);

    var notification = new NotificationFx({
        wrapper : document.body,
        message : '<p>' + message + '</p>',
        // layout type: growl|attached|bar|other
        layout : 'growl',
        effect : 'genie',
        // notice, warning, error, success
        type : 'warning',
        ttl : 30000,
        onClose : function() { return false; },
        onOpen : function() { return false; }
    });

    notification.show();
}

/**
 * Show preloader on div content
 */
function spinner(status, dellay) {
    if(status) {
        $('.loading').fadeIn(dellay || 1000);
    } else {
        $('.loading').fadeOut(dellay || 1000);
    }
}
