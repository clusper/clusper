module.exports = {
    rules: {
        title: {
            required: true,
            minlength: 5
        },
        description: {
            required: true,
            minlength: 10
        },
        price:{
            required: true,
            number: true
        }
    },
    errorElement : 'div',
    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    }
}