module.exports = {
    rules: {
        title: {
            required: true,
            minlength: 5
        },
        alias: {
            required: true,
            minlength: 5
        },
        description: {
            required: true,
            minlength: 10
        },
        company: {
            required: true
        },
        paypal_email: {
            required: true,
            email: true
        },
        phone: {
            required: true
        },
        country: {
            required: true
        },
        city: {
            required: true
        },

    },
    errorElement : 'div',
    errorPlacement: function(error, element) {
        var placement = $(element).data('error');
        if (placement) {
            $(placement).append(error)
        } else {
            error.insertAfter(element);
        }
    }
}