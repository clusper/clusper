import {router} from '../app'
import Vue from 'vue'
import bus from '../bus'

export default {

    user: {
        authenticated: false,
        data: false
    },

    login(credentials, redirect) {
        Vue.http.post('login', credentials).then((response) => {
            localStorage.setItem('authenticated', true)

            this.user.authenticated = true

            window.location.reload()

        }, (err) => {
            console.log(err)
        })
    },

    signup(credentials, redirect) {
        Vue.http.post('register', credentials).then((response) => {
            localStorage.setItem('authenticated', true)

            this.user.authenticated = true

            window.location.reload()

        }, (err) => {
            console.log(err)
        })
    },

    update(){
        Vue.http.get('users/me').then((response) => {
            if (response.data.data.user) {
                this.user.data = response.data.user
                bus.$emit('update-user', this.user.data)
            }
        }, (err) => {
            that.logout()
        })

    },

    logout() {
        Vue.http.get('logout').then((response) => {

        }, (err) => {
            console.log(err)
        })

        localStorage.removeItem('authenticated')

        this.user.authenticated = false
        this.user.data = null

        window.location.reload()
    },

    checkAuth() {
        var authenticated = localStorage.getItem('authenticated')
        if(authenticated) {
            this.user.authenticated = true
            this.getCurrentUser()
        } else {
            this.user.authenticated = false
        }
        return authenticated
    },
    getCurrentUserPromise() {
        var that = this;
        return new Promise((resolved, reject)=> {

            var authenticated = localStorage.getItem('authenticated')
            var user = that.user.data

            if (!user && authenticated) {
                Vue.http.get('users/me').then((response) => {
                    if (response.data.user) {
                        that.user.data = response.data.user
                        resolved(user);
                    }
                }, (err) => {
                    that.logout()
                    reject()
                })
            } else {
                resolved(user);
            }
        })
    },
    getCurrentUser() {
        return this.user.data
    }
}