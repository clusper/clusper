import Vue from 'vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import VueMoment from 'vue-moment'
import Header from './components/header/header.vue'
import Footer from './components/footer/footer.vue'
import Spinner from './components/spinner/spinner.vue'
import Auth from  './auth'
import {configRouter} from './routes'
import store from './vuex/store'
import config from './config'

Vue.use(VueResource)
Vue.use(VueRouter)
Vue.use(VueMoment)

Vue.component('header-component', Header);
Vue.component('footer-component', Footer);
Vue.component('spinner', Spinner);

Vue.config.debug = config.debug
Vue.http.options.root = config.api.base_url
Vue.http.options.credentials = true

Vue.http.interceptors.push({
    response(response) {
        notify(response)
        return response
    }
});


export var router = new VueRouter({
    //history: true,
    root: '/',
});

configRouter(router);

var App = Vue.extend({
    store,
});

Auth.getCurrentUserPromise().then((response) => {
    router.start(App, '#app');
})