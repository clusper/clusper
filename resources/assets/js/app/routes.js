import ProductUpload from './components/product/upload/products-upload.vue';
import ProfileSettings from './components/profile/settings/settings.vue';
import ProfileFeed from './components/profile/feed/index.vue';
import BrandroomProducts from './components/brandroom/products.vue';
import MyBrandroom from './components/brandroom/my-brandroom.vue';
import BrandroomSettings from './components/brandroom/settings.vue';
import BrandroomSales from './components/brandroom/sales.vue';
import Wishroom from './components/profile/wishroom/wishroom.vue';
import NewsFull from './components/brandroom/news/news-full.vue';
import BrandroomCreate from './components/brandroom/create.vue';
import News from './components/brandroom/news/news.vue';
import AddNews from './components/brandroom/news/add-news.vue';
import Brandroom from './components/brandroom/brandroom.vue';
import Category from './components/category/category.vue';
import Product from './components/product/product.vue';
import LoginPage from './components/pages/login.vue';
import Cart from './components/cart/cart.vue';
import ProfileAccount from './components/profile/account.vue';
import Download from './components/download/list.vue';
import ProfileProjects from './components/profile/projects/index.vue'
import ProjectsCreate from './components/profile/projects/create/create.vue'
import Workspace from './components/workspace/index.vue'
import ProjectsDetail from './components/profile/projects/detail.vue'
import HomePage from './components/home/index.vue'

import Auth from './auth';

module.exports = {

    configRouter(router) {

        router.map({
            '*': {
                component: {
                    template: '<div class="container"><h3>404. Not Found!</h3></div>'
                }
            },
            '/': {
                component: HomePage,
            },
            '/404': {
                component: {
                    template: '<div class="container"><h3>404. Not Found!</h3></div>'
                }
            },
            '/:brandroom': {
                name: 'brandroom',
                component: Brandroom
            },
            '/:brandroom/products': {
                name: 'brandroom-products',
                component: BrandroomProducts
            },
            '/:brandroom/:uuid': {
                name: 'product', //
                component: Product
            },
            '/brandroom/create': {
                name: 'create-brandroom',
                component: BrandroomCreate,
                brandroom: false,
                auth: true
            },
            '/brandroom': {
                name: 'brandroom-sales',
                component: MyBrandroom,
                brandroom: true,
                auth: true
            },
            '/brandroom/products': {
                name: 'my-brandroom-products',
                component: MyBrandroom,
                brandroom: true,
                auth: true
            },
            '/brandroom/products/upload':{
                name: 'brandroom-upload-product',
                component: MyBrandroom,
                brandroom: true,
                auth: true
            },
            '/brandroom/products/update/:uuid':{
                name: 'brandroom-update-product',
                component: MyBrandroom,
                brandroom: true,
                auth: true
            },
            '/brandroom/news': {
                name: 'brandroom-news',
                component: MyBrandroom,
                brandroom: true,
                auth: true
            },
            '/brandroom/news/add': {
                name: 'addnews',
                component: MyBrandroom,
                brandroom: true,
                auth: true
            },
            '/brandroom/news/:newsid': {
                name: 'my-newsfull',
                component: MyBrandroom,
                brandroom: true,
                auth: true
            },
            '/:brandroom/news/:newsid': {
                name: 'newsfull',
                component: Brandroom
            },
            '/brandroom/settings': {
                name: 'brandroom-settings',
                component: MyBrandroom,
                brandroom: true,
                auth: true
            },
            '/brandroom/projects': {
                name: 'brandroom-projects',
                component: MyBrandroom,
                brandroom: true,
                auth: true
            },
            '/profile/settings': {
                name: 'profile-settings',
                component: ProfileSettings,
                auth: true
            },
            '/profile/account': {
                name: 'profile-account',
                component: ProfileAccount,
                auth: true
            },
            '/profile/projects': {
                name: 'profile-projects',
                component: ProfileProjects,
                auth: true
            },
            '/projects/create': {
                name: 'projects-create',
                component: ProjectsCreate,
                auth: true
            },
            '/projects/create/:brandroom': {
                name: 'projects-create-brandroom',
                component: ProjectsCreate,
                auth: true
            },
            '/profile/feed': {
                name: 'profile-feed',
                component: ProfileFeed,
                auth: true
            },
            '/profile/download': {
                name: 'profile-download',
                component: Download,
                auth: true
            },
            '/projects/:id': {
                name: 'projects-detail',
                component: ProjectsDetail,
                auth: true
            },
            '/workspaces/:id': {
                name: 'workspace',
                component: Workspace,
                auth: true,
                workspace: true
            },
            '/cart': {
                name: 'cart',
                component: Cart
            },
            '/products/upload': {
                name: '',
                component: ProductUpload,
                auth: true
            },
            '/wishroom': {
                name: 'wishroom',
                component: Wishroom,
                auth: true
            },
            '/templates': {
                name: 'templates',
                component: Category
            },
            '/libraries': {
                name: 'libraries',
                component: Category
            },
            '/graphics': {
                name: 'graphics',
                component: Category
            },
            '/photo': {
                name: 'photo',
                component: Category
            },
            '/ebooks': {
                name: 'ebooks',
                component: Category
            },
            '/audiobooks': {
                name: 'audiobooks',
                component: Category
            },
            '/music': {
                name: 'music',
                component: Category
            },
            '/login': {
                component: LoginPage,
                guest: true
            },
        })

        router.beforeEach((transition) => {
            var authenticated = localStorage.getItem('authenticated')
            var user = Auth.user.data || {}

            if (transition.to.path === '/logout') {
                Auth.logout();
                transition.redirect('/login');
            }

            if (transition.to.auth) {
                if (!authenticated) {
                    router.go('/login')
                }
            }

            if (transition.to.guest) {
                if (authenticated) {
                    router.go('/wishroom')
                }
            }

            if (transition.to.brandroom) {
                if (!user.brandroom) {
                    router.go('/brandroom/create')
                }
            }

            if (transition.to.brandroom === false) {
                if (user.brandroom) {
                    router.go('/brandroom/settings')
                }
            }
            /*if (transition.to.workspace) {
                if (['user', 'brandroom'].indexOf(router.params.type) == -1) {
                    //router.go('/404')
                }
            }*/

            window.scrollTo(0, 0)
            spinner(true)
            transition.next()
            spinner(false)

        });
    }
}
