import {
    UPDATE_CART
} from '../mutation-types'

// initial state
const state = {
    count: 0,
    total: 0,
    products: []
}

// mutations
const mutations = {
    [UPDATE_CART] (state, cart) {
        if(cart) {
            state.count = cart.count || 0;
            state.total = cart.total || 0;
            state.products = cart.products || [];
            localStorage.setItem('cart', JSON.stringify(cart));
        }
    },
}

export default {
    state,
    mutations
}