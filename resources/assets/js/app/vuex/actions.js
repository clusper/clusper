import Vue from 'vue'
import * as types from './mutation-types'

export const addToCart = ({ dispatch }, product) => {
    Vue.http.post('cart', {product_id: product.id}).then((response) => {
        dispatch(types.UPDATE_CART, response.data.data.cart)
    }, (err) => {
        console.log(err)
    });
}

export const updateCart = ({ dispatch }, cart) => dispatch(types.UPDATE_CART, cart)

