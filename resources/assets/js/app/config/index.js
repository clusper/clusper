var env = process.env.APP_ENV || 'development'

var config = {
    'c9': require('./c9.config'),
    'development': require('./development.config'),
    'production': require('./production.config'),
}

module.exports = config[env]