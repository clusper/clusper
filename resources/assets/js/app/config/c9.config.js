var config = {
    env: 'c9',
    debug: true,
    api: {
        base_url: 'https://laravel52-nazarp.c9users.io:8081/api/v1'
    },
    social: {
        facebook: {},
        twitter: {},
        google: {},
    },
}
module.exports = config