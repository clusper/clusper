var config = {
    env: 'production',
    debug: false,
    api: {
        base_url: 'http://api.clusper.com/api/v1'
    },
    social: {
        facebook: {},
        twitter: {},
        google: {},
    },
}
module.exports = config