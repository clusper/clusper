<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function ($faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => str_random(10),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Product::class, function ($faker) {
    return [
        'title' => $faker->company,
        'description' => $faker->paragraph,
        'alias' => str_slug($faker->company),
        'category_id' => 1,
        'status' => 1,
        'visible' => 1,
    ];
});
