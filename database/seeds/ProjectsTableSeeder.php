<?php

use App\Project;
use Illuminate\Database\Seeder;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        foreach ((range(1, 100)) as $index) {
            Project::create([
                'title' => $faker->catchPhrase,
                'description' => $faker->text(200),
                'category_id' => \App\Category::orderByRaw(\DB::raw('RAND()'))->first()->id,
                'price' => $faker->numberBetween(1, 300),
                'customer_id' => \App\User::orderByRaw(\DB::raw('RAND()'))->first()->id,
                'status' => $faker->randomElement(['open', 'personal', 'in-progress', 'completed'])
            ]);
        }
    }
}
