<?php

use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries = ['Ukraine', 'USA', 'Canada', 'Italy', 'Japan', 'New Zeland'];
        foreach($countries as $country){
            \App\Country::create([
                'country' => $country
            ]);
        }
    }
}
