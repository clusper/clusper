<?php

use App\Product;
use App\Brandroom;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		// use the factory to create a Faker\Generator instance
		$faker = Faker\Factory::create();
		foreach ((range(1, 200)) as $index) {
			Brandroom::create([
				'title' => $faker->name,
				'user_id' => rand(1, 20),
				'alias' => str_slug($faker->name),
			]);

			Product::create([
				'title' => $faker->name,
                'uuid' => Uuid::generate(4),
				'category_id' => rand(9,14),
				'status' => 1,
				'active' => 1,
				'brandroom_id' => Brandroom::orderByRaw(\DB::raw('RAND()'))->first()->id,
				'price' => rand(1, 200)
			]);
		}
    }
}
