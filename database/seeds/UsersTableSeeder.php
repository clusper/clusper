<?php

use App\User;
use App\UserPaymentData;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // use the factory to create a Faker\Generator instance
        $faker = Faker\Factory::create();
        foreach ((range(1, 20)) as $index) {
            $user = User::create([
                'name' => $faker->name,
                'balance' => 100,
                'password' => bcrypt(123),
                'email' => $faker->email,
            ]);

            UserPaymentData::create([
                'user_id' => $user->id
            ]);
        }

//        foreach ((range(1, 20)) as $index) {
//            \App\Follower::create([
//                'follower' => \App\User::orderByRaw(\DB::raw('RAND()'))->first()->id,
//                'followed' => \App\Brandroom::orderByRaw(\DB::raw('RAND()'))->first()->id,
//            ]);
//        }

    }
}
