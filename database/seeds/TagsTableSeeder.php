<?php

use App\Tag;
use App\Product;
use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // use the factory to create a Faker\Generator instance
        $faker = Faker\Factory::create();
        foreach ((range(1, 20)) as $index) {
            $tag = Tag::create([
                'name' => $faker->word,
                'alias' => $faker->word,
            ]);
        }
        for($i = 0; $i< 100; $i++) {
            DB::table('product_tags')->insert([
                'product_id' => Product::orderByRaw(\DB::raw('RAND()'))->first()->id,
                'tag_id' => Tag::orderByRaw(\DB::raw('RAND()'))->first()->id
            ]);
        }

    }
}
