<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comment_status', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
        });

        DB::table('comment_status')->insert([
            [
                'title' => 'Status 1'
            ],
            [
                'title' => 'Status 2'
            ],
            [
                'title' => 'Status 3'
            ],
        ]);

        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->text('text');
            $table->integer('user_id')->unsigned()->index();
            $table->integer('commentable_id');
            $table->string('commentable_type');
            $table->integer('status_id')->default(1)->unsigned()->index();
            $table->integer('parent_id')->default(0)->index();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('status_id')->references('id')->on('comment_status')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comments');
        Schema::drop('comment_status');
    }
}
