<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryAttributesTable extends Migration
{
    /**
     * Run the migrations for create automatic product form
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attribute_id')->unsigned()->index();
            $table->integer('category_id')->unsigned()->index();
            $table->string('width')->default(2);

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });

        $category_attributes = [
            [
                'attribute_id' => 1,
                'category_id' => 9,
            ],
            [
                'attribute_id' => 2,
                'category_id' => 9,
            ],
            [
                'attribute_id' => 3,
                'category_id' => 10,
            ]
        ];
        foreach($category_attributes as $category){
            DB::table('category_attributes')->insert($category);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('category_attributes');
    }
}
