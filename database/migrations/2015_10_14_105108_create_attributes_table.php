<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     * All attributes of products(price, artist, genre)
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('name');
            $table->integer('sort');
            $table->integer('visible');
            $table->integer('filter')->default(1);
            $table->integer('type_id')->unsigned()->index();
            $table->timestamps();

            $table->foreign('type_id')->references('id')->on('attribute_types')->onDelete('cascade');
        });

        Schema::create('attribute_options', function (Blueprint $table) {
            $table->increments('id');
            $table->string('value');
            $table->integer('attribute_id')->unsigned()->index(); // parent
            $table->integer('sort');
            $table->integer('visible');
            $table->timestamps();

            $table->foreign('attribute_id')->references('id')->on('attributes')->onDelete('cascade');
        });

        $attributes = [
            [
                'title' => 'Genre',
                'name' => 'genre',
                'sort' => '1',
                'visible' => '1',
				'filter' => '1',
                'type_id' => '4',
            ],
            [
                'title' => 'Artist',
                'name' => 'artist',
                'sort' => '1',
                'visible' => '0',
                'filter' => '0',
                'type_id' => '1',
            ],
            [
                'title' => 'Country',
                'name' => 'country',
                'sort' => '1',
                'visible' => '1',
				'filter' => '1',
                'type_id' => '2',
            ],
            [
                'title' => 'Format',
                'name' => 'format',
                'sort' => '1',
                'visible' => '1',
				'filter' => '1',
                'type_id' => '5',
            ],
        ];

        $attribute_options = [
            [
                'value' => 'Rock',
                'attribute_id' => '1',
                'sort' => '1',
                'visible' => '1',
            ],
            [
                'value' => 'Pop',
                'attribute_id' => '1',
                'sort' => '1',
                'visible' => '1',
            ],
            [
                'value' => 'Drum and bass',
                'attribute_id' => '1',
                'sort' => '1',
                'visible' => '1',
            ],
            [
                'value' => 'Trance',
                'attribute_id' => '1',
                'sort' => '1',
                'visible' => '1',
            ],
            [
                'value' => 'Ukraine',
                'attribute_id' => '3',
                'sort' => '1',
                'visible' => '1',
            ],
            [
                'value' => 'England',
                'attribute_id' => '3',
                'sort' => '1',
                'visible' => '1',
            ],
            [
                'value' => 'MP3',
                'attribute_id' => '4',
                'sort' => '1',
                'visible' => '1',
            ],
            [
                'value' => 'WAV',
                'attribute_id' => '4',
                'sort' => '1',
                'visible' => '1',
            ],
            [
                'value' => 'MIDI',
                'attribute_id' => '4',
                'sort' => '1',
                'visible' => '1',
            ],
        ];
        foreach($attributes as $attribute){
            DB::table('attributes')->insert($attribute);
        };

        foreach($attribute_options as $attribute_option){
            DB::table('attribute_options')->insert($attribute_option);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attribute_options');
        Schema::drop('attributes');
    }
}
