<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandroomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brandrooms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('alias');
            $table->text('description');
            $table->integer('user_id')->unsigned()->index();
            $table->string('city');
            $table->integer('country');
            $table->string('phone');
            $table->string('company');
            $table->string('paypal_email');
            $table->string('background');
            $table->string('background_key');
            $table->string('logo');
            $table->string('logo_key');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('brandrooms');
    }
}
