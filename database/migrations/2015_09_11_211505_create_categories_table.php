<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 100);
            $table->string('description', 200);
            $table->string('alias', 50);
            $table->string('keywords', 100);
            $table->integer('sort')->default(0);
            $table->integer('parent')->default(0);
            $table->timestamps();
        });

        $categories = [
            [
                'title' => 'Music',
                'description' => 'This is a Music',
                'alias' => 'music',
                'keywords' => 'Music',
                'sort' => 1,
            ],
            [
                'title' => 'Audiobooks',
                'description' => 'This is a audiobook',
                'alias' => 'audiobooks',
                'keywords' => 'audiobooks',
                'sort' => 2,
            ],
            [
                'title' => 'E-books',
                'description' => 'This is a books',
                'alias' => 'ebooks',
                'keywords' => 'ebooks',
                'sort' => 3,
            ],
            [
                'title' => 'Photo',
                'description' => 'This is a photo',
                'alias' => 'photo',
                'keywords' => 'photo',
                'sort' => 4,
            ],
            [
                'title' => 'Graphics',
                'description' => 'This is a graphics',
                'alias' => 'graphics',
                'keywords' => 'graphics',
                'sort' => 5,
            ],
            [
                'title' => 'Libraries',
                'description' => 'This is a libraries',
                'alias' => 'libraries',
                'keywords' => 'libraries',
                'sort' => 6,
            ],
            [
                'title' => 'Templates',
                'description' => 'This is a templates',
                'alias' => 'templates',
                'keywords' => 'templates',
                'sort' => 7,
            ],
            [
                'title' => 'Effects',
                'description' => 'This is a effects',
                'alias' => 'effects',
                'keywords' => 'effects',
                'sort' => 8,
            ],


            // Templates
            [
                'title' => 'WordPress',
                'description' => '',
                'alias' => 'wp',
                'keywords' => '',
                'sort' => 1,
                'parent' => 1
            ],
            [
                'title' => 'HTML',
                'description' => '',
                'alias' => 'HTML',
                'keywords' => '',
                'sort' => 1,
                'parent' => 1
            ],
            [
                'title' => 'Marketing',
                'description' => '',
                'alias' => 'Marketing',
                'keywords' => '',
                'sort' => 1,
                'parent' => 1
            ],

            // Libraries
            [
                'title' => 'php',
                'description' => '',
                'alias' => 'php',
                'keywords' => '',
                'sort' => 1,
                'parent' => 6
            ],
            [
                'title' => 'JavaScript',
                'description' => '',
                'alias' => 'js',
                'keywords' => '',
                'sort' => 1,
                'parent' => 6
            ],
            [
                'title' => 'CSS',
                'description' => '',
                'alias' => 'css',
                'keywords' => '',
                'sort' => 1,
                'parent' => 6
            ],


        ];

        foreach($categories as $category){
            DB::table('categories')->insert($category);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
    }
}
