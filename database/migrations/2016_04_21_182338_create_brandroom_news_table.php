<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandroomNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brandroom_news', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brandroom_id')->unsigned()->index();
            $table->text('news');
            $table->timestamps();

            $table->foreign('brandroom_id')->references('id')->on('brandrooms')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('brandroom_news');
    }
}
