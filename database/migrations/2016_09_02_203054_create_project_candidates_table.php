<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_candidates', function (Blueprint $table) {
            $table->unsignedInteger('brandroom_id')->nullable();
            $table->unsignedInteger('project_id')->nullable();

            $table->foreign('brandroom_id')->references('id')->on('brandrooms')->onDelete('cascade');
            $table->foreign('project_id')->references('id')->on('projects')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('project_candidates');
    }
}
