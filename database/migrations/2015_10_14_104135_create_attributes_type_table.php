<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributesTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name'); //text, checkbox, date
        });


        $attribute_types = [
            [
                'name' => 'text'
            ],
            [
                'name' => 'checkbox'
            ],
            [
                'name' => 'date'
            ],
            [
                'name' => 'selectbox'
            ],
            [
                'name' => 'radio'
            ],
            [
                'name' => 'file'
            ],
            [
                'name' => 'number'
            ],
        ];

        foreach($attribute_types as $attribute_type){
            DB::table('attribute_types')->insert($attribute_type);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attribute_types');
    }
}
