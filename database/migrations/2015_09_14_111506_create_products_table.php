<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->string('url', 255);
            $table->string('title', 100);
            $table->string('description', 200);
            $table->string('alias', 50);
            $table->float('price_seller', 50);
            $table->float('commission', 50);
            $table->float('price', 50);
            $table->integer('category_id')->unsigned()->index();
            $table->integer('brandroom_id')->unsigned()->index();
            $table->string('status')->default(0);
            $table->string('active')->default(0);
            $table->string('deleted')->default(0);
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            //$table->foreign('storage_id')->references('id')->on('storages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
