<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('news_id')->unsigned()->nullable()->index();
            $table->integer('product_id');
            $table->timestamps();

            $table->foreign('news_id')->references('id')->on('brandroom_news')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news_products');
    }
}
