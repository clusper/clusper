# Clusper service

## Installation

``` bash
# install npm
npm install
# install composer
composer install
# run server with hot reload
npm run dev
# run gulp
gulp watch
```

## Usage

### Create Page
- Create file in components folder

  ``` bash
  # user.vue
  <template>
    <h3>User Lists</h3>
  </template>

  <style>
    // style here
  </style>

  <script>
    // script here
  </script>
  ```
- Register route component in **resources/assets/js/routes.js**

  ``` bash
  router.map({
    '/user': {
      component: require('./components/user.vue')
    }
  });

  ```
