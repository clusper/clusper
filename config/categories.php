<?php

return [


    'music' => [

        'title' => 'Music',
        'alias' => 'music',

    ],
    
    'graphics' => [

        'title' => 'Graphics',
        'alias' => 'graphics',

    ],
    
    'audiobooks' => [

        'title' => 'Audiobooks',
        'alias' => 'audiobooks',

    ],
    
    'libraries' => [

        'title' => 'Libraries',
        'alias' => 'libraries',

    ],
    
    'ebooks' => [

        'title' => 'E-Books',
        'alias' => 'ebooks',

    ],
    
    'templates' => [

        'title' => 'Templates',
        'alias' => 'templates',

    ],
    
    'photos' => [

        'title' => 'Photos',
        'alias' => 'photos',

    ],
    
    'effects' => [

        'title' => 'Effects',
        'alias' => 'effects',

    ],
    

];
