<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',
    
    'default_user_avatar' => '/img/default_photo.png',
    'default_brandroom_background' => '/img/default_brandroom_header.jpg',
    'default_brandroom_logo' => 'http://www.youthedesigner.com/wp-content/uploads/2012/06/full_ps.jpg'

);
