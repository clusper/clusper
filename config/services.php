<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => '',
        'secret' => '',
    ],

    'mandrill' => [
        'secret' => '',
    ],

    'ses' => [
        'key'    => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'secret' => env('STRIPE_SECRET'),
    ],
    
    'facebook' => [
        'client_id' => '509885752518617',
        'client_secret' => 'daba9f8451e1e603aa036889b31868cd',
        'redirect' => 'http://cloudpire-nazar-pinchuk.c9users.io/login/facebook/call',
    ],
    
    'google' => [
        'client_id' => '90673706873-648rajppck50r08jd31d8rimsnpmnhgm.apps.googleusercontent.com',
        'client_secret' => 'B-FYB6cQ3KSetzFrNq7vR43w',
        'redirect' => 'http://cloudpire-nazar-pinchuk.c9users.io/login/google/call',
    ],

];
