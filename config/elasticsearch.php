<?php

return array(
    'hosts' => array(
                    'localhost:9200'
                    ),
    'logPath' => storage_path('logs/elasticsearch.log'),
    
    "number_of_shards" => 1,
    "number_of_replicas"=> 0,
    "analysis"=> [
        "filter"=> [
            "autocomplete_filter"=> [
                "type"=>     "edge_ngram",
                "min_gram"=> 2,
                "max_gram"=> 15
            ]
        ],
        "analyzer"=> [
            "autocomplete"=> [
                "type"=>      "custom",
                "tokenizer"=> "standard",
                "filter"=> [
                    "lowercase",
                    "autocomplete_filter" 
                ]
            ]
        ]
    ],
    'custom' => [
        'main_index' => 'market'
        ]
);