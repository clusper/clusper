<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Cloudinary API configuration
    |--------------------------------------------------------------------------
    |
    | Before using Cloudinary you need to register and get some detail
    | to fill in below, please visit cloudinary.com.
    |
    */

    'cloudName'  => 'cloudpire',
    'baseUrl'    => 'http://res.cloudinary.com/cloudpire',
    'secureUrl'  => 'https://res.cloudinary.com/cloudpire',
    'apiBaseUrl' => 'https://api.cloudinary.com/v1_1/cloudpire',
    'apiKey'     => '618374347956355',
    'apiSecret'  => 'mPNGS4_X0gDynxkJTkItBBzzn7Q',

    'scaling'    => array(
        'format' => 'png',
        'width'  => 150,
        'height' => 150,
        'crop'   => 'fit',
        'effect' => null
    ),
    
    'folders'    => array(
        'avatar' => 'avatar',
        'product'=> 'product',
        'news'   => 'news',
        'logo'   => 'storage/logo',
        'background'   => 'storage/background',
    ),
    
    'key_length' => 10

);
