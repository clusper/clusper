<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $table = 'messages';

    protected $fillable = ['text', 'chat_id'];

    /**
     * Message has the chat
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function chat()
    {
        return $this->belongsTo('App\Chat');
    }

    /**
     * Message has detail
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projectMessage()
    {
        return $this->hasOne('App\ProjectMessage');
    }


    /**
     * Message has project message media
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projectMessageMedia()
    {
        return $this->hasMany('App\ProjectMessageMedia');
    }
}
