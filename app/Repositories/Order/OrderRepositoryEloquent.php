<?php

namespace App\Repositories\Order;

use App\Order;
use App\Product;
use App\User;
use DB;

class OrderRepositoryEloquent implements OrderRepository
{
    public function buy(Product $product, User $user)
    {
        $order = Order::create([
            'product_id' => $product->id,
            'buyer_id' => $user->id,
            'brandroom_id' => $product->brandroom_id,
            'seller_profit' => $product->price_seller,
            'price' => $product->price,
        ]);

        return $order;
    }
}