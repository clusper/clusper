<?php

namespace App\Repositories\Order;

use App\Product;
use App\User;

interface OrderRepository
{
    public function buy(Product $product, User $user);
}