<?php

namespace App\Repositories\Project;

use App\Chat;
use App\Project;
use App\User;
use App\Brandroom;
use App\Workspace;
use Illuminate\Database\Eloquent\Model;

interface ProjectRepository
{
    public function findById($id);

    public function findOpen();

    public function findByBrandroom(Brandroom $brandroom, $status = null);

    public function findByCustomer(User $user, $status = null);

    public function create($project);

    public function delete(Project $project);

    public function apply(Project $project, Brandroom $brandroom);

    public function cancelApply(Project $project, Brandroom $brandroom);

    public function approve(Project $project, Brandroom $brandroom);

    public function decline(Project $project);

    public function createWorkspace(Project $project, Brandroom $brandroom, Chat $chat);

    public function productUpload(Workspace $workspace, $product);

    public function setStatus(Project $project, $status);

}