<?php

namespace App\Repositories\Project;

use App\Brandroom;
use App\Chat;
use App\Project;
use App\ProjectMedia;
use App\Repositories\Chat\ChatRepository;
use App\User;
use App\Workspace;
use Illuminate\Database\Eloquent\Model;
use Auth;

class ProjectRepositoryEloquent implements ProjectRepository
{

    /**
     * @var Project
     */
    private $project;

    /**
     * @var User|null
     */
    private $user;

    public function __construct(Project $project)
    {
        $this->project = $project;

        $this->user = Auth::user();
    }

    /**
     * Find projects by id
     *
     * @param User $user
     * @return mixed
     */
    public function findById($id)
    {
        return $this->project->findOrFail($id);
    }

    /**
     * Find open projects
     *
     * @return mixed
     */
    public function findOpen()
    {
        return $this->project->where('status', 'open')->get();
    }


    /**
     * Find projects by brandroom
     *
     * @param Brandroom $brandroom
     * @param null $status
     * @return mixed
     */
    public function findByBrandroom(Brandroom $brandroom, $status = null)
    {
        $project = $this->project;

        if($status) {
            $project = $project->where('status', $status);
        }

        return $project->whereHas('workspace', function($query) use ($brandroom) {
            $query->where('brandroom_id', $brandroom->id);
        })->get();

    }

    /**
     * Find projects by customer
     *
     * @param User $user
     * @return mixed
     */
    public function findByCustomer(User $user, $status = null)
    {
        $project = $this->project;

        if($status) {
            $project = $project->where('status', $status);
        }

        return $project->where('customer_id', $user->id)->get();
    }

    /**
     * Create a project
     *
     * @param $project
     * @return Model
     */
    public function create($project)
    {
        $files = $project['files'];

        $project = $this->user->projects()->create($project);

        foreach ($files as $file) {
            $file = $project->media()->create($file);

            $this->user->projectMedia()->save($file);
        }

        return $project;
    }

    /**
     * Delete the project
     *
     * @param Project $project
     * @return bool|null
     */
    public function delete(Project $project)
    {
        return $project->delete();
    }

    /**
     * Apply the project by brandroom
     *
     * @param Project $project
     * @param Brandroom $brandroom
     * @return Project
     */
    public function apply(Project $project, Brandroom $brandroom)
    {
        $project->candidates()->attach($brandroom);

        return $project;
    }

    /**
     * Cancel Apply the project
     *
     * @param Project $project
     * @param Brandroom $brandroom
     * @return Project
     */
    public function cancelApply(Project $project, Brandroom $brandroom)
    {
        $project->candidates()->detach($brandroom);

        return $project;
    }

    /**
     * Approve the personal project
     *
     * @param Project $project
     * @param Brandroom $brandroom
     * @return Project
     */
    public function approve(Project $project, Brandroom $brandroom)
    {
        $this->setStatus($project, Project::STATUS_IN_PROGRESS);

        return $project;
    }

    /**
     * Decline the personal project
     *
     * @param Project $project
     * @return Project
     */
    public function decline(Project $project)
    {
        $project->workspace()->delete();

        return $project;
    }

    /**
     * Create a new workspace
     *
     * @param Project $project
     * @param Brandroom $brandroom
     * @param Chat $chat
     * @return Model
     */
    public function createWorkspace(Project $project, Brandroom $brandroom, Chat $chat)
    {
        return $project->workspace()->create([
            'brandroom_id' => $brandroom->id,
            'chat_id' => $chat->id,
        ]);
    }

    /**
     * Upload workspace product
     *
     * @param Workspace $workspace
     * @param $product
     * @return Model
     */
    public function productUpload(Workspace $workspace, $product)
    {
        return $workspace->products()->create($product['files'][0]);
    }

    /**
     * Set project status
     *
     * @param Project $project
     * @param $status
     * @return bool|int
     */
    public function setStatus(Project $project, $status)
    {
        return $project->update(['status' => $status]);
    }

}