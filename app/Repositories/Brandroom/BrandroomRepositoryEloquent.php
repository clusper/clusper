<?php

namespace App\Repositories\Brandroom;

use App\Brandroom;

class BrandroomRepositoryEloquent implements BrandroomRepository
{
    /**
     * @var Brandroom
     */
    protected $brandroom;

    public function __construct(Brandroom $brandroom)
    {
        $this->brandroom = $brandroom;
    }

    /**
     * Get all brandrooms
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function all() {
        return $this->brandroom->all();
    }
}