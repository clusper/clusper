<?php

namespace App\Repositories\User;

use App\User;

class UserRepositoryEloquent implements UserRepository
{

    /**
     * @var User
     */
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get user by id
     *
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->user->find($id);
    }

    /**
     * Get all users
     *
     * @return mixed
     */
    public function all()
    {
        return $this->user->paginate(10);
    }

    /**
     * Get user followers
     *
     * @param $uuid
     * @return mixed
     */
    public function getFollowers(User $user)
    {
        return $user->followers;
    }

    /**
     * Update user balance
     *
     */
    public function updateUserBalance(User $user, $amount, $status)
    {
        if($status == 'in')
        {
            $user->balance = $user->balance + $amount;
        }else{
            $user->balance = $user->balance - $amount;
        }
        $user->save();
        return $user->balance;
    }

    /**
     * Update user payment history
     *
     */
    public function updateUserPaymentHistory(User $user, $amount)
    {
        $user->payment()->create([
            'amount' => $amount
        ]);

        return $user->payment();
    }
}