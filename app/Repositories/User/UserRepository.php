<?php

namespace App\Repositories\User;

use App\User;

interface UserRepository
{
    public function find($id);

    public function all();

    public function getFollowers(User $user);

    public function updateUserBalance(User $user, $amount, $status);

    public function updateUserPaymentHistory(User $user, $amount);

}