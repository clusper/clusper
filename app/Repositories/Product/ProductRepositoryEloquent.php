<?php

namespace App\Repositories\Product;

use App\Brandroom;
use App\Product;
use App\User;

class ProductRepositoryEloquent implements ProductRepository
{

    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Get product by uuid
     *
     * @param $uuid
     * @return mixed
     */
    public function findByUuid($uuid)
    {
        $product = $this->getProducts()->where('uuid', $uuid)->firstOrFail();

        $options = $product->options->pluck('id');

        $product = $product->load(['attributes.options' => function($query) use ($options) {
            $query->whereIn('id', $options);
        }, 'brandroom', 'tags']);

        return $product;
    }

    /**
     * Get all products
     *
     * @param $category_id
     * @return $this
     */
    public function all( $category = false, $request) {

        $products = $this->getProducts();

        $this->filterCategory($category, $request, $products);

        $this->filterPrice($request, $products);

        $this->filterAttributes($request, $products);

        $this->filterTags($request, $products);

        $this->sortProducts($request, $products);

        return $products;
    }

    public function brandroomProducts(Brandroom $brandroom)
    {
        return $this->getProducts()->where('brandroom_id', $brandroom->id);
    }

    /**
     * Products list for news
     *
     * @param $request
     * @param $products
     * @return mixed
     */
    public function productsListForNews($brandroom_id)
    {
        return $this->product->where('brandroom_id', $brandroom_id);
    }

    /**
     * Filter products by price
     *
     * @param $request
     * @param $products
     * @return mixed
     */
    public function filterPrice($request, $products)
    {
        if ($request->has('price')) {
            $price = $request->get('price');
            if ($price['min'])
                $products->where('products.price', '>=', $price['min']);
            if ($price['max'])
                $products->where('products.price', '<=', $price['max']);
        }

        return $products;
    }

    /**
     * Filter products by attributes
     *
     * @param $request
     * @param $products
     * @return mixed
     */
    public function filterAttributes($request, $products)
    {
        if ($request->has('attr')) {

            foreach ($request->get('attr') as $attribute => $options) {
                if (empty($options[0])) continue;

                $products->join('product_attributes as pa' . $attribute, function ($join) use ($attribute) {
                    $join->on('pa' . $attribute . '.product_id', '=', 'products.id');
                });

                $where = 'where';

                $products->where(function ($query) use ($attribute, $options, $where) {

                    foreach ($options as $option) {

                        $query->{$where}(function ($query) use ($attribute, $option) {
                            $query->where('pa' . $attribute . '.attribute_id', '=', $attribute);
                            $query->where('pa' . $attribute . '.attribute_option_id', '=', $option);
                        });

                        $where = 'orWhere';
                    };
                });
            };
        };

        return $products;
    }


    /**
     * Sort products
     *
     * @param $request
     * @param $products
     * @return mixed
     */
    public function sortProducts($request, $products)
    {
        switch ($request->get('sort')) {
            case 'price_low':
                $sort_filed = 'products.price';
                $sort_type = 'asc';
                break;
            case 'price_high':
                $sort_filed = 'products.price';
                $sort_type = 'desc';
                break;
            case 'new':
                $sort_filed = 'products.created_at';
                $sort_type = 'desc';
                break;
            case 'old':
                $sort_filed = 'products.created_at';
                $sort_type = 'asc';
                break;
            default:
                $sort_filed = 'products.id';
                $sort_type = 'asc';
        }

        return $products->orderBy($sort_filed, $sort_type);
    }

    /**
     * Filter products by tags
     *
     * @param $request
     * @param $products
     * @return mixed
     */
    public function filterTags($request, $products)
    {
        if ($request->has('tags')) {
            $tags = explode(',', $request->get('tags'));

            $products->whereHas('tags', function ($query) use ($tags) {
                $query->whereIn('tag_id', $tags);
            });
        }

        return $products;
    }

    /**
     * Filter products by category
     *
     * @param $category
     * @param $request
     * @param $products
     */
    public function filterCategory($category, $request, $products)
    {
        if ($request->subcategory) {
            $products->where(['category_id' => $request->subcategory]);
        }
        if ($category) {
            $products->whereIn('category_id', $category->children()->pluck('id'));
        }
    }

    /**
     * Get available products
     *
     * @return $this
     */
    public function getProducts()
    {
        return $this->product->with(['tags', 'brandroom'])->where(['active' => 1, 'status' => 1]);
    }


    /**
     * Get user viewed products
     *
     * @param $uuid
     * @return mixed
     */
    public function viewedProducts(User $user)
    {
        return $user->viewedProducts->load('brandroom');
    }
}