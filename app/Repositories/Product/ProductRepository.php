<?php

namespace App\Repositories\Product;

use App\Brandroom;
use App\User;

interface ProductRepository
{
    public function findByUuid($uuid);

    public function all($category, $request);

    public function brandroomProducts(Brandroom $brandroom);

    public function viewedProducts(User $user);
    
    public function productsListForNews($brandroom_id);

}