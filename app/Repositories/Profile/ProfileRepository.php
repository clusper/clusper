<?php

namespace App\Repositories\Profile;

interface ProfileRepository
{
    public function addStripeId($user_id, $stripe_id);

    public function addCard($user_id, $card);
}