<?php

namespace App\Repositories\Profile;

use App\User;
use App\UserPayment;
use App\UserPaymentData;
use Auth;

class ProfileRepositoryEloquent implements ProfileRepository
{
    public function addStripeId($user_id, $stripe_id)
    {
        $user = UserPaymentData::where('user_id', $user_id)->first();
        $user->stripe_id = $stripe_id;
        $user->save();
        return true;
    }

    public function addCard($user_id, $card)
    {
        $user = UserPaymentData::where('user_id', $user_id)->first();
        $user->card_type = $card['brand'];
        $user->card_four = $card['last4'];
        $user->save();
        return true;
    }
}