<?php

namespace App\Repositories\Message;

use App\Chat;
use App\Message;
use App\User;
use Illuminate\Database\Eloquent\Model;

interface MessageRepository
{
    public function findById($id);
    
    public function findByUser(User $user);

    public function findByChat(Chat $chat);

    public function create(Chat $chat, $message);

    public function read(Message $message);

    public function unread(Message $message);

    public function delete(Message $message);

}