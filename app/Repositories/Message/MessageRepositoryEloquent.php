<?php

namespace App\Repositories\Message;

use App\Brandroom;
use App\Chat;
use App\Message;
use App\User;
use Illuminate\Database\Eloquent\Model;

class MessageRepositoryEloquent implements MessageRepository
{

    /**
     * @var Message
     */
    private $message;

    private $user;

    public function __construct(Message $message)
    {
        $this->message = $message;

        $this->user = \Auth::user();
    }

    /**
     * Find messages by user
     *
     * @param User $user
     * @return mixed
     */
    public function findById($id)
    {
        return $this->message->findOrFail($id);
    }

    /**
     * Find messages by user
     *
     * @param User $user
     * @return mixed
     */
    public function findByUser(User $user)
    {
        return $user->message;
    }

    /**
     * Find messages by user
     *
     * @param User $user
     * @return mixed
     */
    public function findByChat(Chat $chat)
    {
        return $chat->messages->load(['projectMessage.sender']);
    }

    /**
     * Create a message
     *
     * @param Chat $chat
     * @param $message
     * @return $message
     */
    public function create(Chat $chat, $message)
    {
        $files = (isset($message['files']) && $message['files']) ? $message['files'] : [];

        $sender = $chat->workspace->project->customer_id == $this->user->id ? $this->user : $this->user->brandroom;

        $message = $chat->messages()->create($message);

        // add files to message
        foreach ($files as $file) {
            $message->projectMessageMedia()->create($file);
        }

        $members = $chat->members->load('member');

        // send message every member in chat
        foreach ($members as $member) {
            $project_message = $message->projectMessage()->create([]);
            $sender->senderMessages()->save($project_message);
            $member->member->receiveMessages()->save($project_message);
        }
        return $message->load(['projectMessage.sender']);
    }

    /**
     * Read a message
     *
     * @param Message $message
     * @return bool|null
     */
    public function read(Message $message)
    {
        return $message->update(['status' => 'read']);
    }

    /**
     * Unread a message
     *
     * @param Message $message
     * @return bool|null
     */
    public function unread(Message $message)
    {
        return $message->update(['status' => 'unread']);
    }

    /**
     * Delete a message
     *
     * @param Message $message
     * @return bool|null
     */
    public function delete(Message $message)
    {
        return $message->update(['status' => 'delete']);
    }

}