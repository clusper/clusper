<?php

namespace App\Repositories\Category;

use App\Category;

class CategoryRepositoryEloquent implements CategoryRepository
{

    /**
     * @var Category
     */
    private $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function all()
    {
        return $this->category->with('children')->get();
    }

    public function parents()
    {
        return $this->category->where('parent', 0)->get();
    }

    /**
     * Find category by id
     *
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        return $this->category->findOrFail($id);
    }

    /**
     * Create a category
     *
     * @param $data
     */
    public function create($data)
    {
    }

    /**
     * Delete category
     *
     * @param Category $category
     * @return bool|null
     */
    public function delete(Category $category)
    {
        return $category->delete();
    }

}