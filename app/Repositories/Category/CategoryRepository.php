<?php

namespace App\Repositories\Category;

use App\Category;

interface CategoryRepository
{
    public function all();

    public function parents();

    public function findById($id);

    public function create($data);

    public function delete(Category $category);

}