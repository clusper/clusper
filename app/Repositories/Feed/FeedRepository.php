<?php

namespace App\Repositories\Feed;

use App\Brandroom;
use App\Feed;
use App\User;
use Illuminate\Database\Eloquent\Model;

interface FeedRepository
{
    public function findByUser(User $user);

    public function findByBrandroom(Brandroom $brandroom);

    public function create(User $user, Model $model, $activity);

    public function delete(User $user, Feed $feed);

}