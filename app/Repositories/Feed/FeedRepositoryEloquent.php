<?php

namespace App\Repositories\Feed;

use App\Brandroom;
use App\Feed;
use App\User;
use Illuminate\Database\Eloquent\Model;

class FeedRepositoryEloquent implements FeedRepository
{

    /**
     * @var Feed
     */
    private $feed;

    public function __construct(Feed $feed)
    {
        $this->feed = $feed;
    }

    /**
     * Find feed by user
     *
     * @param User $user
     * @return mixed
     */
    public function findByUser(User $user) {

        return $this->feed->where('user_id', $user->id)->get();
    }

    /**
     * Find feed by brandroom
     *
     * @param Brandroom $brandroom
     * @return mixed
     */
    public function findByBrandroom(Brandroom $brandroom) {
        return $this->feed->where('brandroom', $brandroom->id)->get();
    }

    /**
     * Create a feed
     *
     * @param Feed $feed
     * @return static
     */
    public function create(User $user, Model $model, $activity) {
        $followers = $user->brandroom->followers;

        foreach ($followers as $follower) {
            $model->feed()->create([
                'activity'  => $activity,
                'author'    => $user->id,
                'user_id'   => $follower->follower
            ]);
        }
    }

    /**
     * Delete a feed
     *
     * @param Feed $feed
     * @return bool|null
     */
    public function delete(User $user,Feed $feed) {
        return $feed->delete();
    }

}