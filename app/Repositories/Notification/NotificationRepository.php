<?php

namespace App\Repositories\Notification;

use App\Notification;
use App\User;
use Illuminate\Database\Eloquent\Model;

interface NotificationRepository
{
    public function findByUser(User $user);

    public function findByAuthor(User $user);

    public function create(User $user, Model $author, Model $model, $activity);

    public function delete(User $user, Notification $feed);

}