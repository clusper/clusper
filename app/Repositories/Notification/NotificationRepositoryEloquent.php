<?php

namespace App\Repositories\Notification;

use App\Brandroom;
use App\Notification;
use App\User;
use Illuminate\Database\Eloquent\Model;

class NotificationRepositoryEloquent implements NotificationRepository
{

    /**
     * @var Notification
     */
    private $notification;

    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * Find notifications by user
     *
     * @param User $user
     * @return mixed
     */
    public function findByUser(User $user)
    {
        return $this->notification->where('user_id', $user->id)->get();
    }

    /**
     * Find notifications by user
     *
     * @param User $user
     * @return mixed
     */
    public function findByAuthor(User $user)
    {
        return $this->notification->where('brandroom', $user->id)->get();
    }

    /**
     * Create a notification
     *
     * @param User $user
     * @param User $author
     * @param Model $model
     * @param $activity
     * @return mixed
     */
    public function create(User $user, Model $author, Model $model, $activity)
    {
         $norification = $model->notifications()->create([
            'user_id'   => $user->id,
            'activity'  => $activity
        ]);

        return $author->authorNotification()->save($norification)->load(['notifiable', 'user', 'author']);
    }

    /**
     * Delete a notification
     *
     * @param Notification $notification
     * @return bool|null
     */
    public function delete(User $user,Notification $notification)
    {
        return $notification->delete();
    }

}