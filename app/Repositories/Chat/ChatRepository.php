<?php

namespace App\Repositories\Chat;

use App\Chat;
use Illuminate\Database\Eloquent\Model;

interface ChatRepository
{
    public function findById($id);
    
    public function findByAuthor(Model $author);

    public function create($author = null);

    public function delete(Chat $chat);

}