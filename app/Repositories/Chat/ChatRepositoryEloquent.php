<?php

namespace App\Repositories\Chat;

use App\Brandroom;
use App\Chat;
use App\User;
use Illuminate\Database\Eloquent\Model;

class ChatRepositoryEloquent implements ChatRepository
{

    /**
     * @var Chat
     */
    private $chat;

    public function __construct(Chat $chat)
    {
        $this->chat = $chat;
    }

    /**
     * Find chats by user
     *
     * @param User $user
     * @return mixed
     */
    public function findById($id)
    {
        return $this->chat->findOrFail($id);
    }

    /**
     * Find chats by author
     *
     * @param Model $author
     * @return mixed
     */
    public function findByAuthor(Model $author)
    {
        return $this->chat->where('author', $author->id)->get();
    }

    /**
     * Create a chat
     *
     * @param null $author
     * @param null $name
     * @return mixed
     */
    public function create($author = null, $name = null)
    {
        return $author->chats()->create([]);
    }

    /**
     * Delete chat
     *
     * @param Chat $chat
     * @return bool|null
     */
    public function delete(Chat $chat)
    {
        return $chat->delete();
    }

}