<?php

namespace App\Repositories\Transaction;

use App\Transaction;
use App\TransactionService;
use App\User;
use Exception;
use App\TransactionLog;

class TransactionRepositoryEloquent implements TransactionRepository
{
    public function create($amount, $commission, $status, $transactionable_type, $transactionable_id)
    {
        $transaction = Transaction::create([
            'amount' => $amount,
            'commission' => $commission,
            'status' => $status,
            'transactionable_type' => $transactionable_type,
            'transactionable_id' => $transactionable_id,
        ]);
        return $transaction;
    }

    public function paymentService($transactions = [], $type, $id){
        foreach($transactions as $transaction){
            TransactionService::create([
                'transaction_id' => $transaction,
                'payment_service' => $type,
                'payment_id' => $id
            ]);
        }

        return $transactions;
    }

    public function errorLog(Exception $e, User $user)
    {
        $log = TransactionLog::create([
            'user_id' => $user->id,
            'code' => $e->getCode(),
            'file' => $e->getFile(),
            'line' => $e->getLine(),
            'message' => $e->getMessage(),
        ]);
        return $log;
    }
}