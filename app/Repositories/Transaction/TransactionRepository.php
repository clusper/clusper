<?php

namespace App\Repositories\Transaction;

use App\Transaction;
use App\User;
use Exception;

interface TransactionRepository
{
    public function create($amount, $commission, $status, $transactionable_type, $transactionable_id);

    public function paymentService($transactions = [], $type, $id);

    public function errorLog(Exception $e, User $user);
}