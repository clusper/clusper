<?php

Route::group(['prefix' => 'api/v1', 'middleware' => 'api'], function () {
    Route::post('chats/{chat}/messages', 'MessageController@create');
    Route::get('chats/{chat}/messages', 'MessageController@getMessages');
    Route::get('messages/{message}/read', 'MessageController@readMessage');
    Route::get('messages/{message}/unread', 'MessageController@unreadMessage');
});