<?php

Route::group(['prefix' => 'api/v1/cart', 'middleware' => 'api'], function () {
    Route::get('',  ['as' => 'cart', 'uses' => 'CartController@index']);
    Route::get('content', 'CartController@content');
    Route::post('', 'CartController@add');
    Route::delete('{id?}', 'CartController@destroy');
    Route::post('buy/account', 'CartController@buyAccount');
    Route::post('buy/stripe', 'CartController@buyStripe');
});