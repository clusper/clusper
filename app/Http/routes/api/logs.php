<?php

Route::group(['prefix' => 'api/v1/logs', 'middleware' => 'api'], function () {
    Route::get('', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});