<?php

Route::group(['prefix' => 'api/v1', 'middleware' => 'api'], function () {

    Route::group(['prefix' => 'profile/projects'], function() {
        Route::get('', 'ProjectController@getCustomerProjects');
        Route::get('{type}', 'ProjectController@getCustomerProjects')->where('type', '(open|personal|in-progress|completed)');
    });

    Route::group(['prefix' => 'projects'], function () {
        Route::get('{project}', 'ProjectController@getProjectDetail');
        Route::get('create/s3', 'ProjectController@projectS3Details');
        Route::delete('{project}', 'ProjectController@delete');
        Route::post('{project}/apply', 'ProjectController@apply');
        Route::post('{project}/cancel-apply', 'ProjectController@cancelApply');
        Route::post('{project}/approve', 'ProjectController@approve');
        Route::post('{project}/decline', 'ProjectController@decline');
        Route::post('{project}/complete', 'ProjectController@complete');
        Route::post('{project}/select-candidate/{brandroom_id}', 'ProjectController@selectCandidate');
        Route::post('{project}/propose/{brandroom_id}', 'ProjectController@propose');
        Route::post('{project}/cancel-propose', 'ProjectController@cancelPropose');
        Route::post('create/{brandroom?}', 'ProjectController@create');
    });

    Route::get('workspaces/{workspace}', 'ProjectController@getWorkspace');
    Route::get('workspace/{workspace}/product/s3', 'ProjectController@workspaceProductS3Details');
    Route::post('workspace/{workspace}/products/upload', 'ProjectController@workspaceProductUpload');
});