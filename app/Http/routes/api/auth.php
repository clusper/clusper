<?php

Route::group(['prefix' => 'api/v1', 'middleware' => 'api'], function () {
    Route::get('login/{provider}', ['as' => 'sociallogin', 'uses' => 'Auth\AuthController@redirectToProvider']);
    Route::get('login/{provider}/call', ['as' => 'sociallogincallback', 'uses' => 'Auth\AuthController@handleProviderCallback']);

    Route::get('user/{email}', 'Auth\AuthController@userExists');
    Route::post('login', 'Auth\AuthController@authenticate');
    Route::get('logout', 'Auth\AuthController@logout');

    Route::post('register', 'Auth\AuthController@register');
});