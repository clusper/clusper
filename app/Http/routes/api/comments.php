<?php

Route::group(['prefix' => 'api/v1', 'middleware' => 'api'], function () {

    Route::delete('comments/{comment}', 'CommentController@destroy');
});