<?php

Route::group(['prefix' => 'api/v1/', 'middleware' => ['auth', 'api']], function () {
    Route::group(['middleware' => 'auth', 'prefix' => 'profile/settings/'], function() {
        Route::get('profile', 'ProfileController@getProfile');
        Route::put('profile', 'ProfileController@updateProfile');
        Route::get('billings', 'ProfileController@getBillings');
        Route::post('stripe', 'ProfileController@updateStripe');
        Route::post('paypal', 'ProfileController@updatePaypal');
    });
    Route::post('profile/account/stripe', 'ProfileController@setMoneyByStripe');

    Route::get('feed', 'FeedController@getUserFeed');
});