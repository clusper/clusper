<?php

Route::group(['prefix' => 'api/v1/categories', 'middleware' => 'api'], function () {
    Route::get('', 'CategoryController@parents');
    Route::get('all', 'CategoryController@all');
    Route::get('{category}/attributes', 'CategoryController@attributes');
});