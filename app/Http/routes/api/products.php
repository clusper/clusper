<?php

Route::group(['prefix' => 'api/v1', 'middleware' => 'api'], function () {

    Route::get('{type}', 'ProductController@getProducts')
        ->where('type', '(music|audiobooks|ebooks|photo|graphics|libraries|templates|effects)');

    Route::group(['prefix' => 'products'], function () {
        Route::post('upload', 'ProductController@upload');
        Route::get('upload/s3', 'ProductController@s3details');
        Route::get('{uuid}', 'ProductController@getProduct');
        Route::get('{product}/attributes', 'ProductController@getProductWithAttr');
        Route::post('{uuid}/update', 'ProductController@update');
        Route::get('{product}/comments', 'ProductController@getComments');
        Route::post('{product}/comments', 'ProductController@storeComments');
        Route::post('{product}/report', 'ProductController@storeReport');
    });
});