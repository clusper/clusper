<?php

Route::group(['prefix' => 'api/v1', 'middleware' => 'api'], function () {

    Route::get('brandrooms', 'BrandroomController@all');

    Route::group(['middleware' => 'auth', 'prefix' => 'news'], function(){
        Route::post('media/add', ['uses' => 'NewsController@uploadNewsMedia']);
        Route::post('add', ['uses' => 'NewsController@addNews']);
    });
    Route::group(['prefix' => 'brandroom/projects'], function() {
        Route::get('', 'ProjectController@getOpenProjects');
        Route::get('{type}', 'ProjectController@getBrandroomProjects')->where('type','(open|personal|in-progress|completed)');
    });

    Route::get('brandroom/news/add', 'BrandroomController@brandroomProductsList');
    Route::get('{brandroom_id}/news/load/{skip}', 'NewsController@loadMore');
    Route::get('brandroom/news', 'BrandroomController@news');
    Route::get('{brandroom}/news', 'BrandroomController@news');
    Route::get('brandroom/news/{news}', 'NewsController@fullNews');
    Route::get('{brandroom}/news/{news}', 'NewsController@fullNews');
    Route::delete('brandroom/news/{news}', 'NewsController@delete');
    Route::get('{brandroom}/news/images', 'BrandroomController@images');
    Route::get('brandroom/products', 'BrandroomController@brandroomProducts');
    Route::post('brandroom/background', 'BrandroomController@uploadCover');
    Route::get('{brandroom}/products', 'BrandroomController@brandroomProducts');
    Route::post('brandroom', 'BrandroomController@store');
    Route::put('brandroom', 'BrandroomController@update');
    Route::get('brandroom/settings', 'BrandroomController@settings');
    Route::get('brandroom/{brandroom?}', 'BrandroomController@data');
    Route::get('brandroom/{brandroom_alias}/cover', 'BrandroomController@coverData');
    Route::get('check_alias', ['uses' => 'BrandroomController@checkAlias']);
    Route::get('check_paypal_email', ['uses' => 'BrandroomController@checkPayPalAccount']);
});