<?php

Route::get('/', function () {
    if(Auth::check())
        return redirect('/wishroom');
    return view('pages.home');
});


