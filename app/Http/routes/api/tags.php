<?php

Route::group(['prefix' => '/api/v1/tags', 'middleware' => 'api'], function () {
    Route::get('list', 'TagController@get');

});