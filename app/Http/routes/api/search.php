<?php

Route::group(['prefix' => 'api/v1', 'middleware' => 'api'], function () {

    Route::get('search/{query}', 'SearchController@mainSearch');
});