<?php

Route::group(['prefix' => 'api/v1/users', 'middleware' => ['auth', 'api']], function () {
    Route::get('me', 'Auth\AuthController@me');
    Route::get('me/viewed-products', 'UserController@viewedProducts');
    Route::get('me/followers', 'UserController@getFollowers');
    Route::get('me/notifications', 'UserController@getNotifications');
});