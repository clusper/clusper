<?php

Route::group(['prefix' => 'api/v1/wishroom', 'middleware' => 'api'], function () {
    Route::get('', 'WishroomController@index');
    Route::post('create', 'WishroomController@add');
    Route::delete('{id}', 'WishroomController@destroy');
});