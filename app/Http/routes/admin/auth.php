<?php

Route::group(['middleware' => 'check_admin', 'prefix' => 'admin/v1', 'namespace' => 'Admin'], function () {
    Route::get('dashboard', 'DashboardController@index');

    Route::post('authenticate', 'SettingsController@index');

    Route::post('logout', 'GeneralController@logout');
});