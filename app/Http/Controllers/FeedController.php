<?php

namespace App\Http\Controllers;

use App\Feed;
use App\Product;
use App\Repositories\Feed\FeedRepository;
use Auth;
use Illuminate\Http\Request;

use App\Http\Requests;

class FeedController extends Controller
{

    /**
     * @var FeedRepository
     */
    protected $feedRepository;

    /**
     * Current user
     *
     * @var
     */
    protected $user;

    /**
     * FeedController constructor.
     * @param FeedRepository $feedRepository
     */
    public function __construct(FeedRepository $feedRepository)
    {
        $this->user = Auth::user();

        $this->feedRepository = $feedRepository;
    }


    /**
     * Get feed by current user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserFeed(Request $request)
    {
        $feed = $this->feedRepository->findByUser($this->user);

        return $this->successResponse(compact('feed'));
    }
}
