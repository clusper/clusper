<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Events\UserCreateNotification;
use App\Http\Requests\CommentRequest;
use App\Http\Requests\UploadProductRequest;
use App\Http\Requests\ReportRequest;
use App\Product;
use App\ProductAttribute;
use App\Report;
use App\Http\Requests;
use App\Repositories\Product\ProductRepository;
use App\Repositories\User\UserRepository;
use Auth;
use Event;
use Exception;
use Illuminate\Http\Request;
use App\Facades\AmazonService;
use JWTAuth;
use Webpatser\Uuid\Uuid;
use Storage;

class ProductController extends Controller
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    private $userRepository;

    /**
     * @var mixed
     */
    protected $user;

    public function __construct(ProductRepository $productRepository, UserRepository $userRepository)
    {
        $this->user = Auth::user();

        $this->productRepository = $productRepository;

        $this->userRepository = $userRepository;
    }

    public function getProduct($uuid)
    {
        $product = $this->productRepository->findByUuid($uuid);

        return $this->successResponse(['product' => $product]);
    }

    public function getProductWithAttr(Product $product)
    {
        $options = $product->options->pluck('id');

        $product = $product->load(['attributes.options' => function($query) use ($options) {
            $query->whereIn('id', $options);
        }]);

        return $this->successResponse($product);
    }

    /**
     * Display a listing of the products by category.
     *
     * @param null $category
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProducts($category = null, Request $request)
    {
        $category = Category::where('alias', $category)
            ->with(['children.attributes.options', 'children.attributes.type'])->first();

        $products = $brandroom = $this->productRepository->all($category, $request);

        $products = $products->paginate(18);

        $brandrooms = $brandroom->random()->groupBy('brandroom_id')->take(10)->get()->pluck('brandroom');

        return $this->successResponse(compact('products', 'brandrooms', 'category'));
    }

    public function upload(Request $request){
        $data = $request->all();
        $data['uuid'] = Uuid::generate(4);
        $data['brandroom_id'] = $this->user->brandroom->id;

        $product = Product::create($data);

        if(isset($data['$data'])) {
            foreach ($data['attr'] as $key => $value) {
                foreach ($value as $option) {
                    ProductAttribute::create([
                        'product_id' => $product->id,
                        'attribute_id' => $key,
                        'attribute_option_id' => $option,
                    ]);
                }
            }
        }

        return response()->json([
            'data'      => [],
            'status'    => 'success',
            'message'   => 'Product will be cheking by moderator and add to your brandroom'
        ], 200);
    }

    /**
     * Update the product
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($uuid, Request $request)
    {
        $data = $request->all();

        if(isset($data['attr'])){
            $attr_values = $data['attr'];
            unset($data['attr']);
        }

        $product = Product::where('uuid', $uuid)->first();
        $product->update($data);

        if(isset($attr_values)) {
            foreach ($attr_values as $key => $value) {
                foreach ($value as $option) {
                    ProductAttribute::where('product_id', $product->id)->update([
                        'attribute_id' => $key,
                        'attribute_option_id' => $option,
                    ]);
                }
            }
        }

        return $this->successResponse([], 'Product was updated');
    }

    /**
     * Remove the product
     *
     * @param Product $product
     * @return bool|null
     */
    public function destroy(Product $product){

        $this->authorize('destroy', $product);

        $product->delete();

        return $this->successResponse(null, 'Product was deleted!');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function s3details(Request $request){
        $source = 'brandrooms/'.$this->user->brandroom->id.'/products/'.$request->get('filename');
        $detail = AmazonService::getS3Details($source, $request->get('type'), 'public-read');
        return $this->successResponse(['params' =>$detail, 'url' => getenv('S3')]);
    }

    public function getComments(Product $product)
    {
        $comments = $product->comments->load('parent');

        return $this->successResponse(compact('comments'));
    }

    /**
     * Create a new comment for product
     *
     * @param Product $product
     * @param CommentRequest $request
     * @return array
     */
    public function storeComments(Product $product, CommentRequest $request)
    {
        $comment = $this->user->comments()->create($request->all());

        $comment = $product->comments()->save($comment)->load(['user', 'parent']);

        if($request->get('parent_id')) {
            Event::fire(new UserCreateNotification($comment->parent->user, $this->user, $comment,  'reply-comment'));
        } else {
            Event::fire(new UserCreateNotification($comment->commentable->brandroom->user, $this->user, $comment->commentable, 'create-comment'));
        }

        return $this->successResponse(compact('comment'));
    }

    public function getBrandroom(Brandroom $brandroom)
    {
        $products = $this->brandroomProduct($brandroom);

        $news = $this->brandroomNews($brandroom);

    }

    public function brandroomProduct(Brandroom $brandroom)
    {
        $brandroom->load('products');
    }

    public function brandroomNews(Brandroom $brandroom)
    {
        $brandroom->load('news');
    }

    /**
     * Create a new report for product
     *
     * @param Product $product
     * @param ReportRequest|Request $request
     * @return mixed
     * @internal param $uuid
     */
    public function storeReport(Product $product, ReportRequest$request)
    {
        $report =$this->user->reports()->create($request->all());

        $product->reports()->save($report);

        return $this->successResponse(null, 'Your report was sent');
    }
}
