<?php

namespace App\Http\Controllers;

use App\Chat;
use App\Http\Requests\MessageRequest;
use App\Message;
use App\Repositories\Message\MessageRepository;
use App\Transformers\MessageTransformer;
use Illuminate\Http\Request;

use App\Http\Requests;
use Redis;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class MessageController extends Controller
{
    /**
     * @var MessageRepository
     */
    private $messageRepository;

    public function __construct(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * Create a message
     *
     * @param Chat $chat
     * @param MessageRequest $request
     */
    public function create(Chat $chat, MessageRequest $request)
    {
        $this->authorize('workspace', $chat->workspace);

        $message = $this->messageRepository->create($chat, $request->all());

        $message = $this->transformItem($message);

        $redis = Redis::connection();
        $redis->publish('send-chat-message', json_encode($message));

        return $this->successResponse($message, null, 'message');
    }

    /**
     * Get messages by chat
     *
     * @param Chat $chat
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMessages(Chat $chat)
    {
        $this->authorize('messages', $chat);

        $messages = $this->messageRepository->findByChat($chat);

        $messages = $this->transformCollection($messages);

        return $this->successResponse($messages, null, 'messages');
    }

    /**
     * Read the message
     *
     * @param Message $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function readMessage(Message $message)
    {
        $this->authorize('workspace', $message->chat->workspace);

        $message = $this->messageRepository->read($message);

        return $this->successResponse($message, null, 'message');
    }

    public function unreadMessage(Message $message)
    {
        $this->authorize('workspace', $message->chat->workspace);

        $message = $this->messageRepository->unread($message);

        return $this->successResponse($message, null, 'message');
    }

    /**
     * @param $messages
     * @return array
     */
    public function transformCollection($messages)
    {
        return fractal()
            ->collection($messages, new MessageTransformer())
            ->serializeWith(new \Spatie\Fractal\ArraySerializer())
            ->toArray();
    }

    /**
     * @param $messages
     * @return array
     */
    public function transformItem($message)
    {
        return fractal()
            ->item($message, new MessageTransformer())
            ->serializeWith(new \Spatie\Fractal\ArraySerializer())
            ->toArray();
    }
}
