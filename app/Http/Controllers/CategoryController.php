<?php

namespace App\Http\Controllers;

use App\Category;
use App\Repositories\Category\CategoryRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{

    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }


    /**
     * Get all categories
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function parents()
    {
        $categories = $this->categoryRepository->parents();

        return $this->successResponse($categories, null, 'categories');
    }

    /**
     * Get all categories with attributes
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        $categories = $this->categoryRepository->all()->load(['attributes', 'attributes.type', 'attributes.options']);

        return $this->successResponse($categories, null, 'categories');
    }

    /**
     * Get category attributes
     *
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function attributes(Category $category){
        return $this->successResponse(['attributes' => $category->attributes->load(['options', 'type'])]);
    }
}
