<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use App\Http\Controllers\Controller;
use App\Content;
use App\Facades\SearchService;

class SearchController extends Controller
{
    
    public $ids = array();
    
    public function search(Request $request) {
        $sresults = array();
        DB::connection()->enableQueryLog();
        $query = $request->input('q');//отримую запит
        $page = $request->input('page');
        $query_blocks = explode(' ', $query);//запит розбивається на окремі слова
        
        /* налаштування (тимчасово) */
        $lang_id = 2;//id мови
        $perpage = $request->input('s') == 'main' ? 5 : 10;//результатів на сторінку
        
        $query_blocks = array_filter($query_blocks, function($el){
            return (1 < strlen($el));//беремо тільки слова більше 1 літери
        });
        
        if(1 < count($query_blocks)) {//якщо запит більше ніж з одного слова
            
            $query = $query_blocks[0];
            unset($query_blocks[0]);
            
        }else{
            $query_blocks = array();
        }
        
        query:
        $sresults[] = Content::where('lang', $lang_id)
        ->where(function($tmp_query) use ($query){
            $tmp_query->where('title', 'like', '%'.$query.'%')
            ->orWhere('description', 'like', '%'.$query.'%');
        })
        ->groupBy('id')
        ->take($perpage)
        ->get()
        ->toArray();
        
        
        if(1 < count($query_blocks)){
            $query = $query_blocks[0];
            unset($query_blocks[0]);
            goto query;
        }elseif(1 == count($query_blocks)) {
            $query = implode('', $query_blocks);
            $query_blocks = array();
            goto query;
        }
        
        /* фільтруємо результати що повторяються */
        $cleaned_result = array();
        foreach($sresults as $result){
            if(1 < count($result)){
                $result = array_filter($result, function($res){
                    if(in_array($res['id'], $this->ids)){
                        return 0;
                    }else{
                        $this->ids[] = $res['id'];
                        return 1;
                    }
                }); 
            }
            if(!empty($result)) $cleaned_result[] = $result;
        }
            
        // print_r($cleaned_result);
        // return json_encode($this->ids);
        // $queries = DB::getQueryLog();
        // var_dump(json_encode($sresults));die();
        return view('index', ['results' => $cleaned_result]);
    }
    
    public function mainSearch($query){
        $search_results['brandrooms'] = SearchService::searchBrandrooms($query);
        // $search_results['products'] = SearchService::searchProducts($query);
        return $this->SuccessResponse($search_results);
    }
}
