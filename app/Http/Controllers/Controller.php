<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function errorResponse(Exception $e)
    {
        return response()->json([
            'status'    => 'error',
            'code'      => $e->getCode(),
            'trace'     => $e->getTrace(),
            'message'   => $e->getMessage()
        ], 500);
    }

    public function successResponse($data, $message = null, $name = 'data')
    {
        return response()->json([
            $name       => $data,
            'status'    => 'success',
            'message'   => $message
        ], 200);
    }
}
