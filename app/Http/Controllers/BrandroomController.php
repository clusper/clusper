<?php

namespace App\Http\Controllers;

use App\Http\Requests\BrandroomUpdateRequest;
use App\Http\Requests\BrandroomCreateRequest;
use App\Facades\ProductService;
use App\Facades\MediaService;
use App\Product;
use App\Repositories\Brandroom\BrandroomRepository;
use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;
use App\Facades\Comments;
use App\Http\Requests;

use App\Http\Controllers\Controller;
use Debugbar;
use Exception;
use App\Events\BrandroomWasCreated;
use App\Events\BrandroomWasUpdated;

use App\Brandroom;
use App\BrandroomNews;
use App\NewsMedia;
use App\ReservedWord;
use Auth;
use Event;
use JWTAuth;

class BrandroomController extends Controller
{
    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * Current user
     *
     * @var \App\User|null
     */
    protected $user;

    /**
     * @var BrandroomRepository
     */
    protected $brandroomRepository;

    public function __construct(ProductRepository $productRepository, BrandroomRepository $brandroomRepository)
    {
        $this->user = Auth::user();

        $this->productRepository = $productRepository;

        $this->brandroomRepository = $brandroomRepository;

        $this->middleware('auth', ['except' => ['news', 'coverData', 'data']]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function all()
    {
        return $this->successResponse($this->brandroomRepository->all(), null, 'brandrooms');
    }

    /**
     * Get brandroom data
     *
     * @param Brandroom $brandroom
     * @return Brandroom
     */
    public function data(Brandroom $brandroom)
    {
        return $this->successResponse($brandroom);
    }
    
    /**
     * Get brandroom data for cover
     *
     * @param Brandroom $brandroom
     * @return Brandroom
     */
    public function coverData($brandroom_alias)
    {
        $brandroom = Brandroom::select('background', 'title')->where('alias',  $brandroom_alias)->first();
            
        return $brandroom;
    }

    /**
     * Get brandroom news
     *
     * @param Brandroom $brandroom
     * @return Brandroom
     */
    public function news(Brandroom $brandroom)
    {
        if(!$brandroom->exists) {
            $brandroom = $this->user->brandroom;
        }

        return $this->successResponse($brandroom->load(['news.images', 'media' => function($query){
            $query->take(10);
        }, 'news' => function($query){
            $query->orderBy('created_at', 'desc')->withCount('comments')->take(10);
        }]));
    }


    /**
     * Create new brandroom.
     *
     * @param BrandroomCreateRequest $request
     * @return mixed
     */
    public function store(BrandroomCreateRequest $request)
    {
        $brandroom_data = $request->all();
        $brandroom_data['logo'] = config('image.default_brandroom_logo');
        $brandroom_data['background'] = config('image.default_brandroom_background');
        $brandroom = $this->user->brandroom()->create($brandroom_data);
        Event::fire(new BrandroomWasCreated($brandroom));
        return $brandroom;
    }

    /**
     * Get brandroom info for settings
     *
     * @param $brandroom
     */
    public function settings()
    {
        $brandroom = $this->user->brandroom;
        return $this->successResponse(compact('brandroom'));
    }

    /**
     * Update the brandroom
     *
     * @param BrandroomUpdateRequest $request
     * @return mixed
     */
    public function update(BrandroomUpdateRequest $request)
    {
        $brandroom = $this->user->brandroom->fill($request->all())->save();
        Event::fire(new BrandroomWasUpdated($request->all()));
        // TODO: update logo in index

        return $this->successResponse(compact('brandroom'), 'Brandroom updated successfully!');
    }

    /**
     * Get brandroom products list
     *
     * @param Brandroom $brandroom
     * @return Brandroom
     */
    public function brandroomProductsList(Brandroom $brandroom)
    {
        if(!$brandroom->exists) {
            $brandroom = $this->user->brandroom;
        }

        return $this->successResponse($this->productRepository->productsListForNews($brandroom->id)->select('id', 'title')->get());
    }

    /**
     * Get brandroom products
     *
     * @param Brandroom $brandroom
     * @return Brandroom
     */
    public function brandroomProducts(Brandroom $brandroom)
    {
        if(!$brandroom->exists) {
            $brandroom = $this->user->brandroom;
        }

        $products = $this->productRepository->brandroomProducts($brandroom)->paginate(3);

        $response = [
            'brandroom' => $brandroom,
            'products'  => $products
        ];
        return $response;
    }

    /**
     * Check alias for brandroom
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkAlias(Request $request){

        if(Brandroom::where('title', $request->get('alias'))->first()
            || ReservedWord::where('word', $request->get('alias'))->first()){
            return response()->json(['correct' => 'not']);
        }
        return response()->json(['correct' => 'yes']);
    }

    /**
     * Upload cover image for brandroom
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadCover(Request $request){
        
        $uploaded_cover = MediaService::resizeCropUploadImage($request, 'background');
        
        $storage = Brandroom::where('user_id', $this->user->id)
        ->update([
            'background' => $uploaded_cover['url'],
            'background_key' => $uploaded_cover['key']
            ]);
        
        return $this->successResponse([
				"url" => "${uploaded_cover['url']}",
				"width" => 1200,
				"height" => 120
            ]);
    }

    public function checkPayPalAccount(){

    }
}
