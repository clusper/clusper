<?php

namespace App\Http\Controllers;

use App\Category;
use App\Facades\ProductService;
use App\Repositories\Product\ProductRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use JWTAuth;

class WishroomController extends Controller
{

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * Current user
     *
     * @var \App\User|null
     */
    protected $user;

    public function __construct(ProductRepository $productRepository)
    {
        $this->user = Auth::user();

        $this->productRepository = $productRepository;
    }

    /**
     * Get a wishroom products.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category = [];

        if($request->category) {
            $category = Category::findOrFail($request->category);
        }

        $products = $this->productRepository->all($category, $request)->with('users')->whereHas('users', function($query) {
            $query->where('user_id', $this->user->id);
        })->paginate(2);

        return $this->successResponse(compact('products'));
    }

    /**
     * Add the product to wishroom.
     *
     * @param Request $request
     */
    public function add(Request $request)
    {
        $this->user->wishroom()->attach($request->product_id);

        return $this->successResponse(NULL, 'Product has been added to wishroom!');
    }

    /**
     * Remove the product from a wishroom.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->user->wishroom()->detach($id);

        return $this->successResponse(NULL, 'Product has been remoed from wishroom!');
    }
}
