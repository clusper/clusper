<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserLogged;
use App\Events\UserLogout;
use App\Facades\CartService;
use App\User;
use App\UserPaymentData;
use Event;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Socialite;
use Auth;
use Illuminate\Http\Request;
use App\Facades\MediaService;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Config;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */
    
    protected $data = array();
    protected $includes  = array();

    use ThrottlesLogins;
    
    protected $redirectTo = '/profile';
    protected $providers = ['facebook', 'google'];


    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255|min:2',
            'last_name' => 'required|max:255|min:2',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }
    
    public function register(Request $request){
        $this->data['name'] = $request['name'];
        $this->data['email'] = $request['email'];
        $this->data['password'] = $request['password'];
        $this->data['avatar'] = $this->data['avatar_key'] = config('image.default_user_avatar');
        if($gravatar = MediaService::askGravatar($request['email'])){
            $this->data['avatar'] = Config::get('image.default_user_avatar');
            $this->data['avatar_key'] = '';
        }else{
            $this->data['avatar'] = Config::get('image.default_user_avatar');
            $this->data['avatar_key'] = '';
        }
        if($user = $this->create()){
            UserPaymentData::create(['user_id' => $user->id]);
            Auth::login($user, true);
            return response()->json(['status' => 'success', 'error' => null])->withCookie(cookie()->forever('known', 1));
        }else{
            return response()->json(['status' => 'fail', 'error' => null]);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create()
    {
        return User::create([
            'name' => $this->data['name'],
            'email' => $this->data['email'],
            'password' => bcrypt($this->data['password']),
            'avatar' => $this->data['avatar'],
            'avatar_key' => $this->data['avatar_key'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function createSocial(array $data)
    {
        return User::create($data);
    }

    /**
     * Login user.
     *
     * @return json
     */
    public function authenticate(Request $request)
    {
        $cart = CartService::content()->toArray();
        if (Auth::attempt(['email' => $request['email'], 'password' => $request['password']], 1)) {
            Event::fire(new UserLogged(Auth::user(), $cart));
            
            // Authentication passed...
            return response()->json(['status' => 'success', 'error' => null])->withCookie(cookie()->forever('known', 1));
        }else{
            return response()->json(['status' => 'fail', 'error' => null], 400);
        }
    }

    /**
     * Redirect to provider.
     *
     * @return boolean
     */
    public function redirectToProvider($provider)
    {
        if(!in_array($provider, $this->providers))
        return redirect('/');
        return Socialite::with($provider)->redirect('/');
    }
    
    /**
     * Logout the user
     *
     * @return boolean
     */
    public function logout() {

        Auth::logout();
        Event::fire(new UserLogout());

    }

    /**
     * Retrive auth data.
     *
     * @return User
     */
    public function handleProviderCallback($provider)
    {
        if(!in_array($provider, $this->providers))
        return redirect('/');
        try {
            $user = Socialite::driver($provider)->user();
        } catch (Exception $e) {
            return Redirect::to('/');
        }

        $authUser = $this->findOrCreateUser($user, $provider);

        Auth::login($authUser, true);

        return redirect('music');
    }
    
    /**
     * Return user if exists; create and return if doesn't
     *
     * @param $fbuser
     * @return User
     */
    private function findOrCreateUser($user, $provider)
    {
        if ($authUser = User::where($provider, $user->id)->first()) {
            return $authUser;
        }

        $user->user['email'] = $user->email;
        
        if($provider == 'google'){
            $user->user['name'] = $user->user['name']['givenName'] . ' ' . $user->user['name']['familyName'];
            $user->avatar= preg_replace('/\?(\w*=\d*)/', 'sz=300', $user->avatar);
        }elseif($provider == 'facebook'){
            $user->user['name'] = $user->name;
            $user->avatar = $user->avatar_original;
        }
        
        $avatar_result = MediaService::uploadToCdn($user->avatar, 'avatar');

        return User::create([
            'name' => $user->user['name'],
            'email' => $user->user['email'],
            'status' => 1,
            $provider => $user->id,
            'avatar' => $avatar_result['url'],
            'avatar_key' => $avatar_result['key'],
        ]);
    }
    
    /**
     * Check if user exists by email
     * 
     * @reurn Response
     */
    public function userExists($email, Request $request){
        $user = User::where('email', $email)->firstOrFail();
        
        $default_avatar = config('image.default_user_avatar');
        
        if (!$request->cookie('known')) {
            $user->avatar = $default_avatar;
        }

        $response = [
            'status' => 'success',
            'data' => [
                'name' => $user->name,
                'avatar' => $user->avatar ?: $default_avatar
            ],
            'error' => null
        ];

        return response()->json($response);
    }

    public function me(Request $request)
    {
        $user = Auth::user()->load(['brandroom', 'payment']);
        //$user = JWTAuth::parseToken()->authenticate()->load(['brandroom', 'payment']);

        return $this->successResponse($user, null, 'user');
    }


    public function authenticateJwt(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        try {
            $cart = CartService::content()->toArray();

            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                Event::fire(new UserLogged(Auth::user(), $cart));
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        // all good so return the token
        return response()->json(compact('token'));
    }
    public function validateToken()
    {
        // Our routes file should have already authenticated this token, so we just return success here
        return response()->array(['status' => 'success']);
    }
}
