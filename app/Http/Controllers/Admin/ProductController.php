<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Comment;
use App\Events\UserCreateNotification;
use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Http\Requests\UploadProductRequest;
use App\Http\Requests\ReportRequest;
use App\Product;
use App\ProductAttribute;
use App\Report;
use App\Http\Requests;
use App\Repositories\Product\ProductRepository;
use App\Repositories\User\UserRepository;
use Auth;
use Event;
use Exception;
use Illuminate\Http\Request;
use App\Facades\AmazonService;
use Webpatser\Uuid\Uuid;
use Storage;

class ProductController extends Controller
{
    /**
     * @var ProductRepository
     */
    private $productRepository;

    private $userRepository;

    public function __construct(ProductRepository $productRepository, UserRepository $userRepository)
    {
        $this->productRepository = $productRepository;

        $this->userRepository = $userRepository;
    }

    public function getProduct($uuid)
    {
        $product = $this->productRepository->findByUuid($uuid);

        return $this->successResponse(['product' => $product]);
    }

    public function getProductWithAttr(Product $product)
    {
        $options = $product->options->pluck('id');

        $product = $product->load(['attributes.options' => function($query) use ($options) {
            $query->whereIn('id', $options);
        }]);

        return $this->successResponse($product);
    }

    /**
     * Display a listing of the products by category.
     *
     * @param null $category
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProducts($category = null, Request $request)
    {
        $category = Category::where('alias', $category)
            ->with(['children.attributes.options', 'children.attributes.type'])->first();

        $products = $brandroom = $this->productRepository->all($category, $request);

        $products = $products->paginate(18);

        $brandrooms = $brandroom->random()->groupBy('brandroom_id')->take(10)->get()->pluck('brandroom');

        return $this->successResponse(compact('products', 'brandrooms', 'category'));
    }


    /**
     * Remove the product
     *
     * @param Product $product
     * @return bool|null
     */
    public function destroy(Product $product){

        $this->authorize('destroy', $product);

        $product->delete();

        return $this->successResponse(null, 'Product was deleted!');
    }



    public function getComments(Product $product)
    {
        $comments = $product->comments->load('parent');

        return $this->successResponse(compact('comments'));
    }
}
