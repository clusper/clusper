<?php

namespace App\Http\Controllers;

use App\Brandroom;
use App\Events\UserCreateNotification;
use App\Facades\AmazonService;
use App\Http\Requests\ProjectRequest;
use App\Project;
use App\Repositories\Chat\ChatRepository;
use App\Repositories\Project\ProjectRepository;
use App\Transformers\ProjectTransformer;
use App\Transformers\WorkspaceTransformer;
use App\Workspace;
use Auth;
use Event;
use Illuminate\Http\Request;

use App\Http\Requests;
use JWTAuth;

class ProjectController extends Controller
{
    /**
     * @var ProjectRepository
     */
    private $projectRepository;

    /**
     * @var ChatRepository
     */
    private $chatRepository;

    /**
     * @var \App\User|null
     */
    private $user;

    public function __construct(ProjectRepository $projectRepository, ChatRepository $chatRepository)
    {
        $this->projectRepository = $projectRepository;

        $this->chatRepository = $chatRepository;

        $this->user = Auth::user();
    }

    /**
     * Get project detail
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProjectDetail(Project $project)
    {
        $project = $this->transformItem($project);

        return $this->successResponse($project, 'null', 'project');
    }
    
    /**
     * Get open projects
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOpenProjects()
    {
        $projects = $this->projectRepository->findOpen();

        $projects = $this->transformCollection($projects);

        return $this->successResponse($projects, 'null', 'projects');
    }

    /**
     * Get open customer projects
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCustomerProjects($status = 'open')
    {
        $projects = $this->projectRepository->findByCustomer($this->user, $status)->load(['workspace']);

        $projects = $this->transformCollection($projects);

        return $this->successResponse($projects, 'null', 'projects');
    }

    /**
     * Get in-progress projects of brandroom
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBrandroomProjects($status)
    {
        $projects = $this->projectRepository->findByBrandroom($this->user->brandroom, $status)->load(['workspace']);

        $projects = $this->transformCollection($projects);

        return $this->successResponse($projects, 'null', 'projects');
    }

    /**
     * Create the project
     *
     * @param ProjectRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Brandroom $brandroom, ProjectRequest $request)
    {
        $project = $this->projectRepository->create($request->all());

        $this->projectRepository->setStatus($project, Project::STATUS_OPEN);

        if($brandroom->exists) {
            return $this->attachBrandroomToProject($brandroom, $project);
        }

        $project = $this->transformItem($project);

        return $this->successResponse($project, 'null', 'project');
    }

    /**
     * Attach brandroom to project
     *
     * @param Brandroom $brandroom
     * @param $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function attachBrandroomToProject(Brandroom $brandroom, Project $project)
    {
        $this->projectRepository->setStatus($project, Project::STATUS_PERSONAL);

        Event::fire(new UserCreateNotification($brandroom->user, $project->customer, $project, 'create-personal-project'));

        return $this->createWorkspace($project, $brandroom);
    }

    /**
     * Apply the project by brandroom
     *
     * @param Project $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function apply(Project $project)
    {
        if($project->customer->id == $this->user->id) {
            return $this->errorResponse(new \Exception('You can\'t apply your project!'));
        }

        $this->authorize('apply', $project);

        $project = $this->projectRepository->apply($project, $this->user->brandroom);

        Event::fire(new UserCreateNotification($project->customer, $this->user->brandroom, $project,  'apply-project'));

        $project = $this->transformItem($project);

        return $this->successResponse($project, 'Success! Now, you\'re candidate!', 'project');
    }

    /**
     * Approve the personal project
     *
     * @param Project $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function approve(Project $project)
    {
        $project = $this->projectRepository->approve($project, $this->user->brandroom);

        Event::fire(new UserCreateNotification($project->customer, $this->user->brandroom, $project,  'approve-project'));

        $workspace = $project->workspace;

        return $this->successResponse($workspace, 'Success! You have a project!', 'workspace');
    }

    /**
     * Decline the personal project
     *
     * @param Project $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function decline(Project $project)
    {
        $this->authorize('decline', $project);

        $project = $this->projectRepository->decline($project);

        Event::fire(new UserCreateNotification($project->customer, $this->user->brandroom, $project,  'decline-project'));

        $project = $this->transformItem($project);

        return $this->successResponse($project, 'Success! You declined a project!', 'project');
    }

    /**
     * Cancel apply the project by brandroom
     *
     * @param Project $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelApply(Project $project)
    {
        $this->authorize('cancel-apply', $project);

        $project = $this->projectRepository->cancelApply($project, $this->user->brandroom);

        Event::fire(new UserCreateNotification($project->customer, $this->user->brandroom, $project,  'cancel-apply-project'));

        $project = $this->transformItem($project);

        return $this->successResponse($project, 'You canceled your bid!', 'project');
    }

    /**
     * Select brandroom from candidates
     *
     * @param Project $project
     * @param Brandroom $brandroom
     */
    public function selectCandidate(Project $project, Brandroom $brandroom)
    {
        $this->projectRepository->setStatus($project, Project::STATUS_IN_PROGRESS);

        return $this->createWorkspace($project, $brandroom);
    }

    /**
     * Propose project for brandroom
     *
     * @param Project $project
     * @param Brandroom $brandroom
     * @return \Illuminate\Http\JsonResponse
     */
    public function propose(Project $project, Brandroom $brandroom)
    {
        if($project->workspace()->exists()) {
            return $this->errorResponse(new \Exception('Sorry, You can\'t propose project second time!'));
        }

        $this->authorize('propose', $project);

        $this->attachBrandroomToProject($brandroom, $project);

        return $this->successResponse($project, 'You proposed for brandroom, pleas wait for answer!', 'project', 'project');
    }

    /**
     * Cancel proposed project for brandroom
     *
     * @param Project $project
     * @param Brandroom $brandroom
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelPropose(Project $project, Brandroom $brandroom)
    {
        $this->authorize('decline', $project);

        $this->projectRepository->decline($project);

        return $this->successResponse($project, 'You proposed for brandroom, pleas wait for answer!', 'project', 'project');
    }

    /**
     * Create the workspace
     *
     * @param Project $project
     * @param Brandroom $brandroom
     * @return \Illuminate\Http\JsonResponse
     */
    public function createWorkspace(Project $project, Brandroom $brandroom)
    {
        if($project->workspace()->exists()) {
            return $this->errorResponse(new \Exception('Workspace already exists!'));
        }
        $chat = $this->chatRepository->create($brandroom);

        // add chat members
        $project->customer->chatable()->create(['chat_id' => $chat->id]);
        $brandroom->chatable()->create(['chat_id' => $chat->id]);

        $workspace = $this->projectRepository->createWorkspace($project, $brandroom, $chat);

        return $this->successResponse($workspace, 'Workspace created successfully! You can chat with brandroom and start project!', 'workspace');
    }

    /**
     * Get workspace
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWorkspace(Workspace $workspace)
    {
        $this->authorize('workspace', $workspace);

        $workspace = $workspace->load(['brandroom', 'customer', 'project', 'products']);

        $workspace = fractal()
            ->item($workspace, new WorkspaceTransformer())
            ->parseIncludes(['project'])
            ->serializeWith(new \Spatie\Fractal\ArraySerializer())
            ->toArray();

        return $this->successResponse($workspace, null, 'workspace');
    }

    /**
     * Complete the project
     *
     * @param Project $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function complete(Project $project)
    {
        $this->authorize('complete', $project);

        $this->projectRepository->setStatus($project, Project::STATUS_COMPLETED);

        return $this->successResponse($project, 'Project completed, good luck!', 'project');
    }

    /**
     * Workspace product upload
     *
     * @param Workspace $workspace
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function workspaceProductUpload(Workspace $workspace, Request $request)
    {
        $this->authorize('workspace', $workspace);

        $product = $this->projectRepository->productUpload($workspace, $request->all());

        return $this->successResponse($product, 'Project completed, good luck!', 'product');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function projectS3Details(Request $request)
    {
        $source = 'users/'. Auth::id() . '/projects/'.$request->get('file');

        $detail = AmazonService::getS3Details($source, $request->get('type'), 'public-read');

        return $this->successResponse(['params' =>$detail, 'url' => getenv('S3')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function workspaceProductS3Details(Workspace $workspace, Request $request)
    {
        $source = 'workspaces/' . $workspace->id . '/products/'.$request->get('file');

        $detail = AmazonService::getS3Details($source, $request->get('type'), 'public-read');

        return $this->successResponse(['params' =>$detail, 'url' => getenv('S3')]);
    }

    /**
     * Delete the project
     *
     * @param Project $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Project $project)
    {
        $this->projectRepository->delete($project);

        return $this->successResponse(null, 'Project has been deleted', 'product');
    }

    /**
     * Transform projects
     *
     * @param $projects
     * @return array
     */
    public function transformCollection($projects)
    {
        return fractal()
            ->collection($projects, new ProjectTransformer())
            ->serializeWith(new \Spatie\Fractal\ArraySerializer())
            ->toArray();
    }

    /**
     * Transform project
     *
     * @param $project
     * @return array
     */
    public function transformItem($project)
    {
        return fractal()
            ->item($project, new ProjectTransformer())
            ->serializeWith(new \Spatie\Fractal\ArraySerializer())
            ->toArray();
    }
}
