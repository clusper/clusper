<?php

namespace App\Http\Controllers;

use App\Repositories\Notification\NotificationRepository;
use App\Repositories\Product\ProductRepository;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use JWTAuth;

class UserController extends Controller
{

    /**
     * @var \App\User|null
     */
    private $user;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * @var NotificationRepository
     */
    private $notificationRepository;


    public function __construct(UserRepository $userRepository,
                                ProductRepository $productRepository,
                                NotificationRepository $notificationRepository)
    {
        $this->user = Auth::user();

        $this->userRepository = $userRepository;

        $this->productRepository = $productRepository;

        $this->notificationRepository = $notificationRepository;
    }
    /**
     * Get user viewed products
     *
     * @return string
     */
    public function viewedProducts()
    {
        $products = $this->productRepository->viewedProducts($this->user);

        return $this->successResponse($products, null, 'products');
    }

    /**
     * Get user followers
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFollowers()
    {
        $followers = $this->userRepository->getFollowers($this->user);

        return $this->successResponse($followers, null, 'followers');
    }

    /**
     * Get user notifications
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNotifications()
    {
        $notifications = $this->notificationRepository->findByUser($this->user);

        return $this->successResponse($notifications, null, 'notifications');
    }
}
