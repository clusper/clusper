<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;

use App\Http\Requests;

class CountryController extends Controller
{
    /**
     * Get all countries
     *
     * @return mixed
     */
    public function all()
    {
        return Country::all();
    }
}
