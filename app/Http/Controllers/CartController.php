<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Facades\StripeService;
use App\Product;
use App\Repositories\Order\OrderRepository;
use App\Repositories\Profile\ProfileRepository;
use App\Repositories\Transaction\TransactionRepository;
use App\Repositories\User\UserRepository;
use App\Services\CartItem;
use App\Transaction;
use App\User;
use App\UserPaymentData;
use Auth;
use Illuminate\Http\Request;
use App\Facades\CartService;
use JWTAuth;
use Session;
use DB;
use Exception;

class CartController extends Controller
{
    private $orderRepository;
    private $transactionRepository;
    private $profileRepository;
    private $userRepository;

    /**
     * @var mixed
     */
    protected $user;

    public function __construct(OrderRepository $orderRepository,
                                TransactionRepository $transactionRepository,
                                ProfileRepository $profileRepository,
                                UserRepository $userRepository)
    {
        $this->user = Auth::user();
        $this->orderRepository = $orderRepository;
        $this->transactionRepository = $transactionRepository;
        $this->profileRepository = $profileRepository;
        $this->userRepository = $userRepository;
    }

    /**
     * Get cart products with info
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = $this->getProducts();

        return $this->successResponse(compact('products'));
    }

    public function getProducts()
    {
        $products = [];

        $cart = CartService::content();

        if($cart) {
            $products = Product::with('brandroom')->whereIn('id', $cart['products'])->get();
        }
        return $products;
    }

    /**
     * Get cart content
     *
     * @return \Illuminate\Http\Response
     */
    public function content()
    {
        $cart = CartService::content();

        return $this->successResponse(compact('cart'));
    }

    /**
     * Add the product to a cart.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        CartService::add($request->product_id);

        $cart = CartService::content();

        return $this->successResponse(compact('cart'), 'Product added to cart!');
    }

    /**
     * Remove the product from a cart.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = null)
    {
        if($id) {
            CartService::remove($id);
        } else {
            CartService::destroy();
        }

        $cart = CartService::content();

        return $this->successResponse(compact('cart'), 'Product has been removed!');
    }

    public function buyAccount(){
        $cart = CartService::content();
        $products = $this->getProducts();

        if($this->user->balance > $cart['total'])
        {
            DB::beginTransaction();

            try
            {
                foreach($products as $product)
                {
                    $order = $this->orderRepository->buy($product, $this->user);
                    $this->userRepository->updateUserBalance($this->user, $product->price, 'out');
                    $this->userRepository->updateUserBalance(User::find($product->brandroom->user_id), $product->price_seller, 'in');
                    $transaction = $this->transactionRepository->create($product->price, $product->commission, 'inside', 'App\Order', $order->id);
                    $this->transactionRepository->paymentService([0 => $transaction->id], 'inside', 0);
                }
            }catch(Exception $e){
                DB::rollBack();
                $this->transactionRepository->errorLog($e, $this->user);
                return $this->errorResponse($e);
            }
        }else return $this->successResponse([], 'You dont have money');

        DB::commit();
        $this->destroy();
        return $this->successResponse([], 'Products pay complete!');
    }

    public function buyStripe(){
        $cart = CartService::content();
        $products = $this->getProducts();
        $transactions = [];

        DB::beginTransaction();

        try
        {
            foreach($products as $product)
            {
                $order = $this->orderRepository->buy($product, $this->user);
                $transaction = $this->transactionRepository->create($product->price, $product->commission, 'inside', 'App\Order', $order->id);
                $transactions = [$transaction->id];
            }
        }catch(Exception $e){
            DB::rollBack();
            $this->transactionRepository->errorLog($e, $this->user);
            return $this->errorResponse($e);
        }

        $charge = StripeService::charge($this->user->payment->stripe_id, $cart['total']);

        if(!$charge)
        {
            DB::rollBack();
            return $this->successResponse(null, 'You dont have money');
        }

        try
        {
            $this->transactionRepository->paymentService($transactions, 'stripe', $charge['id']);
        }catch(Exception $e){
            DB::rollBack();
            $this->transactionRepository->errorLog($e, $this->user);
            return $this->errorResponse($e);
        }

        DB::commit();
        $this->destroy();
        return $this->successResponse([], 'Products pay complete!');
    }
}
