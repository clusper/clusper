<?php

namespace App\Http\Controllers;

use App\Facades\CartService;
use App\Http\Requests\SettingsProfileRequest;
use App\Facades\StripeService;
use App\Repositories\Transaction\TransactionRepository;
use App\Repositories\Profile\ProfileRepository;
use App\Repositories\User\UserRepository;
use Exception;
use Illuminate\Http\Request;
use DB;

use App\Http\Requests;
use Auth;
use JWTAuth;

class ProfileController extends Controller
{
    protected $user;

    private $profileRepository;

    private $userRepository;

    private $transactionRepository;

    public function __construct(ProfileRepository $profileRepository, UserRepository $userRepository, TransactionRepository $transactionRepository)
    {
        $this->user = Auth::user();
        $this->user->load('payment');
        $this->userRepository = $userRepository;
        $this->profileRepository = $profileRepository;
        $this->transactionRepository = $transactionRepository;
    }

    /**
     * Get the settings profile
     *
     * @return \App\User|null
     */
    public function getProfile()
    {
        return $this->successResponse($this->user, null, 'profile');
    }

    /**
     * Update settings profile
     *
     * @return \App\User|null
     */
    public function updateProfile(SettingsProfileRequest $request)
    {
        $this->user->update($request->all());

        return $this->successResponse($this->user, 'Information profile is updated!', 'user');
    }

    /**
     * Update Stripe info
     */
    public function updateStripe(Request $request)
    {
        if($this->user->payment->stripe_id == null)
        {
            $customer = StripeService::createUser($this->user->email);
            $this->profileRepository->addStripeId($this->user->id, $customer['id']);
            $stripe_id = $customer['id'];
        }else{
            $stripe_id = $this->user->payment->stripe_id;
        }
        $card = StripeService::updateCart($stripe_id, $request->get('stripeToken'));
        $this->profileRepository->addCard($this->user->id, $card);
        return $this->successResponse([], 'Card was added');
    }

    /**
     * Update PayPal
     */
    public function updatePaypal(Request $request)
    {

        return $this->successResponse();
    }

    /**
     * Set money to account
     */

    public function setMoneyByStripe(Request $request)
    {
        if($this->user->payment->stripe_id != null &&  $request->get('amount') > 10)
        {
            DB::beginTransaction();

            try
            {
                $this->userRepository->updateUserBalance($this->user, $request->get('amount'), 'in');
                $user_payment = $this->userRepository->updateUserPaymentHistory($this->user, $request->get('amount'));
                $transaction = $this->transactionRepository->create($request->get('amount'), 0.00, 'in', 'App\UserPayment', $this->user->payment->id);
                $this->transactionRepository->paymentService([0 => $transaction->id], 'inside', 0);
            }catch(Exception $e){
                DB::rollBack();
                $this->transactionRepository->errorLog($e, $this->user);
                return $this->errorResponse($e);
            }

            $charge = StripeService::charge($this->user->payment->stripe_id, $request->get('amount'));

            if(!$charge)
            {
                DB::rollBack();
                return $this->successResponse(null, 'You dont have money');
            }

            DB::commit();
            return $this->successResponse(null, 'Money was set');
        }

        return $this->successResponse(null, 'You dont have card or amount not enouph');
    }
}
