<?php

namespace App\Http\Controllers;

use App\Events\UserCreateFeed;
use App\Http\Requests\CommentRequest;
use Event;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Brandroom;
use App\BrandroomNews;
use App\NewsMedia;
use App\NewsProduct;
use App\Facades\MediaService;
use Auth;
use JWTAuth;

class NewsController extends Controller
{
    
    /**
     * Current user
     *
     * @var \App\User|null
     */
    protected $user;
    
    public function __construct()
    {
        $this->user = Auth::user();
    }
    
    
    /**
    * Show news page
    * 
    * return view
    */
    public function index(){
        
    }

    /**
     * Get brandroom products list
     *
     * @param Brandroom $brandroom
     * @return Brandroom
     */
    public function fullNews(Brandroom $brandroom, BrandroomNews $news)
    {
        // $news->load(['images', 'products']);

        return $news;
        // return compact('brandroom', 'news');

    }

    /**
     * Load next portion of news list
     *
     * @param Brandroom $brandroom
     * @return Brandroom
     */
    public function loadMore($brandroom_id, $skip)
    {
        return $this->successResponse(
            BrandroomNews::where('brandroom_id', $brandroom_id)
            ->orderBy('created_at', 'desc')
            ->skip($skip)
            ->take(10)
            ->with(['images'])
            ->withCount('comments')
            ->get()
            );

    }
    
    /**
    * Get News data
    * 
    * return response json
    */
    public function uploadNewsMedia(Request $request){
        
        $this->middleware('auth');
        
        try{
            $response = MediaService::uploadToCdn($request->file('file'), 'news');
            if(is_array($response) && !empty($response))
                $response['status'] = 'success';
            else
                $response['status'] = 'error';
                
            return $this->successResponse($response);
        } catch(\Exception $e){
            return $this->errorResponse($e);
        }
    }
    
    /**
    * Get News data
    * 
    * return response json
    */
    public function addNews(Request $request){
        $brandroom_id = $this->user->brandroom->id;
        try{
            $news_item = BrandroomNews::create([
                'news'=> "$request->body",
                'brandroom_id' => $brandroom_id
                ]);
                
            if($news_item) {
                $news_item->products()->attach($request->products);
                
                if(($request->media)){
                    foreach ($request->media as $media) {
                        $news_media = [
                			'url' => $media['url'],
                			'key' => $media['key'],
                			'type' => 'image',
            			];
                        $news_item->images()->create($news_media);
                    }
                }
                Event::fire(new UserCreateFeed($this->user, $news_item, 'create-news'));

                return $this->successResponse(['news_id' => $news_item->id]);
            }
        }catch(\Exception $e){
            return $this->errorResponse($e);
        }
    }
    
    /**
    * Get News data
    * 
    * return response json
    */
    public function delete(BrandroomNews $news){
        if(MediaService::deleteRelatedMedia($news->id, 'news')){
            $news->comments()->delete();
            $news->delete();
        }
        
        return $this->successResponse(null, 'News deleted successfully');
    }
    
    /**
    * Get News data
    * 
    * return response json
    */
    public function getNewsData($brandroomId){
        News::where()->with(['images', 'comments'])->get();
    }
    
    /**
    * Delete News
    * 
    * return response json
    */
    public function getSingleNewsData($id){
    }
    
    /**
     * Get comments for news
     *
     * @param Product $product
     * @return mixed
     */
    public function getComments(BrandroomNews $news)
    {
        $comments = $news->comments->load('parent');

        return $comments;
    }

    /**
     * Create a new comment for news
     *
     * @param BrandroomNews $news
     * @param CommentRequest $request
     * @return array
     */
    public function storeComments(BrandroomNews $news, CommentRequest $request)
    {
        $comment = $this->user->comments()->create($request->all());

        $comment = $news->comments()->save($comment)->load(['user', 'parent']);

        return compact('comment');
    }

    /**
     * Remove news comment
     *
     * @param Product $product
     * @param Comment $comment
     * @return array
     * @throws \Exception
     */
    public function destroyComments(BrandroomNews $news, Comment $comment)
    {
        $comment->delete();

        return ['message' => 'Comment deleted!'];
    }
}
