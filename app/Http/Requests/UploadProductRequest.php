<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UploadProductRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'   => 'required',
            'description'  => 'required',
            'price'   => 'required',
            'product_title'   => 'required',
            'product_url'   => 'required',
            'rules'   => 'required',
        ];
    }
}
