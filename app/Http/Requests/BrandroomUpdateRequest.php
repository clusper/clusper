<?php

namespace App\Http\Requests;


use Auth;

class BrandroomUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Auth::user()->brandroom->id;

        return [
            'title' => 'required|unique:brandrooms,title,' . $id .'|max:255',
            'alias' => 'required|unique:brandrooms,alias,' . $id .'|max:255',
            'description' => 'required',
            'company' => 'required|max:255',
            'paypal_email' => 'required|email|unique:brandrooms,paypal_email,' . $id .'|max:255',
            'phone' => 'required|max:255',
            'country' => 'required|max:255',
            'city' => 'required|max:255',
        ];
    }
}
