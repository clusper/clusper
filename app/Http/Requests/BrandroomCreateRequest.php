<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BrandroomCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|unique:brandrooms,title|max:255',
            'alias' => 'required|unique:brandrooms,alias|max:255',
            'description' => 'required',
            'company' => 'required|max:255',
            'paypal_email' => 'required|email|unique:brandrooms,paypal_email|max:255',
            'phone' => 'required|max:255',
            //'country' => 'required|max:255',
            'city' => 'required|max:255',
            'rules' => 'required'
        ];
    }
}
