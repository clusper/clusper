<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdministratorPermission extends Model
{
    protected $table = 'administrator_permissions';
    protected $fillable = [
        'status_name'
    ];
}
