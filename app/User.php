<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users'; 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'first_name', 'account', 'last_name', 'facebook', 'google', 'avatar', 'avatar_key'];

    protected $appends = ['url', 'type'];

    /**
     * @return array
     */
    public function getAvatarAttribute($value)
    {

        return $value ?: config('image.default_user_avatar');
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    public function brandroom()
    {
        return $this->hasOne('App\Brandroom');
    }

    public function payment()
    {
        return $this->hasOne('App\UserPaymentData');
    }

    /**
     * User has wishlist products
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function wishroom()
    {
        return $this->belongsToMany('App\Product', 'wishroom', 'user_id', 'product_id')->withTimestamps();
    }

    /**
     * User has cart products
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function cart()
    {
        return $this->belongsToMany('App\Product', 'carts')->withTimestamps();
    }

    /**
     * User has comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }


    /**
     * User has feed(news brandroom)
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function feed()
    {
        return $this->HasMany('App\Feed');
    }

    /**
     * User has notifications
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications()
    {
        return $this->HasMany('App\Notification');
    }

    /**
     * User is author any notifications
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function authorNotification()
    {
        return $this->morphMany('App\Notification', 'author');
    }

    /**
     * User has viewed products
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function viewedProducts()
    {
        return $this->belongsToMany('App\Product', 'viewed_products', 'user_id', 'product_id');
    }

    /**
     * User can has chat
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function chats()
    {
        return $this->morphMany('App\Chat', 'author');
    }

    /**
     * User has projects
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects()
    {
        return $this->hasMany('App\Project', 'customer_id', 'id');
    }

    /**
     * User has messages
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany('App\Message');
    }

    /**
     * User is member of chat
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function chatable()
    {
        return $this->morphMany('App\ChatMember', 'member');
    }


    /**
     * User is member of chat
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function senderable()
    {
        return $this->morphMany('App\ProjectMessage', 'sender');
    }

    /**
     * User is author of project media
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function projectMedia()
    {
        return $this->morphMany('App\ProjectMedia', 'author');
    }

    /**
     * User is member of chat
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function senderMessages()
    {
        return $this->morphMany('App\ProjectMessage', 'sender');
    }

    /**
     * User is member of chat
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function receiveMessages()
    {
        return $this->morphMany('App\ProjectMessage', 'receiver');
    }


    /**
     * url user
     *
     * @return mixed
     */
    public function getUrlAttribute()
    {
        return '/user/' . $this->id;
    }

    /**
     * type user
     *
     * @return mixed
     */
    public function getTypeAttribute()
    {
        return 'user';
    }
}
