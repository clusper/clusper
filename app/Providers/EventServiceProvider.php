<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserLogged' => [
            'App\Listeners\UpdateCart',
        ],
        'App\Events\UserLogout' => [
            'App\Listeners\ResetCart',
        ],
        'App\Events\UserCreateFeed' => [
            'App\Listeners\CreateFeed',
        ],
        'App\Events\BrandroomWasCreated' => [
            'App\Listeners\AddBrandroomToSearchIndex',
        ],
        'App\Events\BrandroomWasUpdated' => [
            'App\Listeners\UpdateBrandroomInSearchIndex',
        ],
        'App\Events\UserCreateNotification' => [
            'App\Listeners\CreateNotification',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
