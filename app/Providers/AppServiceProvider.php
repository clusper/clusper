<?php

namespace App\Providers;

use App\Repositories\Brandroom\BrandroomRepository;
use App\Repositories\Brandroom\BrandroomRepositoryEloquent;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Category\CategoryRepositoryEloquent;
use App\Repositories\Chat\ChatRepository;
use App\Repositories\Chat\ChatRepositoryEloquent;
use App\Repositories\Feed\FeedRepository;
use App\Repositories\Feed\FeedRepositoryEloquent;
use App\Repositories\Order\OrderRepository;
use App\Repositories\Order\OrderRepositoryEloquent;
use App\Repositories\Profile\ProfileRepository;
use App\Repositories\Profile\ProfileRepositoryEloquent;
use App\Repositories\Project\ProjectRepository;
use App\Repositories\Project\ProjectRepositoryEloquent;
use App\Repositories\Message\MessageRepository;
use App\Repositories\Message\MessageRepositoryEloquent;
use App\Repositories\Notification\NotificationRepository;
use App\Repositories\Notification\NotificationRepositoryEloquent;
use App\Repositories\Product\ProductRepository;
use App\Repositories\Product\ProductRepositoryEloquent;
use App\Repositories\Transaction\TransactionRepository;
use App\Repositories\Transaction\TransactionRepositoryEloquent;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryEloquent;
use Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use App\Services\StripeService;
use App\Services\PayPalService;
use App\Services\MediaService;
use App\Services\AmazonService;
use App\Services\SearchService;
use App\Services\AuthCartService;
use App\Services\GuestCartService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Switch case in blade
        Blade::extend(function($value, $compiler)
        {
            $value = preg_replace('/(?<=\s)@switch\((.*)\)(\s*)@case\((.*)\)(?=\s)/', '<?php switch($1):$2case $3: ?>', $value);
            $value = preg_replace('/(?<=\s)@endswitch(?=\s)/', '<?php endswitch; ?>', $value);

            $value = preg_replace('/(?<=\s)@case\((.*)\)(?=\s)/', '<?php case $1: ?>', $value);
            $value = preg_replace('/(?<=\s)@default(?=\s)/', '<?php default: ?>', $value);
            $value = preg_replace('/(?<=\s)@break(?=\s)/', '<?php break; ?>', $value);

            return $value;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->bootRepositories();

        $this->bootServices();
    }

    public function bootServices()
    {
        $this->app->singleton('CartService', function ($app) {
            return Auth::check() ? new AuthCartService() : new GuestCartService();
        });

        $this->app->bind('StripeService', function ($app) {
            return new StripeService();
        });

        $this->app->bind('PayPalService', function ($app) {
            return new PayPalService();
        });

        $this->app->bind('MediaService', function ($app) {
            return new MediaService();
        });

        $this->app->bind('AmazonService', function ($app) {
            return new AmazonService();
        });

        $this->app->bind('SearchService', function ($app) {
            return new SearchService();
        });

        if ($this->app->environment() == 'local') {
            $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
        }
    }

    public function bootRepositories()
    {
        $this->app->bind(NotificationRepository::class, NotificationRepositoryEloquent::class);
        $this->app->bind(ProductRepository::class, ProductRepositoryEloquent::class);
        $this->app->bind(FeedRepository::class, FeedRepositoryEloquent::class);
        $this->app->bind(UserRepository::class, UserRepositoryEloquent::class);
        $this->app->bind(ProjectRepository::class, ProjectRepositoryEloquent::class);
        $this->app->bind(ChatRepository::class, ChatRepositoryEloquent::class);
        $this->app->bind(MessageRepository::class, MessageRepositoryEloquent::class);
        $this->app->bind(TransactionRepository::class, TransactionRepositoryEloquent::class);
        $this->app->bind(BrandroomRepository::class, BrandroomRepositoryEloquent::class);
        $this->app->bind(TransactionRepository::class, TransactionRepositoryEloquent::class);
        $this->app->bind(OrderRepository::class, OrderRepositoryEloquent::class);
        $this->app->bind(ProfileRepository::class, ProfileRepositoryEloquent::class);
        $this->app->bind(CategoryRepository::class, CategoryRepositoryEloquent::class);
    }
}
