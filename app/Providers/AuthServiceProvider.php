<?php

namespace App\Providers;

use App\BrandroomNews;
use App\Chat;
use App\Comment;
use App\Policies\BrandroomNewsPolicy;
use App\Policies\ChatPolicy;
use App\Policies\CommentPolicy;
use App\Policies\ProductPolicy;
use App\Policies\ProjectPolicy;
use App\Product;
use App\Project;
use App\Workspace;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Chat::class => ChatPolicy::class,
        Comment::class => CommentPolicy::class,
        Product::class => ProductPolicy::class,
        Project::class => ProjectPolicy::class,
        Workspace::class => ProjectPolicy::class,
        BrandroomNews::class => BrandroomNewsPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot()
    {
        parent::registerPolicies();

        //
    }
}
