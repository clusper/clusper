<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use App\BrandroomNews;
use App\Brandroom;
use App\Workspace;
use App\Category;
use App\Product;
use App\Comment;
use App\Project;
use App\Message;
use App\Chat;
use Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Route::model('brandroom_id', Brandroom::class);
        Route::model('category', Category::class);
        Route::model('comment', Comment::class);
        Route::model('news', BrandroomNews::class);
        Route::model('project', Project::class);
        Route::model('message', Message::class);
        Route::model('chat', Chat::class);
        Route::model('workspace', Workspace::class);

        Route::bind('brandroom', function($value)
        {
            return Brandroom::withCount(['followers', 'ifollow'])->where('alias',  $value)->first();
        });

        Route::bind('product', function($value)
        {
            return Product::where('uuid', $value)->firstOrFail();
        });
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Route  $Route
     * @return void
     */
    public function map()
    {
        Route::group(['namespace' => $this->namespace], function ($Route) {
            //$routes = \Cache::remember('routes_files', 60, function () {
                $routes = array_merge(
                    glob(app_path('Http/routes/api') . '/*.php'),
                    glob(app_path('Http/routes') . '/vue.php')
                );

                ksort($routes);
             //   return $routes;
            //});

            foreach ($routes as $route) {
                require $route;
            }
        });
    }
}
