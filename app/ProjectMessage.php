<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectMessage extends Model
{
    protected $table = 'project_messages';

    protected $fillable = ['message_id', 'sender_type', 'sender_id', 'receiver_type', 'receiver_id', 'status'];

    /**
     * Project Message has a sender
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function sender()
    {
        return $this->morphTo();
    }

    /**
     * Project Message has a receiver
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function receiver()
    {
        return $this->morphTo();
    }

    /**
     * Project Message has message
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function message()
    {
        return $this->belongsTo('App\Message');
    }

    /**
     * Project Message has project media
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function media()
    {
        return $this->hasMany('App\ProjectMessageMedia', 'message_id', 'message_id');
    }
}
