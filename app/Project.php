<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';

    protected $fillable = ['title', 'description', 'price', 'category_id', 'customer_id', 'status', 'deadline'];

    protected $with = ['customer'];

    protected $appends = ['applied'];

    protected $dates = ['deadline'];
    
    const STATUS_OPEN           = 'open';
    const STATUS_PERSONAL       = 'personal';
    const STATUS_IN_PROGRESS    = 'in-progress';
    const STATUS_COMPLETED      = 'completed';

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', function(Builder $builder) {
            $builder->orderBy('id', 'desc');
        });
    }

    public function getAppliedAttribute()
    {
        if(!\Auth::user()->brandroom) {
            return false;
        }
        return $this->candidates()->wherePivot('brandroom_id', '=', \Auth::user()->brandroom->id)->first() ? true : false;
    }

    public function setDeadlineAttribute($value)
    {
        $this->attributes['deadline'] =  \Carbon\Carbon::createFromFormat('d F, Y', $value);
    }

    /**
     * Project has category
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    /**
     * Project has the customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('App\User', 'customer_id', 'id');
    }

    /**
     * Project has candidates(brandroom)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function candidates()
    {
        return $this->belongsToMany('App\Brandroom', 'project_candidates');
    }

    /**
     * Project has the workspace
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function workspace()
    {
        return $this->hasOne('App\Workspace');
    }

    /**
     * Get all of the comments's notifications.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function notifications(){
        return $this->morphMany('App\Notification', 'notifiable');
    }

    /**
     * Project has a media
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function media()
    {
        return $this->hasMany('App\ProjectMedia');
    }
}
