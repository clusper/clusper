<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatMember extends Model
{
    protected $table = 'chat_members';

    protected $fillable = ['chat_id'];

    public function member()
    {
        return $this->morphTo();
    }
}
