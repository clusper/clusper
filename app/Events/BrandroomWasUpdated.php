<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BrandroomWasUpdated extends Event
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($brandroom_data)
    {
        $this->brandroom = [
            'id' => $brandroom_data['id'],
            'title' => $brandroom_data['title'],
            'alias' => $brandroom_data['alias'],
            // 'description' => $brandroom_data['description'],
            'logo' => $brandroom_data['logo']
            ];
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
