<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserCreateFeed extends Event
{
    use SerializesModels;

    public $user;

    public $entity;

    public $activity;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $entity, $activity)
    {
        $this->user = $user;

        $this->entity = $entity;

        $this->activity = $activity;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
