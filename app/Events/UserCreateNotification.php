<?php

namespace App\Events;

use App\Events\Event;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserCreateNotification extends Event
{
    use SerializesModels;

    /**
     * @var User
     */
    public $user;

    /**
     * @var
     */
    public $model;

    /**
     * @var
     */
    public $activity;

    /**
     * @var User
     */
    public $author;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Model $author, $model, $activity)
    {
        $this->user = $user;

        $this->author = $author;

        $this->model = $model;

        $this->activity = $activity;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
