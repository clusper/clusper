<?php

namespace App\Events;

use App\Events\Event;
use App\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UserLogged extends Event
{
    use SerializesModels;

    public $user;

    public $cart;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, $cart)
    {
        $this->user = $user;

        $this->cart = $cart;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
