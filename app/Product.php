<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'uuid',
        'url',
        'title',
        'description',
        'alias',
        'price_seller',
        'commission',
        'price',
        'category_id',
        'brandroom_id',
        'status',
        'active',
        'deleted'
    ];

    protected $appends = ['wishroom'];

    protected $hidden = ['options', 'deleted', 'active', 'status', 'brandroom_id'];

    protected $with = ['brandroom'];

    public function tags()
    {
        return $this->belongsToMany('App\Tag', 'product_tags');
    }

    public static function scopeRandom($query)
    {
        return $query->orderByRaw(\DB::raw('RAND()'));
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function feed()
    {
        return $this->morphMany('App\Feed', 'feedable');
    }


    /**
     * Product has many reports
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reports()
    {
        return $this->morphMany('App\Report', 'reportable');
    }

	/**
	 * Product has one a brandroom
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function brandroom()
    {
		return $this->belongsTo('App\Brandroom');
	}

    /**
     * Accessor wishroom
     *
     * @return bool
     */
    public function getWishroomAttribute()
    {
        if(Auth::check()) {
            //return in_array($this->id, (array) Auth::user()->wishroom()->pluck('product_id')->toArray());
        }
        return false;
    }

    /**
     * Product has a users that wish it
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'wishroom')->withTimestamps();
    }

    /**
     * Product has many options
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function options()
    {
        return $this->belongsToMany('App\AttributeOption', 'product_attributes', 'product_id', 'attribute_option_id');
    }

    /**
     * Product has many attributes
     *
     * @return mixed
     */
    public function attributes()
    {
        return $this->belongsToMany('App\Attribute', 'product_attributes', 'product_id', 'attribute_id')->groupBy('attribute_id');
    }

    /**
     * Product has a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function user()
    {
        return $this->belongsTo('App\Brandroom');
    }


    /**
     * Get all of the comments's notifications.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function notifications(){
        return $this->morphMany('App\Notification', 'notifiable');
    }

}
