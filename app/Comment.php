<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['user_id', 'parent_id', 'text'];

    protected $hidden = ['commentable_type', 'commentable_id'];

    protected $appends = ['author'];

    protected $with = ['user', 'commentable'];

    protected $casts = [
        'parent_id' => 'integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * Comment has the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Comment has the parent comment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('App\Comment', 'parent_id');
    }

    /**
     * Check if user is author of comment
     *
     * @return bool
     */
    public function getAuthorAttribute()
    {
        return $this->user_id == Auth::id();
    }

    /**
     * Comment has a children comments
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children() {
        return $this->hasMany('App\Comment', 'parent_id');
    }

    /**
     * Get all of the comments's notifications.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function notifications(){
        return $this->morphMany('App\Notification', 'notifiable');
    }
}
