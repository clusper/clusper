<?php

namespace App\Policies;

use App\Product;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given comment can be updated by the user.
     *
     * @param \App\User $user
     * @param \App\Product $product
     * @return bool
     */
    public function update(User $user, Product $product)
    {
        return $user->id === $product->user_id;
    }

    /**
     * Determine if the given comment can be deleted by the user.
     *
     * @param \App\User $user
     * @param \App\Product $product
     * @return bool
     */
    public function destroy(User $user, Product $product)
    {
        return $user->id === $product->user_id;
    }
}
