<?php

namespace App\Policies;

use App\Brandroom;
use App\Project;
use App\User;
use App\Workspace;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * Determine if the given project can be deleted by the user.
     *
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function destroy(User $user, Project $project)
    {
        return $user->id === $project->customer_id;
    }

    /**
     *
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function apply(User $user, Project $project)
    {
        return $project->status == Project::STATUS_OPEN &&
            !$project->candidates()->wherePivot('brandroom_id', '=', $user->brandroom->id)->first();
    }

    /**
     *
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function cancelApply(User $user, Project $project)
    {
        return $project->status == Project::STATUS_OPEN &&
            $project->candidates()->wherePivot('brandroom_id', '=', $user->brandroom->id)->first();
    }

    /**
     *
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function selectCandidate(Brandroom $brandroom , Project $project)
    {
        return $project->candidates()->wherePivot('brandroom_id', '=', $brandroom->id)->first() ? true : false;
    }

    /**
     * If user has access to workspace
     *
     * @param User $user
     * @param Workspace $workspace
     * @return bool
     */
    public function workspace(User $user, Workspace $workspace)
    {
        return $workspace->brandroom->id === $user->brandroom->id || $workspace->customer->id === $user->id;
    }

    /**
     * If user(brandroom) has access complete project
     *
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function complete(User $user, Project $project)
    {
        return $project->customer_id === $user->id && $project->status === Project::STATUS_IN_PROGRESS;
    }

    /**
     * If user or brandroom has access decline project
     *
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function decline(User $user, Project $project)
    {
        return $project->workspace->brandroom_id === $user->brandroom->id || $project->customer_id === $user->id;
    }

    /**
     * If user has access propose project for brandroom
     *
     * @param User $user
     * @param Project $project
     * @return bool
     */
    public function propose(User $user, Project $project)
    {
        return !$project->candidates()->wherePivot('brandroom_id', '=', $user->brandroom->id)->first() ? true : false;
    }

}
