<?php

namespace App\Policies;

use App\BrandroomNews;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BrandroomNewsPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given brandroom news can be updated by the user.
     *
     * @param \App\User $user
     * @param \App\BrandroomNews $brandroomNews
     * @return bool
     */
    public function update(User $user, BrandroomNews $brandroomNews)
    {
        return $user->id === $brandroomNews->user_id;
    }

    /**
     * Determine if the given brandroom news can be deleted by the user.
     *
     * @param \App\User $user
     * @param \App\BrandroomNews $brandroomNews
     * @return bool
     */
    public function destroy(User $user, BrandroomNews $brandroomNews)
    {
        return $user->id === $brandroomNews->user_id;
    }
}
