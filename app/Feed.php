<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    protected $table = 'feed';

    protected $fillable = ['user_id', 'brandroom', 'activity'];

    protected $hidden = ['feedable_type', 'feedable_id'];

    protected $with = ['feedable', 'user', 'brandroom'];

    /**
     *
     * Get all of the owning feedable models.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function feedable()
    {
        return $this->morphTo();
    }

    /**
     * Feed has the user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Feed has the author notification
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brandroom()
    {
        return $this->belongsTo('App\User', 'brandroom', 'id');
    }
}
