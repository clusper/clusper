<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'keywords', 'alias', 'sort', 'parent'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     *
     * A category is composed of many attributes
     *
     * @return mixed
     */
    public function attributes()
    {
        return $this->hasManyThrough('App\Attribute', 'App\CategoryAttribute', 'category_id', 'id')
                    ->available();
    }

    /**
     * A category has a parent category
     *
     * @return mixed
     */
    public function parent()
    {
        return $this->belongsTo('App\Category', 'parent');
    }

    /**
     * A category has a children categories
     *
     * @return mixed
     */
    public function children()
    {
        return $this->hasMany('App\Category', 'parent');
    }
}
