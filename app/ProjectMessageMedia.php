<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectMessageMedia extends Model
{
    protected $table = 'project_message_media';

    protected $fillable = ['message_id', 'name', 'url', 'type', 'key'];

    /**
     * Media has message
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function message()
    {
        return $this->belongsTo('App\Message');
    }
}
