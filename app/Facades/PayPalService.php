<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class PayPalService extends Facade {

    protected static function getFacadeAccessor() { return 'PayPalService'; }

}