<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class AmazonService extends Facade {

    protected static function getFacadeAccessor() { return 'AmazonService'; }

}