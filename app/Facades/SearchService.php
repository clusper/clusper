<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class SearchService extends Facade {

    protected static function getFacadeAccessor() { return 'SearchService'; }

}