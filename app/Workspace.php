<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workspace extends Model
{
    protected $table = 'workspaces';

    protected $fillable = ['project_id', 'brandroom_id', 'chat_id'];

    /**
     * Workspace has project
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo('App\Project');
    }

    /**
     * Workspace has a brandroom
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brandroom()
    {
        return $this->belongsTo('App\Brandroom');
    }

    /**
     * Workspace has a customer
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->project->customer();
    }

    /**
     * Workspace has a chat
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function chat()
    {
        return $this->belongsTo('App\Chat');
    }

    /**
     * Workspace has a products
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany('App\WorkspaceProduct');
    }

}
