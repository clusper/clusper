<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkspaceProduct extends Model
{
    protected $table = 'workspace_products';

    protected $fillable = ['workspace_id', 'name', 'url', 'type', 'key'];

    /**
     * Workspace product has the Workspace
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function workspace()
    {
        return $this->belongsTo('App\Project');
    }
}
