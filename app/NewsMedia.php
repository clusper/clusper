<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsMedia extends Model
{
    protected $fillable = [
        'news_id',
        'url',
        'type',
        'key'
    ];
    
    
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    
    public function news()
    {
        return $this->hasOne('App\BrandroomNews', 'id', 'news_id');
    }
}
