<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeOption extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attribute_options';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'value', 'sort'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes has options
     *
     * @return \Illuminate\Contracts\Events\Dispatcher
     */
    public function attribute()
    {
        return $this->belongsTo('App\Attribute');
    }

    /**
     * Scope Get available(visible) options
     *
     * @param $query
     * @return mixed
     */
    public function scopeAvailable($query)
    {
        return $query->where('visible',1)
                     ->latest('sort');
    }
}
