<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'amount',
        'commission',
        'status',
        'transactionable_type',
        'transactionable_id'
    ];
}
