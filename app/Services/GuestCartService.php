<?php

namespace App\Services;

use Session;
use App\Product;
use App\Interfaces\Cart as CartInterface;
use Illuminate\Support\Collection;

class GuestCartService implements CartInterface
{
    const DEFAULT_INSTANCE = 'cart';
    /**
     * Holds the current cart instance.
     *
     * @var string
     */
    private $instance;

    /**
     * Cart constructor.
     */
    public function __construct()
    {
        $this->instance = self::DEFAULT_INSTANCE;
    }

    /**
     * Add an item to the cart.
     *
     * @param mixed     $id
     * @return mixed
     */
    public function add($id)
    {
        if(is_array($id)) {
            return array_map(function ($item) {
                return $this->add($item);
            }, $id);
        }

        $products = $this->content()['products'];

        if(!$products->contains($id)) {
            $products->push($id);
        }

        Session::put($this->instance, $products);
    }

    /**
     * Remove an item from cart
     *
     * @param string $rowId
     * @return void
     */
    public function remove($id)
    {
        $content = $this->content()['products']->diff($id);

        Session::put($this->instance, $content->values());
    }
    /**
     * Destroy the current cart instance.
     *
     * @return void
     */
    public function destroy()
    {
        Session::remove($this->instance);
    }
    /**
     * Get the content of the cart.
     *
     * @return \Illuminate\Support\Collection
     */
    public function content()
    {
        $ids = Session::has($this->instance)
            ? Session::get($this->instance)
            : new Collection;

        $content = [
            'products'  => $ids,
            'count'     => $this->count(),
            'total'     => $this->total(),
        ];

        return collect($content);
    }
    /**
     * Get the number of items in the cart.
     *
     * @return int|float
     */
    public function count()
    {
        return count(Session::get($this->instance));
    }

    /**
     * Get total price cart
     *
     * @return int|float
     */
    public function total()
    {
        $products = $this->getProducts();

        return $products ? $products->sum('price') : 0;
    }

    public function getProducts()
    {
        $ids = Session::get($this->instance);

        return $ids ? Product::whereIn('id', $ids)->get() : collect();
    }
}