<?php

namespace App\Services;

use Stripe;

class StripeService{

    public function createUser($email)
    {
        $customer = Stripe::customers()->create([
            'email' => $email
        ]);
        return $customer;
    }

    public function updateCart($stripe_id, $token)
    {
        $card = Stripe::cards()->create($stripe_id, $token);
        return $card;
    }

    public function charge($stripe_id, $amount){
        try
        {
            $charge = Stripe::charges()->create([
                'customer' => $stripe_id,
                'currency' => 'USD',
                'amount'   => $amount,
            ]);
            return $charge;
        } catch(Cartalyst\Stripe\Exception\CardErrorException $e) {
            return false;
        }
    }

}