<?php

namespace App\Services;

use Es;

class SearchService{
	
	
	
	/**
	 * Add new item to index
	 *
	 * @param $type
	 * @param $id
	 * @param $data
	 * @return bulean
	 */
    public function addIndex($type, $id, $data){
        $data = [
		    'body' => $data,
		    'index' => config('elasticsearch.custom.main_index'),
		    'type' => $type,
		    'id' => $id,
		    

		];
		
		$return = Es::index($data);
    }	
	
	/**
	 * Update item index
	 *
	 * @param $type
	 * @param $id
	 * @param $data
	 * @return boolean
	 */
    public function updateIndex($type, $id, $data){
        $data = [
		    'body' => [
		        'doc' => $data
		        ],
		    'index' => config('elasticsearch.custom.main_index'),
		    'type' => $type,
		    'id' => $id,
		];
        
        return Es::update($data);
    }
	
	/**
	 * Search by query
	 *
	 * @param $type
	 * @param $id
	 * @param $data
	 * @return boolean
	 */
    public function searchBrandrooms($query){
        $searchParams['index'] = config('elasticsearch.custom.main_index');
        $searchParams['type'] = 'brandroom';
        $searchParams['size'] = 5;
        $searchParams['body']['query']['multi_match']['query'] = $query;
        $searchParams['body']['query']['multi_match']['fields'] = ['title', 'alias'];
        
        return Es::search($searchParams);
    }


}