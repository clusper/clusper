<?php

namespace App\Services;

use Cloudder;
use Gravatar;
use App\ProductsMedia;
use App\NewsMedia;
use File;
use Request;
use Image;
use Storage;



class MediaService{
	
	
	
	/**
	 * Ckeck if gravatar for email exists
	 *
	 * @param $email
	 * @return $response
	 */
    public function askGravatar($email){
        $response = [];

        if(Gravatar::exists($email)){
            $avatar_result = $this->uploadToCdn(Gravatar::get($email), 'avatar');
            $response = [
            	'avatar' => 1111,
            	'avatar_key' => 1111
            	];
        }
        
        return isset($response) ?: false;
    }

	/**
	 * Upload news media file to CDN and save in DB
	 *
	 * @param $file
	 * @param $type 'video' or 'image'
	 * @return 
	 */
	public function uploadNewsMedia($file, $news_id, $type = 'image') {
		$media_info = $this->uploadToCdn($file, 'news');
		$media = NewsMedia::create([
			'news_id' => $news_id,
			'url' => $media_info['url'],
			'type' => $type,
			'key' => $media_info['key']
			]);
			
		return $media->id;
	}

	/**
	 * Upload product media file to CDN and save in DB
	 *
	 * @param $file
	 * @return 
	 */
	public function uploadProductsMedia($file, $type = 'image') {
		$media_info = $this->uploadToCdn($file, 'product');
		$media = ProductMedia::create([
			'nproduct_id' => $news_id,
			'url' => $media_info['url'],
			'type' => $type,
			'key' => $media_info['key']
			]);
			
		return $media->id;
	}

	/**
	 * Delete media file from CDN and DB by item id
	 *
	 * @param $file
	 * @return (boolean)
	 */
	public function deleteRelatedMedia($item_id, $type) {
/**
TODO: check if cloudder returns positive result
*/
		switch($type){
			case 'avatar':
				$user = User::first($item_id);
				Cloudder::destroy($user->avatar_key);
				$user->avatar_key = '';
				$user->avatar = '';
				$user->save();
				break;
			case 'news':
				$news_media = NewsMedia::where('news_id', $item_id)->pluck('key')->toArray();
				if(empty($news_media))
					return true;
				Cloudder::destroyImages($news_media);
				break;
			case 'product':
				$products_media = ProductsMedia::where('product_id', $item_id);
				Cloudder::destroyImages($products_media->pluck('key'));
				$products_media->delete();
				break;
		}
		
		return true;
	}

	/**
	 * Delete media file from CDN and DB by key
	 *
	 * @param $file
	 * @return (boolean)
	 */
	public function deleteMediaByKey($key, $type) {
/**
TODO: check if cloudder returns positive result
*/
		if(is_array($key) & $type != 'avatar')
			Cloudder::destroyImages($key);
		else
			Cloudder::destroy($key);
			
		switch($type){
			case 'avatar':
					User::where('avatar_key', $key)->update([
						'avatar_key' => '',
						'avatar' => ''
						]);
				break;
			case 'news':
				if(is_array($key))
					NewsMedia::whereIn('key', $key)->delete();
				else
					NewsMedia::where('key', $key)->delete();
				break;
			case 'product':
				if(is_array($key))
					ProductsMedia::whereIn('key', $key)->delete();
				else
					ProductsMedia::where('key', $key)->delete();
				break;
		}
		
		return true;
	}

	/**
	 * Upload media file to CDN
	 *
	 * @param $file
	 * @return array [url, key]
	 */
	public function uploadToCdn($file, $type) {
		$file_key = config('cloudder.folders.' . $type) . '/' 
		. substr(md5($file . time()), 0, config('cloudder.key_length'));
		$wrapper = Cloudder::upload($file, $file_key);
		$upload_result = $wrapper->getResult();
		
		return [
			'url' => $upload_result['secure_url'], 
			'key' => $file_key
			];
	}

	/**
	 * Get extension from mime type
	 *
	 * @param $type
	 * @return string
	 */
	public function getExtensionFromMimeType($type) {
		 switch($type){
		 	case 'image/jpeg':
		 		return '.jpg';
		 		break;
		 	case 'image/png':
		 		return '.png';
		 		break;
		 }
	}

	/**
	 * Resize, crop and upload image
	 *
	 * @param $request
	 * @param $type
	 * @return array [url, key]
	 */
	public function resizeCropUploadImage($request, $image_type) {
		/*
		* TODO: check if $image_type in array of config('cloudder.folders');
		*/
		list($type, $data) = explode(';', $request->imgUrl);
		list(, $data)      = explode(',', $data);
		list(, $type)      = explode(':', $type);
		$data = base64_decode($data);
		
		$file_name = md5(mt_rand(5, 15) . time()) . $this->getExtensionFromMimeType($type);
		
		$path = $image_type . '_crop/' . $file_name;
		$full_path = storage_path() . '/app/' . $path;
		
		Storage::put($path, $data);
		
		$img = Image::make($full_path);
		$cropX = $request->imgX2;
		$cropY = $request->imgY2;
		if ($image_type == 'background') {
			$cropX = 1280;
			$cropY = 120;
		}
		
		$img->resize($request->imgW, $request->imgH)->crop($cropX, $cropY, $request->imgX1, $request->imgY1)->save();

		$uploaded = $this->uploadToCdn($full_path, $image_type);
		
		Storage::delete($path);
		
		return $uploaded;
	}

}