<?php

namespace App\Services;

use App\Cart;
use Auth;
use Session;
use App\Product;
use Illuminate\Support\Collection;
use App\Interfaces\Cart as CartInterface;

class AuthCartService implements CartInterface
{
    /**
     * User cart
     *
     * @var \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public $user;

    /**
     * Holds the current cart instance.
     *
     * @var string
     */
    private $instance;

    /**
     * Cart constructor.
     */
    public function __construct()
    {
        $this->user = Auth::user();
    }

    /**
     * Add an item to the cart.
     *
     * @param mixed     $id
     * @return mixed
     */
    public function add($id)
    {
        if(is_array($id)) {
            return array_map(function ($item) {
                return $this->add($item);
            }, $id);
        }

        if(!$this->user->cart()->where('carts.product_id', $id)->first())
            $this->user->cart()->attach($id);
    }

    /**
     * Remove the cart item with the given rowId from the cart.
     *
     * @param string $rowId
     * @return void
     */
    public function remove($id)
    {
        $this->user->cart()->detach($id);

        return $this->content();
    }

    /**
     * Destroy the current cart instance.
     *
     * @return void
     */
    public function destroy()
    {
        $this->user->cart()->detach();
    }
    /**
     * Get the content of the cart.
     *
     * @return \Illuminate\Support\Collection
     */
    public function content()
    {
        $content = collect([
            'products'  => $this->user->cart()->pluck('carts.product_id'),
            'count'     => $this->count(),
            'total'     => $this->user->cart()->sum('price'),
        ]);

        return $content;
    }
    /**
     * Get the number of items in the cart.
     *
     * @return int|float
     */
    public function count()
    {
        return count($this->user->cart()->get());
    }

    /**
     * Get total price cart
     *
     * @return int|float
     */
    public function total()
    {
        return $this->user->cart()->summ('price');
    }
}