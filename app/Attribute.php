<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attributes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'name', 'sort', 'visible', 'filter', 'type_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * A attribute is composed of many values
     *
     * @return \Illuminate\Contracts\Events\Dispatcher
     */
    public function options()
    {
        return $this->hasMany('App\AttributeOption')
                    ->available();
        //return $this->hasManyThrough('App\AttributeOption', 'App\ProductAttribute', 'attribute_option_id', 'id', 'attribute_id');
    }

    /**
     * A attribute is owned by a type(text, checkbox, selectbox)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\AttributeType', 'type_id', 'id');
    }

    /**
     * Scope Get available(visible) attributes
     *
     * @param $query
     * @return mixed
     */
    public function scopeAvailable($query)
    {
        return $query->where(['visible' => 1, 'filter' => 1])
					 ->latest('sort');
    }
}
