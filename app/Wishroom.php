<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wishroom extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wishroom';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'name', 'sort', 'visible', 'filter', 'type_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    /**
     * Product has a users that wish it
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }
}
