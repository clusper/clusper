<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandroomNews extends Model
{
    protected $fillable = [
        'brandroom_id',
        'news'
    ];

    public function comments(){
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function commentsCount(){
        return $this->morphMany('App\Comment', 'commentable')->count();
    }

    public function images(){
        return $this->hasMany('App\NewsMedia', 'news_id', 'id')->where('type', 'image');
    }

    public function videos(){
        return $this->hasMany('App\NewsMedia', 'news_id', 'id')->where('type', 'video');
    }

    public function brandroom(){
        return $this->hasOne('App\Brandroom', 'id', 'brandroom_id');
    }
    
    public function products(){
        return $this->belongsToMany('App\Product', 'news_products', 'news_id', 'product_id');
    }

    /**
     * Get all of the brandnroom's feed.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function feed(){
        return $this->morphMany('App\Feed', 'feedable');
    }

}
