<?php

namespace App\Listeners;

use App\Events\BrandroomWasCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Facades\SearchService;

class AddBrandroomToSearchIndex
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BrandroomWasCreated  $event
     * @return void
     */
    public function handle(BrandroomWasCreated $event)
    {
        SearchService::addIndex('brandroom', $event->brandroom['id'], $event->brandroom);
    }
}
