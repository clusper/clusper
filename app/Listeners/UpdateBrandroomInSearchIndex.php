<?php

namespace App\Listeners;

use App\Events\BrandroomWasUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Facades\SearchService;

class UpdateBrandroomInSearchIndex
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BrandroomWasUpdated  $event
     * @return void
     */
    public function handle(BrandroomWasUpdated $event)
    {
        SearchService::updateIndex('brandroom', $event->brandroom['id'], $event->brandroom);
    }
}
