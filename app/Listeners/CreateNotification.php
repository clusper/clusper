<?php

namespace App\Listeners;

use App\Events\UserCreateNotification;
use App\Repositories\Notification\NotificationRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Redis;

class CreateNotification
{

    /**
     * @var NotificationRepository
     */
    private $notificationRepository;

    /**
     * Create the event listener.
     *
     * @param NotificationRepository $notificationRepository
     */
    public function __construct(NotificationRepository $notificationRepository)
    {
        $this->notificationRepository = $notificationRepository;
    }

    /**
     * Handle the event.
     *
     * @param  UserCreateNotification  $event
     * @return void
     */
    public function handle(UserCreateNotification $event)
    {
        $notification = $this->notificationRepository->create($event->user, $event->author, $event->model, $event->activity);

        $redis = Redis::connection();
        $redis->publish('notification', json_encode($notification));
    }
}
