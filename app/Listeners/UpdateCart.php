<?php

namespace App\Listeners;

use App\Events\UserLogged;
use App\Facades\CartService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateCart
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLogged  $event
     * @return void
     */
    public function handle(UserLogged $event)
    {
        $ids = $event->user->cart()->pluck('products.id')->toArray();
        $event->user->cart()->sync(array_unique(array_merge($ids, $event->cart['products'])));
    }
}
