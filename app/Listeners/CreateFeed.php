<?php

namespace App\Listeners;

use App\Brandroom;
use App\BrandroomNews;
use App\Comment;
use App\Events\UserCreateFeed;
use App\Product;
use App\Repositories\Feed\FeedRepository;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateFeed
{
    /**
     * @var FeedRepository
     */
    private $feedRepository;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(FeedRepository $feedRepository)
    {
        $this->feedRepository = $feedRepository;
    }

    /**
     * Handle the event.
     *
     * @param  UserCreateFeed  $event
     * @return void
     */
    public function handle(UserCreateFeed $event)
    {
        $this->feedRepository->create($event->user, $event->entity, $event->activity);
    }
}
