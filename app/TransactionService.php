<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionService extends Model
{
    protected $fillable = [
        'transaction_id',
        'payment_service',
        'payment_id'
    ];
}
