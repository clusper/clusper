<?php

namespace App\Transformers;

use App\Message;
use App\Product;
use League\Fractal;

class ProductTransformer extends Fractal\TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'author'
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Product $product)
    {
        return [
            'id'        => (int) $product->id,
            'text'      => $product->text,
            'created_at' => $product->created_at,
        ];
    }


    /**
     * Include Author
     *
     * @return League\Fractal\ItemResource
     */
    public function includeAuthor(Message $message)
    {
        $author = $message->author;

        return $this->item($author, new AuthorTransformer);
    }
}