<?php

namespace App\Transformers;

use App\Project;
use App\Workspace;
use Carbon\Carbon;
use League\Fractal;

class WorkspaceTransformer extends Fractal\TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'project'
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Workspace $workspace)
    {
        return [
            'id'        => (int) $workspace->id,
            'brandroom' => $workspace->brandroom,
            'products'  => $workspace->products,
            'chat_id'   => $workspace->chat_id,
            'created_at'=> $workspace->created_at->format('Y-m-d H:i:s')
        ];
    }


    /**
     * Include project
     *
     * @return League\Fractal\ItemResource
     */
    public function includeProject(Workspace $workspace)
    {
        $project = $workspace->project;

        return $this->item($project, new ProjectTransformer());
    }
}