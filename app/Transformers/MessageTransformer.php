<?php

namespace App\Transformers;

use App\Message;
use Carbon\Carbon;
use League\Fractal;

class MessageTransformer extends Fractal\TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [

    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Message $message)
    {
        return [
            'id'        => (int) $message->id,
            'text'      => $message->text,
            'chat_id'   => $message->chat_id,
            'status'    => $message->projectMessage->status,
            'media'     => $message->projectMessageMedia,
            'sender'    => [
                'name'  => $message->projectMessage->sender->name,
                'url'       => $message->projectMessage->sender->url,
                'avatar'    => $message->projectMessage->sender->logo,
                'type'      => $message->projectMessage->sender->type,
            ],
            'created_at' => $message->created_at->format('Y-m-d H:i:s')
        ];
    }
}