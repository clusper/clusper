<?php

namespace App\Transformers;

use App\Project;
use Carbon\Carbon;
use League\Fractal;

class ProjectTransformer extends Fractal\TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [

    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Project $project)
    {
        return [
            'id'            => (int) $project->id,
            'title'         => $project->title,
            'description'   => $project->description,
            'price'         => $project->price,
            'status'        => $project->status,
            'applied'       => $project->applied,
            'workspace'     => $project->workspace,
            'candidates'    => $project->candidates,
            'candidates_count'=> $project->candidates()->count(),
            'deadline'      => $project->deadline->format('Y-m-d'),
            'brandroom'     => (object) ($project->workspace ? $project->workspace->brandroom : null),
            'customer'      => [
                'id'        => $project->customer->id,
                'name'      => $project->customer->name,
                'url'       => $project->customer->url,
                'avatar'    => $project->customer->logo,
                'type'      => $project->customer->type,
            ],
            'media'         => $project->media,
            'created_at'    => $project->created_at->format('Y-m-d H:i:s')
        ];
    }
}