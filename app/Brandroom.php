<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brandroom extends Model
{
    protected $fillable = [
        'title',
        'alias',
        'description',
        'user_id',
        'city',
        'country',
        'phone',
        'company',
        'paypal_email',
        'background_key',
        'logo',
        'logo_key'
    ];

    protected $appends = ['name', 'url', 'type'];

    /**
     * @return array
     */
    public function getLogoAttribute($value)
    {

        return $value ?: config('image.default_brandroom_logo');
    }

    /**
     * @return array
     */
    public function getBackgroundAttribute($value)
    {

        return $value ?: config('image.default_brandroom_background');
    }
    public function news(){
        return $this->hasMany('App\BrandroomNews')->orderBy('created_at', 'desc');
    }
    public function media(){
        return $this->hasManyThrough('App\NewsMedia','App\BrandroomNews', 'brandroom_id', 'news_id');
    }
    public function products(){
        return $this->hasMany('App\Product');
    }
    
    public function followers(){
        return $this->hasMany('App\Follower', 'followed', 'id');
    }
    
    public function followed(){
        return $this->hasMany('App\Follower', 'follower', 'id');
    }
    
    /**
    Check if user follows brandroom
    */
    public function ifollow(){
        if(\Auth::check())
            $user_id = \Auth::user()->id;
        else
            $user_id = 0;
            
        return $this->hasMany('App\Follower', 'followed', 'id')->where('follower', $user_id);
    }

    public function getBackgroundAttributes()
    {
        
    }

    /**
     * Name brandroom
     *
     * @return mixed
     */
    public function getNameAttribute()
    {
        return $this->title;
    }

    /**
     * Url brandroom
     *
     * @return mixed
     */
    public function getUrlAttribute()
    {
        return '/brandroom/' . $this->alias;
    }

    /**
     * Type brandroom
     *
     * @return mixed
     */
    public function getTypeAttribute()
    {
        return 'brandroom';
    }

    /**
     * Brandroom has a user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }


    /**
     * Brandroom is author any notifications
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function authorNotification()
    {
        return $this->morphMany('App\Notification', 'author');
    }


    /**
     * Brandroom is author of chat
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function chats()
    {
        return $this->morphMany('App\Chat', 'author');
    }

    /**
     * Some brandroom has workspaces
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function workSpaces()
    {
        return $this->hasMany('App\Workspace');
    }

    /**
     * Brandroom is member of chat
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function chatable()
    {
        return $this->morphMany('App\ChatMember', 'member');
    }

    /**
     * Brandroom is sender of message
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function senderMessages()
    {
        return $this->morphMany('App\ProjectMessage', 'sender');
    }

    /**
     * Brandroom is receive of message
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function receiveMessages()
    {
        return $this->morphMany('App\ProjectMessage', 'receiver');
    }

    /**
     * Brandroom is autho of project media
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function projectMedia()
    {
        return $this->morphMany('App\ProjectMedia', 'author');
    }

}
