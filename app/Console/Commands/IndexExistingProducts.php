<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Product;
use Es;

class IndexExistingProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elastic:import_products';
    
    /**
     * The name of the index.
     *
     * @var string
     */
    protected $index_name;
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Indexing existing brandrooms';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->index_name = config('elasticsearch.custom.main_index');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('starting indexing existing products...');
        $p = Product::with(['tags'])->get(['id', 'uuid', 'title', 'description', 'category_id', 'price']);
        $bar = $this->output->createProgressBar(count($p));
        $created = false;
        
        foreach($p as $pp){
            $tags = '';
            if(count($pp->tags) > 0){
                foreach ($pp->tags as $value) {
                    $tags .= $value->name.',';
                }
            }
            $data = [
    		    'body' => [
    		        'title' => $pp->title,
    		        'description' => $pp->description,
    		        'uuid' => $pp->uuid,
    		        'tags' => rtrim($tags, ','),
    		        'category' => $pp->category_id,
    		        'price' => round($pp->price, 0)
    		        ],
    		    'index' => 'market',
    		    'type' => 'product',
    		    'id' => $pp->id,
    		];
    		
    		$response = Es::index($data);
    		
    		if(!$response["created"])
    		    $created = $response["created"];
    		    
    		$bar->advance();
        }
        
        $bar->finish();
        
        if(!$created)
            $this->info("\n...existing products indexed");
    }
}
