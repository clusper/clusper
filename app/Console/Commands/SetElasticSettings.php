<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Es;

class SetElasticSettings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elastic:create';
    
    /**
     * The name of the index.
     *
     * @var string
     */
    protected $index_name;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets settings for index "market" (will delete previous index if exists)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->index_name = config('elasticsearch.custom.main_index');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if(Es::indices()->exists(["index" => $this->index_name])){
            if ($this->confirm('Delete current index "'.$this->index_name.'"? [y|N]')) {
                $response = Es::indices()->delete(["index" => $this->index_name]);
                if($response['acknowledged'])
                    $this->info('index "'.$this->index_name.'" was deleted');
            }else{
                $this->info('no changes were made');
                return;
            }
        }
        $data = [
            'index' => $this->index_name,
            'body' => [
                "settings" => [
                    "analysis" => [
                        "filter" => [
                            "trigrams_filter" => [
                                "type" =>     "ngram",
                                "min_gram" => 4,
                                "max_gram" => 20
                            ]
                        ],
                        "analyzer" => [
                            "trigrams" => [
                                "type" =>      "custom",
                                "tokenizer" => "standard",
                                "filter" =>   [
                                    "lowercase",
                                    "trigrams_filter"
                                ]
                            ]
                        ]
                    ]
                ],
                "mappings" => [
                    "brandroom" => [
                        "properties" => [
                            "title" => [
                                "type" =>     "string",
                                "analyzer" => "trigrams" 
                            ],
                            "alias" => [
                                "type" =>     "string",
                                "analyzer" => "trigrams" 
                            ],
                            "id" => [
                                "type" =>     "long",
                                "index" =>    "not_analyzed"
                            ],
                            "logo" => [
                                "type" =>     "string",
                                "index" =>    "not_analyzed"
                            ],
                        ]
                    ],
                    'product' => [
                        "properties" => [
                            "uuid" => [
                                "type" => "string",
                                "index" => "not_analyzed"
                            ],
                            "title" => [
                                "type" => "string",
                                "analyzer" => "trigrams"
                            ],
                            "description" => [
                                "type" => "string",
                                "analyzer" => "trigrams"
                            ],
                            "tags" => [
                                "type" => "string",
                                "analyzer" => "trigrams"
                            ],
                            "category" => [
                                "type" => "long",
                                "index" => "not_analyzed"
                            ],
                            "price" => [
                                "type" => "long",
                                "index" => "not_analyzed"
                            ],
                        ],
                    ]
                ]
            ]
        ];
        $response = Es::indices()->create($data);
        if($response['acknowledged'])
            $this->info('index "'.$this->index_name.'" was created');
    }
}
