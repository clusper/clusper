<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Brandroom;
use Es;

class IndexExistingBrandrooms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'elastic:import_brandrooms';
    
    /**
     * The name of the index.
     *
     * @var string
     */
    protected $index_name;
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Indexing existing brandrooms';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->index_name = config('elasticsearch.custom.main_index');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('starting indexing existing brandrooms...');
        $b = Brandroom::all();
        $bar = $this->output->createProgressBar(count($b));
        $created = false;
        
        foreach($b as $bb){
            $data = [
    		    'body' => [
    		        'title' => $bb->title,
    		        'alias' => $bb->alias,
    		        'id' => $bb->id,
    		        'logo' => $bb->logo
    		        ],
    		    'index' => 'market',
    		    'type' => 'brandroom',
    		    'id' => $bb->id,
    		];
    		
    		$response = Es::index($data);
    		
    		if(!$response["created"])
    		    $created = $response["created"];
    		    
    		$bar->advance();
        }
        
        $bar->finish();
        
        if(!$created)
            $this->info('...existing brandrooms indexed');
    }
}
