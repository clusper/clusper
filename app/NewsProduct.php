<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsProduct extends Model
{
    protected $fillable = [
        'news_id',
        'product_id'
    ];

}
