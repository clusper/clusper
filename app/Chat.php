<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chats';

    protected $fillable = ['name', 'author'];

    protected $with = ['author'];

    /**
     * Chat has the author
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->morphTo();
    }

    /**
     * Chat has messages
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany('App\Message');
    }

    /**
     * Chat has members
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function members()
    {
        return $this->hasMany('App\ChatMember');
    }

    /**
     * Caht can has project workspace
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function workspace()
    {
        return $this->hasOne('App\Workspace');
    }
}
