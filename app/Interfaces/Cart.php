<?php

namespace App\Interfaces;

/**
 * Created by PhpStorm.
 * User: arsen
 * Date: 28.06.16
 * Time: 12:13
 */
interface Cart
{
    /**
     * Add an item to the cart.
     *
     * @param $id
     * @return mixed
     */
    public function add($id);

    /**
     * Remove an item from cart
     *
     * @param $id
     * @return mixed
     */
    public function remove($id);

    /**
     * Destroy cart
     *
     * @return mixed
     */
    public function destroy();

    /**
     * Get the content of the cart.
     *
     * @return mixed
     */
    public function content();

    /**
     * Get the number of items in the cart.
     *
     * @return mixed
     */
    public function count();

    /**
     * Get total price otems cart
     *
     * @return mixed
     */
    public function total();
}