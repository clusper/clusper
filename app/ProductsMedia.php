<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsMedia extends Model
{
    protected $fillable = [
        'product_id',
        'url',
        'type'
    ];
}
