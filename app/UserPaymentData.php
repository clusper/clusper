<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPaymentData extends Model
{
    protected $fillable = [
        'user_id',
        'stripe_id',
        'card_type',
        'card_four',
        'paypal_email'
    ];
}
