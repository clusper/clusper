<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Administrator extends Model
{
    protected $fillable = [
        'name',
        'email',
        'password',
        'permission'
    ];

    public function permission_status(){
        return $this->hasOne('App\AdministratorPermission', 'id', 'permission');
    }
}
