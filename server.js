var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var redis = require('redis');
var elasticsearch = require('elasticsearch');
var redisNotification = redis.createClient();
var redisMessage = redis.createClient();

redisNotification.subscribe('notification');
redisMessage.subscribe('send-chat-message');

redisMessage.on("message", (channel, message) => {
    console.log(JSON.parse(message))
    io.sockets.in(JSON.parse(message).chat_id).emit('send-chat-message', message);
});

server.listen(8082);

console.log('server is running');

var client = new elasticsearch.Client({
  host: 'localhost:9200',
  log: 'trace'
});

io.on('connection', (socket) => {

    /* join user to private chat */
    socket.on('join-member-to-chat', (room, member) => {
        socket.join(room);
        console.log(member.id + " has joined room " + room);
    });

    redisNotification.on("message", (channel, message) => {
        socket.emit(channel, message);
    });

    console.log("new client connected");

    socket.on('disconnect', () => {

    });

    redisMessage.on("message", function(channel, message) {
        io.sockets.in(15).emit('send-chat-message', message);
    });

    socket.on('disconnect', function() {
        redisNotification.quit();
    });
    
    /* search */
    // TODO: use filter to discard not active items
    socket.on('main-search', query => {
        client.search({
          index: 'market',
          type: 'brandroom',
          size: 5,
          body: {
            query: {
              multi_match: {
                query: query,
                fields: ["title", "alias"]
              }
            }
          }
        }).then(function (resp) {
            socket.emit('brandroom-results', resp);
        }, function (err) {
            console.trace(err.message);
        });
        
        client.search({
          index: 'market',
          type: 'product',
          size: 5,
          body: {
            query: {
              bool: {
                should: [
                  {match: {"title": query}},
                  {match: {"description": query}},
                  {match: {"tags": query}},
                  ]
              }
                    // must: {
                    // }
            },
            filter: {
              term: {
                category: 13
              }
            }
          }
        }).then(function (resp) {
            socket.emit('product-results', resp);
        }, function (err) {
            console.trace(err.message);
        });
    });
    /* /search */

});